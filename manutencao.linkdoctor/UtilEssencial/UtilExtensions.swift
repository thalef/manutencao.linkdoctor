//
//  UtilExtensions.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 28/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation
import UIKit


class UtilEssencial{
    
    static func toScreenOnBrFormat(date: Date) -> String! {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy' 'HH:mm:ss";
        return dateFormatter.string(from: date);
    }
    
    static func toScreenOnBrFormatWithOutHour(date: Date) -> String! {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy";
        return dateFormatter.string(from: date);
    }
    
    
    static func getDateFromStr(_ strToConvert: String) -> Date? {
        if(strToConvert.count > 0){
            let dateFormatter = DateFormatter();
            dateFormatter.dateFormat = "dd/MM/yyyy";
            //dateFormatter.timeZone = TimeZone.current
            //dateFormatter.locale = Locale.current
            return dateFormatter.date(from: strToConvert) // replace Date String
        }else{
            return nil;
        }
    }
    
}


extension Date {
    func toEpochInt64() -> Int64! {
        return Int64(self.timeIntervalSince1970)
    }
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
    func getDeviceDateDisplayBrWithHour() -> String! {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy' 'HH:mm:ss";
        return dateFormatter.string(from: Date());
    }
    
}

extension UITextField {
    
    enum Direction {
        case Left
        case Right
    }
    
    
    func dropDownStyle(){
        if let dropDownIcon = UIImage(named: "baseline_expand_more_black_24pt"){
            self.withImage(direction: .Right, image: dropDownIcon, colorSeparator: UIColor.lightGray, colorBorder: UIColor.lightGray)
        }
    }
    
    func withImage(direction: Direction, image: UIImage, colorSeparator: UIColor, colorBorder: UIColor){
        
        self.tintColor = .clear;
        let imageView = UIImageView(frame: CGRect(x: 0.0, y: 10.0, width: image.size.width, height: image.size.height));
        imageView.image = image;
        imageView.tintColor = UIColor.black;
        if(Direction.Left == direction){
            self.leftViewMode = .always;
            self.leftView = imageView;
        } else {
            self.rightViewMode = .always;
            self.rightView = imageView;
        }
    }
    
}



extension UITextView {
    
    enum Direction {
        case Left
        case Right
    }
    
    
    func dropDownStyle(){
        if let dropDownIcon = UIImage(named: "baseline_expand_more_black_24pt"){
            self.applyLeftImage(direction: .Right, image: dropDownIcon);
        }
    }
    
    func textFieldStyle(){
        self.applyBorder();
    }
    
    func applyBorder() {
        let borderGray = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
        self.layer.borderColor = borderGray.cgColor
        self.layer.borderWidth = 0.5
        self.layer.cornerRadius = 5
        
    }
    
    func applyLeftImage(direction: Direction, image: UIImage) {
        let icn : UIImage = image;
        let imageView = UIImageView(image: icn);
        let viewWidth = self.frame.size.width;
        self.tintColor = .clear;
        let borderGray = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
        self.layer.borderColor = borderGray.cgColor
        self.layer.borderWidth = 0.5
        self.layer.cornerRadius = 5
        
        imageView.frame = CGRect(x: viewWidth, y: 15.0, width: icn.size.width + 20, height: icn.size.height);
        imageView.contentMode = UIView.ContentMode.center;
        imageView.tintColor = UIColor.black;
        self.addSubview(imageView);
        self.textContainerInset = UIEdgeInsets(top: 2.0, left: icn.size.width + 10.0 , bottom: 2.0, right: 2.0);
    }
}
