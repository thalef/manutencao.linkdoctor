//
//  AppDelegate.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 06/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import UIKit
import RealmSwift
import UserNotifications
enum Identifiers {
    static let viewAction = "VIEW_IDENTIFIER"
    static let newsCategory = "NEWS_CATEGORY"
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        RealmSetup.configureMigration();
        initializeRealm();
        LoginSwitcher.updateRootVC();
        UNUserNotificationCenter.current().delegate = self
        registerForPushNotifications();
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0;
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    private func initializeRealm() {
        let realm = try! Realm();
        guard realm.isEmpty else { return }
    }
    
    func registerForPushNotifications() {
        UNUserNotificationCenter.current()
            .requestAuthorization(options: [.alert, .sound, .badge]) {
                [weak self] granted, error in
                guard let self = self else { return }
                guard granted else { return }
                let viewAction = UNNotificationAction(identifier: Identifiers.viewAction, title: "View", options: [.foreground]);
                let newsCategory = UNNotificationCategory(identifier: Identifiers.newsCategory, actions: [viewAction], intentIdentifiers: [], options: []);
                UNUserNotificationCenter.current().setNotificationCategories([newsCategory]);
                self.getNotificationSettings()
        }
    }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    
    func application(
        _ application: UIApplication,
        didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined();
        UserDefaults.standard.set(token, forKey: Session.KEY_PUSH_NOTIFICATION_KEY);
    }
    
    func application(
        _ application: UIApplication,
        didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    
    func application(
        _ application: UIApplication,
        didReceiveRemoteNotification userInfo: [AnyHashable: Any],
        fetchCompletionHandler completionHandler:
        @escaping (UIBackgroundFetchResult) -> Void) {
        
        guard let aps = userInfo["aps"] as? [String: AnyObject] else {
            completionHandler(.failed)
            return
        }
        // SE QUISER TRATAR QUANDO APP TIVER ABERTO
        // POR ENQUANTO DEIXA NOTIFICACAO MESMO
        if aps["content-available"] as? Int == 1 {
            completionHandler(.newData);
        } else  {
            completionHandler(.newData)
        }
        
    }
    
}



// MARK: - UNUserNotificationCenterDelegate
extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound]);
    }
    
    func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        didReceive response: UNNotificationResponse,
        withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = response.notification.request.content.userInfo;
        let application = UIApplication.shared;
        if(application.applicationState == .active){
            //("user tapped the notification bar when the app is in foreground");
        }
        if(application.applicationState == .inactive)
        {
            //("user tapped the notification bar when the app is in background");
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let tabBarController = storyboard.instantiateViewController(withIdentifier: "TabBarCustomerViewController") as! TabBarCustomerViewController;
        let tabCustomerViewController = storyboard.instantiateViewController(withIdentifier: "TabCustomerViewController") as! TabCustomerViewController;
        let tabAppointmentController = storyboard.instantiateViewController(withIdentifier: "TabAppointmentTableViewController") as! TabAppointmentTableViewController;
        
        guard let aps = userInfo["aps"] as? [String: AnyObject] else {
            completionHandler();
            return;
        }
        guard let customedIdSelected = aps["customerId"] as? String else {
            completionHandler();
            return;
        }
        guard response.actionIdentifier == UNNotificationDefaultActionIdentifier else { return }
        
        guard let appCustomerId = aps["appCustomerId"] as? String else {
            completionHandler();
            return;
        }
        
        if(appCustomerId.count > 0 && appCustomerId == Session.getUsuarioLogadoCodCliente()){
            
            if(customedIdSelected.count > 0){
                let navVC = self.window!.rootViewController as? UINavigationController;
                tabBarController.customerId = customedIdSelected;
                tabCustomerViewController.customerId = customedIdSelected;
                tabAppointmentController.customerId = customedIdSelected;
                tabAppointmentController.syncWhenOpen = true;
                tabBarController.viewControllers?[3] = tabAppointmentController;
                tabBarController.selectedIndex = 3;
                navVC?.pushViewController(tabBarController, animated: false);
                //navVC?.pushViewController(tabAppointmentController, animated: false);
            }
        }
        
        completionHandler();
    }
    
}


