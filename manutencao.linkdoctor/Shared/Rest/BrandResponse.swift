//
//  BrandResponse.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 08/05/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation

struct BrandResponse: Codable {

    let id: String = "";
    let descricaoMarca: String = "";
    let codigo: String = "";
    let telefonePrincipal: String = "";
    let celular: String = "";
    let email: String = "";
    let posicao: Int?;
    let versao: Int?;
    let ativo: Bool;
    let deletado: Bool;
    let servidorAtualizadoEmTimeStamp: Double = 0.0;
    
    enum CodingKeys: String, CodingKey {
        case id
        case descricaoMarca
        case codigo
        case telefonePrincipal
        case celular
        case email
        case posicao
        case versao
        case ativo;
        case deletado;
        case servidorAtualizadoEmTimeStamp;
    }
    
}

