//
//  BrandTypeItemResponse.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 08/05/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation

struct BrandTypeItemResponse: Codable {
    
    let id: String = "";
    let descricaoModelo: String = "";
    let codigo: String = "";
    let codMarca: String = "";
    let descricaoMarca: String = "";
    let posicao: Int?;
    let versao: Int?;
    let ativo: Bool;
    let deletado: Bool;
    let servidorAtualizadoEmTimeStamp: Double = 0.0;
    
    enum CodingKeys: String, CodingKey {
        case id
        case descricaoModelo
        case codigo
        case codMarca
        case descricaoMarca
        case posicao
        case versao
        case ativo;
        case deletado;
        case servidorAtualizadoEmTimeStamp;
    }
    
}

