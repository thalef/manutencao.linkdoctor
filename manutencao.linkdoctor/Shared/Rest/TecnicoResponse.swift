//
//  TecnicoResponse.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 15/05/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//
import Foundation

struct TecnicoResponse: Codable {
    
    let id: String;
    let nome: String;
    let email: String;
    let razaoSocial: String;
    let celularPrincipal: String?;
    let telefonePrincipal: String?;
    let tipoPessoa: String;
    let ativo: Bool;
    let deletado: Bool;
    let servidorAtualizadoEmTimeStamp: Double;
    
    enum CodingKeys: String, CodingKey {
        case id
        case nome
        case email
        case razaoSocial
        case celularPrincipal
        case telefonePrincipal
        case tipoPessoa
        case ativo
        case deletado
        case servidorAtualizadoEmTimeStamp
    }
}
