//
//  GenericTableResponse.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 08/05/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation

struct GenericTableResponse: Codable {
    let productTypes: [ProductTypeResponse];
    let brands: [BrandResponse];
    let brandTypeItems: [BrandTypeItemResponse];
    enum CodingKeys: String, CodingKey {
        case productTypes
        case brands
        case brandTypeItems
    }
    
}
