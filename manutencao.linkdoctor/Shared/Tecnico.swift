//
//  Tecnico.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 15/05/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class Tecnico: Object {
    
    @objc dynamic var id: String = "";
    @objc dynamic var nome: String = "";
    @objc dynamic var email: String = "";
    @objc dynamic var razaoSocial: String = "";
    @objc dynamic var celularPrincipal: String = "";
    @objc dynamic var telefonePrincipal: String = "";
    @objc dynamic var tipoPessoa: String = "";
    @objc dynamic var ativo: Bool = true;
    @objc dynamic var deletado: Bool = false;
    @objc dynamic var servidorAtualizadoEmTimeStamp: Double = 0.0;
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(
        id: String,
    nome: String,
    email: String,
    razaoSocial: String,
    celularPrincipal: String,
    telefonePrincipal: String,
    tipoPessoa: String,
    ativo: Bool,
    deletado: Bool,
    servidorAtualizadoEmTimeStamp: Double) {
        self.init();
        self.id = id;
        self.nome = nome;
        self.email = email;
        self.razaoSocial = razaoSocial;
        self.celularPrincipal = celularPrincipal;
        self.telefonePrincipal = telefonePrincipal;
        self.tipoPessoa = tipoPessoa;
        self.ativo = ativo;
        self.deletado = deletado;
        self.servidorAtualizadoEmTimeStamp = servidorAtualizadoEmTimeStamp;
    }
    
}



extension Tecnico {
    
    static func convertResponseToObject(_ object: TecnicoResponse) -> Tecnico {
        return Tecnico(
            id: object.id,
            nome: object.nome,
            email: object.email,
            razaoSocial: object.razaoSocial,
            celularPrincipal: object.celularPrincipal ?? "",
            telefonePrincipal: object.telefonePrincipal ?? "",
            tipoPessoa: object.tipoPessoa,
            ativo: object.ativo,
            deletado: object.deletado,
            servidorAtualizadoEmTimeStamp: object.servidorAtualizadoEmTimeStamp
        );
    }
    
    static func converListResponseToListObject(_ listToConvert: [TecnicoResponse]) -> [Tecnico] {
        var list: [Tecnico] = [];
        for obj in listToConvert {
            list.append(convertResponseToObject(obj));
        }
        return list;
    }
    
    static func loadAllFromRealm(in realm: Realm = try! Realm()) -> Results<Tecnico> {
        return realm.objects(Tecnico.self)
            .filter("ativo = true and deletado = false")
            .sorted(byKeyPath: "nome")
    }
    
    static func loadAllFromRealmActive(in realm: Realm = try! Realm()) -> Results<Tecnico> {
        return realm.objects(Tecnico.self)
            .filter("\(Constants.CAMPO_ATIVO) = true AND \(Constants.CAMPO_DELETADO) = false")
            .sorted(byKeyPath: "nome")
    }
    
    static func loadAllFromRealmWithDeleted(in realm: Realm = try! Realm()) -> Results<Tecnico> {
        return realm.objects(Tecnico.self)
            .sorted(byKeyPath: "nome")
    }
    
    static func loadFromId(in realm: Realm = try! Realm(), id: String) -> Tecnico? {
        return realm.objects(Tecnico.self).filter("\(Constants.CAMPO_ID) = '\(id)'").first;
    }
    
    static func loadAllFromCustomerRealm(in realm: Realm = try! Realm(), customerId: String) -> Results<Tecnico> {
        return realm.objects(Tecnico.self)
            .filter("\(Constants.CAMPO_COD_LOCAL_ATUAL) = '\(customerId)' AND \(Constants.CAMPO_ATIVO) = true AND \(Constants.CAMPO_DELETADO) = false")
            .sorted(byKeyPath: Constants.CAMPO_CRIADO_EM_TIMESTAMP, ascending: false);
    }
    
    @discardableResult
    static func add(objectToSave: Tecnico, in realm: Realm = try! Realm())
        -> Tecnico {
            let item = objectToSave;
            try! realm.write {
                realm.add(item)
            }
            return item
    }
    
    @discardableResult
    static func addOrUpdate(objectToSave: Tecnico, in realm: Realm = try! Realm())
        -> Tecnico {
            let item = objectToSave;
            try! realm.write {
                realm.add(item, update: .all);
            }
            return item
    }
    
    @discardableResult
    static func addAll(listToSave: [Tecnico], in realm: Realm = try! Realm())
        -> [Tecnico] {
            realm.beginWrite();
            for user in listToSave
            {
                realm.add(user)
            }
            try! realm.commitWrite()
            return listToSave
    }
    @discardableResult
    static func addOrUpdateAll(listToSave: [Tecnico], in realm: Realm = try! Realm())
        -> [Tecnico] {
            
//            realm.beginWrite();
//            realm.add(listToSave, update: true)
//            try! realm.commitWrite()
//            return listToSave
            
            do {
                realm.beginWrite();
                realm.add(listToSave, update: .all)
                try realm.commitWrite()
            return listToSave
            } catch let errorCatch {
                print(errorCatch);
            }
            
            return [];
            
            
    }
    
    //    func toggleCompleted() {
    //        guard let realm = realm else { return }
    //        try! realm.write {
    //            isCompleted = !isCompleted
    //        }
    //    }
    //
    //    func delete() {
    //        guard let realm = realm else { return }
    //        try! realm.write {
    //            realm.delete(self)
    //        }
    //    }
}
