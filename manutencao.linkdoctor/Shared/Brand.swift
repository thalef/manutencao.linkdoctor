//
//  Brand.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 08/05/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation
//
//  Brand.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 06/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class Brand: Object {
    
    @objc dynamic var id: String = "";
    @objc dynamic var descricaoMarca: String = "";
    @objc dynamic var codigo: String = "";
    @objc dynamic var telefonePrincipal: String = "";
    @objc dynamic var celular: String = "";
    @objc dynamic var email: String = "";
    @objc dynamic var posicao: Int = 0;
    @objc dynamic var versao: Int = 0;
    @objc dynamic var ativo: Bool = true;
    @objc dynamic var deletado: Bool = false;
    @objc dynamic var servidorAtualizadoEmTimeStamp: Double = 0.0;
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(
        id: String,
        descricaoMarca: String,
        codigo: String,
        telefonePrincipal: String,
        celular: String,
        email: String,
        posicao: Int?,
        versao: Int?,
        ativo: Bool,
        deletado: Bool,
        servidorAtualizadoEmTimeStamp: Double) {
        
        self.init();
        
        self.id = id;
        self.descricaoMarca = descricaoMarca;
        self.codigo = codigo;
        self.telefonePrincipal = telefonePrincipal;
        self.celular = celular;
        self.email = email;
        self.posicao = posicao ?? 0;
        self.versao = versao ?? 0;
        self.ativo = ativo;
        self.deletado = deletado;
        self.servidorAtualizadoEmTimeStamp = servidorAtualizadoEmTimeStamp;
    }
    
}



extension Brand {
    
    static func convertResponseToObject(_ object: BrandResponse) -> Brand {
        return Brand(
            id: object.id,
            descricaoMarca: object.descricaoMarca,
            codigo: object.codigo,
            telefonePrincipal: object.telefonePrincipal,
            celular: object.celular,
            email: object.email,
            posicao: object.posicao ?? 0,
            versao: object.versao ?? 0,
            ativo: object.ativo,
            deletado: object.deletado,
            servidorAtualizadoEmTimeStamp: object.servidorAtualizadoEmTimeStamp
        );
    }
    
    static func converListResponseToListObject(_ listToConvert: [BrandResponse]) -> [Brand] {
        var list: [Brand] = [];
        for obj in listToConvert {
            list.append(convertResponseToObject(obj));
        }
        return list;
    }
    
    static func loadAllFromRealm(in realm: Realm = try! Realm()) -> Results<Brand> {
        return realm.objects(Brand.self).sorted(byKeyPath: "descricaoEquipamento")
    }
    
    static func loadAllFromRealmActive(in realm: Realm = try! Realm()) -> Results<Brand> {
        return realm.objects(Brand.self)
            .filter("\(Constants.CAMPO_ATIVO) = true AND \(Constants.CAMPO_DELETADO) = false")
            .sorted(byKeyPath: "descricaoEquipamento")
    }
    
    static func loadFromId(in realm: Realm = try! Realm(), id: String) -> Brand? {
        return realm.objects(Brand.self).filter("\(Constants.CAMPO_ID) = '\(id)'").first;
    }
    
    static func loadAllFromCustomerRealm(in realm: Realm = try! Realm(), customerId: String) -> Results<Brand> {
        return realm.objects(Brand.self)
            .filter("\(Constants.CAMPO_COD_LOCAL_ATUAL) = '\(customerId)' AND \(Constants.CAMPO_ATIVO) = true AND \(Constants.CAMPO_DELETADO) = false")
            .sorted(byKeyPath: Constants.CAMPO_CRIADO_EM_TIMESTAMP, ascending: false);
    }
    
    @discardableResult
    static func add(objectToSave: Brand, in realm: Realm = try! Realm())
        -> Brand {
            let item = objectToSave;
            try! realm.write {
                realm.add(item)
            }
            return item
    }
    
    @discardableResult
    static func addOrUpdate(objectToSave: Brand, in realm: Realm = try! Realm())
        -> Brand {
            let item = objectToSave;
            try! realm.write {
                realm.add(item, update: .all);
            }
            return item
    }
    
    @discardableResult
    static func addAll(listToSave: [Brand], in realm: Realm = try! Realm())
        -> [Brand] {
            realm.beginWrite();
            for user in listToSave
            {
                realm.add(user)
            }
            try! realm.commitWrite()
            return listToSave
    }
    @discardableResult
    static func addOrUpdateAll(listToSave: [Brand], in realm: Realm = try! Realm())
        -> [Brand] {
            //try! realm.beginWrite();
            realm.beginWrite();
            realm.add(listToSave, update: .all)
            try! realm.commitWrite()
            return listToSave
    }
    
    //    func toggleCompleted() {
    //        guard let realm = realm else { return }
    //        try! realm.write {
    //            isCompleted = !isCompleted
    //        }
    //    }
    //
    //    func delete() {
    //        guard let realm = realm else { return }
    //        try! realm.write {
    //            realm.delete(self)
    //        }
    //    }
}
