//
//  BrandTypeItemTypeItem.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 08/05/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class BrandTypeItem: Object {
    
    @objc dynamic var id: String = "";
    @objc dynamic var descricaoModelo: String = "";
    @objc dynamic var codigo: String = "";
    @objc dynamic var codMarca: String = "";
    @objc dynamic var descricaoMarca: String = "";
    @objc dynamic var posicao: Int = 0;
    @objc dynamic var versao: Int = 0;
    @objc dynamic var ativo: Bool = true;
    @objc dynamic var deletado: Bool = false;
    @objc dynamic var servidorAtualizadoEmTimeStamp: Double = 0.0;
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(
        id: String,
        descricaoModelo: String,
        codigo: String,
        codMarca: String,
        descricaoMarca: String,
        posicao: Int?,
        versao: Int?,
        ativo: Bool,
        deletado: Bool,
        servidorAtualizadoEmTimeStamp: Double) {
        
        self.init();
        
        self.id = id;
        self.descricaoModelo = descricaoModelo;
        self.codigo = codigo;
        self.codMarca = codMarca;
        self.descricaoMarca = descricaoMarca;
        self.posicao = posicao ?? 0;
        self.versao = versao ?? 0;
        self.ativo = ativo;
        self.deletado = deletado;
        self.servidorAtualizadoEmTimeStamp = servidorAtualizadoEmTimeStamp;
    }
    
}



extension BrandTypeItem {
    
    static func convertResponseToObject(_ object: BrandTypeItemResponse) -> BrandTypeItem {
        return BrandTypeItem(
            id: object.id,
            descricaoModelo: object.descricaoModelo,
            codigo: object.codigo,
            codMarca: object.codMarca,
            descricaoMarca: object.descricaoMarca,
            posicao: object.posicao ?? 0,
            versao: object.versao ?? 0,
            ativo: object.ativo,
            deletado: object.deletado,
            servidorAtualizadoEmTimeStamp: object.servidorAtualizadoEmTimeStamp
        );
    }
    
    static func converListResponseToListObject(_ listToConvert: [BrandTypeItemResponse]) -> [BrandTypeItem] {
        var list: [BrandTypeItem] = [];
        for obj in listToConvert {
            list.append(convertResponseToObject(obj));
        }
        return list;
    }
    
    static func loadAllFromRealm(in realm: Realm = try! Realm()) -> Results<BrandTypeItem> {
        return realm.objects(BrandTypeItem.self).sorted(byKeyPath: "descricaoEquipamento")
    }
    
    static func loadAllFromRealmActive(in realm: Realm = try! Realm()) -> Results<BrandTypeItem> {
        return realm.objects(BrandTypeItem.self)
            .filter("\(Constants.CAMPO_ATIVO) = true AND \(Constants.CAMPO_DELETADO) = false")
            .sorted(byKeyPath: "descricaoEquipamento")
    }
    
    static func loadFromId(in realm: Realm = try! Realm(), id: String) -> BrandTypeItem? {
        return realm.objects(BrandTypeItem.self).filter("\(Constants.CAMPO_ID) = '\(id)'").first;
    }
    
    static func loadAllFromCustomerRealm(in realm: Realm = try! Realm(), customerId: String) -> Results<BrandTypeItem> {
        return realm.objects(BrandTypeItem.self)
            .filter("\(Constants.CAMPO_COD_LOCAL_ATUAL) = '\(customerId)' AND \(Constants.CAMPO_ATIVO) = true AND \(Constants.CAMPO_DELETADO) = false")
            .sorted(byKeyPath: Constants.CAMPO_CRIADO_EM_TIMESTAMP, ascending: false);
    }
    
    @discardableResult
    static func add(objectToSave: BrandTypeItem, in realm: Realm = try! Realm())
        -> BrandTypeItem {
            let item = objectToSave;
            try! realm.write {
                realm.add(item)
            }
            return item
    }
    
    @discardableResult
    static func addOrUpdate(objectToSave: BrandTypeItem, in realm: Realm = try! Realm())
        -> BrandTypeItem {
            let item = objectToSave;
            try! realm.write {
                realm.add(item, update: .all);
            }
            return item
    }
    
    @discardableResult
    static func addAll(listToSave: [BrandTypeItem], in realm: Realm = try! Realm())
        -> [BrandTypeItem] {
            realm.beginWrite();
            for user in listToSave
            {
                realm.add(user)
            }
            try! realm.commitWrite()
            return listToSave
    }
    @discardableResult
    static func addOrUpdateAll(listToSave: [BrandTypeItem], in realm: Realm = try! Realm())
        -> [BrandTypeItem] {
            //try! realm.beginWrite();
            realm.beginWrite();
            realm.add(listToSave, update: .all)
            try! realm.commitWrite()
            return listToSave
    }
    
    //    func toggleCompleted() {
    //        guard let realm = realm else { return }
    //        try! realm.write {
    //            isCompleted = !isCompleted
    //        }
    //    }
    //
    //    func delete() {
    //        guard let realm = realm else { return }
    //        try! realm.write {
    //            realm.delete(self)
    //        }
    //    }
}
