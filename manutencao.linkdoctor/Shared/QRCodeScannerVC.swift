//
//  QRCodeScannerVC.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 02/06/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import AVFoundation
import UIKit

class QRCodeScannerVC: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    var selectedProductId: String = "";
    var vcFrom: String = "";
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()
        self.title = "Ler QRCode";
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)
        captureSession.startRunning()
        
    }
 
    func failed() {
        let ac = UIAlertController(title: "Operação não suportada", message: "Seu aparelho não tem suporte para leitura de QRCode.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
            if(captureSession.inputs.count > 0 ){
                let temp = captureSession.inputs.first as! AVCaptureDeviceInput;
                temp.device.autoFocusRangeRestriction = .near;
                temp.device.focusMode = .continuousAutoFocus;
                temp.device.exposureMode = .continuousAutoExposure;
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }
        dismiss(animated: true)
    }
    
    func found(code: String) {
        self.selectedProductId = code;
        if(vcFrom == "AppointmentViewController"){
            self.performSegue(withIdentifier: "getSelectedProdFromQRCodeSegue", sender: self)
        }else if(vcFrom == "TabCustomerViewController"){
            self.performSegue(withIdentifier: "getSelectedProdFromQRCodeToBoundSegue", sender: self)
        }else {
            closeView();
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func closeView(){
        self.navigationController?.popViewController(animated: true);
        self.dismiss(animated: true, completion: nil);
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
}
