//
//  AppointmentResponse.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 26/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation

struct AppointmentRequest: Codable {
    
    let id: String;
    let ativo: Bool;
    let atualizadoEm: Date;
    let atualizadoEmTimeStamp: Double;
    let codLocal: String;
    let codOS: String?;
    let codSituacao: String;
    let codigoQuemCriou: String;
    let criadoEm: Date;
    let criadoEmTimeStamp: Double;
    let criadoPor: String;
    let criado_local: Bool?;
    let deletado: Bool;
    let descricaoLocal: String;
    let chamadoFinalizado: Bool;
    let codQuemSolicitou: String?;
    let codTecnico: String?;
    let descricaoTecnico: String?;
    let descricaoQuemSolicitou: String?;
    let descricaoProblema: String;
    let observacao: String?;
    let descricaoSituacao: String;
    let tipoDescricao: String;
    let equipamentosIds: [String];
    let descricaoEquipamento: String;
    let descricaoSolucao: String;
    let tipoLocal: String;
    let codigoQuemAtualizou: String?;
    let atualizadoPor: String?;
    let precisaSincronizar: Bool?;
    let app_version_code: Int?;
    let app_version_name: String?;
    
    let dataPrevisaoChegadaTimeStamp: Double?;
    let dataPrevisaoChegada: Date?;
    let horaPrevisaoChegada: String?;
    
    
    
    enum CodingKeys: String, CodingKey {
        case id;
        case ativo;
        case atualizadoEm;
        case atualizadoEmTimeStamp;
        case codLocal;
        case codOS;
        case codSituacao;
        case codigoQuemCriou;
        case criadoEm;
        case criadoEmTimeStamp;
        case criadoPor;
        case criado_local;
        case deletado;
        case descricaoLocal;
        case chamadoFinalizado;
        case codQuemSolicitou;
        case codTecnico;
        case descricaoTecnico;
        case descricaoQuemSolicitou;
        case descricaoProblema;
        case observacao;
        case descricaoSituacao;
        case tipoDescricao;
        case equipamentosIds;
        case descricaoEquipamento;
        case descricaoSolucao;
        case tipoLocal;
        case codigoQuemAtualizou;
        case atualizadoPor;
        case precisaSincronizar;
        case app_version_code;
        case app_version_name;
        
        case dataPrevisaoChegadaTimeStamp;
        case dataPrevisaoChegada;
        case horaPrevisaoChegada;
    }
    
}


