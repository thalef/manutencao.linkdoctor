//
//  TabAppointmentTableViewController.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 27/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import UIKit
import RealmSwift
import RxSwift
import RxCocoa
import Network

class TabAppointmentTableViewController: UITableViewController, UISearchResultsUpdating {
    
    let monitor = NWPathMonitor();
    var queue: DispatchQueue!;
    var customerId: String = "";
    var selectedAppointmentId: String = "";
    var isInternetOn: Bool = false;
    private var appointmentList: Results<Appointment>?
    var notificationToken: NotificationToken? = nil
    let syncProcess = SyncProcessHelper();
    let disposeBag = DisposeBag();
    var searchController: UISearchController!
    var activeMode: Bool = true;
    var syncWhenOpen: Bool = false;
    let genericAlert = UIAlertController(title: nil, message: "", preferredStyle: .alert);
    let toastLoading = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge);
    @IBOutlet weak var emptyView: UIView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.tableView.reloadData();
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNotifications();
        setupPullToRefresh();
        loadData();
        setupFields();
        if(syncWhenOpen){
            self.loadingMode(true);
            self.syncServerToArchive();
        }
    }
    
    func setupPullToRefresh(){
        self.refreshControl?.addTarget(self, action: #selector(syncServer), for: .valueChanged)
    }
    
    @objc func syncServer(refreshControl: UIRefreshControl){
        syncProcess.getDefaultSyncRx(token: Session.getToken(), serviceKey: Session.getServiceKey(),
                                     appCustomerId:  Session.getUsuarioLogadoCodCliente(), tipoUsuario: Session.getUsuarioLogadoTipo(),
                                     beginTimeStamp: Session.getKeySincronizacaoTimestampUltimaString()
            )
            .observeOn(MainScheduler.instance)
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(
                onNext: { el in
            },
                onError: { err in
                    refreshControl.endRefreshing();
            },
                onCompleted: { () in
                    self.setLastSync();
                    refreshControl.endRefreshing();
            },
                onDisposed: { () in
                    refreshControl.endRefreshing();
            }
            ).disposed(by: disposeBag);
        
    }
    
    
    @objc func syncServerToArchive(){
        syncProcess.getDefaultSyncRx(token: Session.getToken(), serviceKey: Session.getServiceKey(),
                                     appCustomerId:  Session.getUsuarioLogadoCodCliente(), tipoUsuario: Session.getUsuarioLogadoTipo(),
                                     beginTimeStamp: Session.getKeySincronizacaoTimestampUltimaString()
            )
            .observeOn(MainScheduler.instance)
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(
                onNext: { el in
            },
                onError: { err in
                    self.showToast(message: "Erro ao tentar salvar o chamado", seconds: 2);
            },
                onCompleted: { () in
                    self.setLastSync();
                    self.showToast(message: "Chamado atualizado", seconds: 2);
            },
                onDisposed: { () in
                    self.loadingMode(false);
            }
            ).disposed(by: disposeBag);
        
    }
    
    func setLastSync(){
        UserDefaults.standard.set((0), forKey: Session.KEY_SINCRONIZACAO_TIMESTAMP_SERVER);
        let tempSyncTsServer: Double = Session.getSincronizacaoTsUltimaServer();
        let dateToSave: Date = Date();
        if (tempSyncTsServer == 0 || tempSyncTsServer < Session.getKeySincronizacaoTimestampUltima()) {
            UserDefaults.standard.set((dateToSave.getDeviceDateDisplayBrWithHour()), forKey: Session.KEY_SINCRONIZACAO_DATA_ULTIMA);
            UserDefaults.standard.set((dateToSave.toEpochInt64()), forKey: Session.KEY_SINCRONIZACAO_TIMESTAMP);
        } else {
            UserDefaults.standard.set((dateToSave.getDeviceDateDisplayBrWithHour()), forKey: Session.KEY_SINCRONIZACAO_DATA_ULTIMA);
            UserDefaults.standard.set((dateToSave.toEpochInt64()), forKey: Session.KEY_SINCRONIZACAO_TIMESTAMP);
        }
    }
    
    func loadFromRealm(active: Bool){
        appointmentList = Appointment.loadAllFromCustomerRealmByActive(customerId: customerId, active: active);
    }
    
    func loadData(){
        loadFromRealm(active: activeMode);
        notificationToken = appointmentList?.observe { [weak self] (changes: RealmCollectionChange) in
            guard let tableView = self?.tableView else { return }
            switch changes {
            case .initial:
                tableView.reloadData()
            case .update(_, let deletions, let insertions, let modifications):
                tableView.beginUpdates()
                tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0)}),
                                     with: .automatic)
                tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.endUpdates()
            case .error(let error):
                fatalError("\(error)")
            }
        }
        
    }
    
    func setupFields(){
        setupSearchBar();
        setupLoadingSpin();
    }
    func setupLoadingSpin(){
        toastLoading.center = view.center;
        toastLoading.color = UIColor.white;
        toastLoading.hidesWhenStopped = true;
        toastLoading.layer.cornerRadius = 05;
        toastLoading.isOpaque = false;
        toastLoading.backgroundColor = UIColor.black;
        toastLoading.alpha = 0.3;
        view.addSubview(toastLoading);
    }
    
    func setupSearchBar(){
        searchController = UISearchController(searchResultsController: nil);
        searchController.searchResultsUpdater = self;
        //        searchController.dimsBackgroundDuringPresentation = false;
        searchController.obscuresBackgroundDuringPresentation = false;
        searchController.searchBar.sizeToFit();
        searchController.searchBar.setValue("Cancelar", forKey: "cancelButtonText");
        searchController.searchBar.placeholder = "Buscar pelo equipamento";
        searchController.searchBar.scopeButtonTitles = ["Ativos", "Arquivados"];
        searchController.searchBar.delegate = self;
        searchController.delegate = self;
        //searchController.hidesNavigationBarDuringPresentation = false;
        
        tableView.tableHeaderView = searchController.searchBar;
        definesPresentationContext = true;
        
        genericAlert.view.backgroundColor = UIColor.black;
        genericAlert.view.alpha = 0.6;
        genericAlert.view.layer.cornerRadius = 15;
        genericAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
    }
    
    
    func updateSearchResults(for searchController: UISearchController) {
        if let searchText = searchController.searchBar.text {
            if(searchText.isEmpty){
                //loadFromRealm(active: activeMode);
                loadData();
            }else{
                if(appointmentList?.count ?? 0 > 0 ){
                    appointmentList = appointmentList?
                        .filter("\(Constants.CAMPO_DESCRICAO_EQUIPAMENTO) contains[cd] %@", searchText);
                }
            }
            self.tableView.reloadData();
        }else{
            //loadFromRealm(active: activeMode);
            loadData();
        }
    }
    
    func loadingMode(_ on: Bool){
        //self.enableInteraction(!on);
        if(on){
            toastLoading.startAnimating();
        }else{
            toastLoading.stopAnimating();
        }
    }
    
    
    func showToast(message: String, seconds: Double) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert);
        alert.view.backgroundColor = UIColor.black;
        alert.view.alpha = 0.6;
        alert.view.layer.cornerRadius = 15;
        self.present(alert, animated: true);
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds){
            alert.dismiss(animated: true);
        }
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let size = appointmentList?.count {
            if(size>0){
                self.emptyView.isHidden = true;
                return size;
            }else{
                self.emptyView.isHidden = false;
                return 0;
            }
        }
        self.emptyView.isHidden = false;
        return appointmentList?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "AppointmentsListCell", for: indexPath) as! AppointmentTableViewCell;
        let appointment = self.appointmentList?[(indexPath as NSIndexPath).row];
        let productStock: ProductStock? = ProductStock.loadFromId(id:appointment?.checkEquipamentsIds() ?? "");
        cell.titleLabel.text = (appointment?.descricaoSituacao ?? "Situação não informada")
            + ( (appointment?.ativo ?? true) == false ? " (Arquivado)" : "");
        cell.subTitleLabel.text = productStock?.getDescricaoTela();
        cell.firstTextLabel.text = appointment?.getDataAbriuChamadoEQuem();
        cell.secondTextLabel.text = appointment?.getThirdLineCard();        
        return cell
    }
    
    func showAlert( vc: UIViewController, _ message: String,_ okBtnTitle: String?){
        vc.present(genericAlert, animated: true, completion: nil);
        genericAlert.setValue(message, forKey: "message");
        if(okBtnTitle != nil){
            genericAlert.actions[0].setValue((okBtnTitle ?? "OK"), forKey: "title");
        }
    }
    
    @available(iOS 11.0, *)
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let appointment = self.appointmentList?[(indexPath as NSIndexPath).row];
        let titleArchive = (appointment?.ativo ?? true) ?  "Arquivar" : "Desarquivar";
        let delete = UIContextualAction(style: .normal, title: titleArchive) { (action, sourceView, completionHandler) in
            if(self.internetOn()){
                if(appointment != nil){
                    completionHandler(false);
                    self.loadingMode(true);
                    Appointment.updateToArchiveOrActive(id: appointment!.id, active: !(appointment!.ativo));
                    Customer.modifyBeforeUpdate(
                        id: appointment!.codLocal,
                        usuarioCod: Session.getUsuarioLogadoId(), usuarioDescricao: Session.getUsuarioLogadoDescricao(), dataInformada: Date()
                    );
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.4){
                        self.syncServerToArchive();
                    }
                }else{
                    self.showAlert(vc: self, "Não foi possivel executar esta ação agora..", nil);
                    completionHandler(false);
                }
            }else{
                completionHandler(false);
            }
            
        }
        //        let rename = UIContextualAction(style: .normal, title: "Edit") { (action, sourceView, completionHandler) in
    
        //            completionHandler(true)
        //        }
        //let swipeActionConfig = UISwipeActionsConfiguration(actions: [rename, delete])
        let swipeActionConfig = UISwipeActionsConfiguration(actions: [delete]);
        swipeActionConfig.performsFirstActionWithFullSwipe = false;
        //swipeActionConfig.performsFirstActionWithFullSwipe = true;
        return swipeActionConfig;
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(self.searchController.isActive){
            self.searchController.isActive = false;
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5){
                self.goToDetail(indexPath);
            }
        }else{
            self.goToDetail(indexPath);
        }
    }
    func goToDetail(_ indexPath: IndexPath){
        selectedAppointmentId = self.appointmentList?[(indexPath as NSIndexPath).row].id ?? "";
        self.performSegue(withIdentifier: "appointmentDetail", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "appointmentDetail") {
            let nextVC = segue.destination as! AppointmentViewController;
            nextVC.customerId = self.customerId;
            if(selectedAppointmentId.count > 0){
                nextVC.selectedAppointmentId = self.selectedAppointmentId;
                nextVC.editMode = true;
            }else{
                nextVC.selectedAppointmentId = "";
                nextVC.editMode = false;
            }
        }
        
    }
    
    override func unwind(for unwindSegue: UIStoryboardSegue, towards subsequentVC: UIViewController) {
    }
    
    func setupNotifications(){
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                self.isInternetOn = true;
            } else {
                self.isInternetOn = false;
            }
        }
        let queue = DispatchQueue(label: "Monitor");
        monitor.start(queue: queue);
    }
    func internetOn() -> Bool{
        if(!isInternetOn){
            self.showAlert(vc: self, "Sem internet não é possível realizar a operação.", nil);
        }
        return isInternetOn;
    }
    deinit {
        notificationToken?.invalidate()
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func isFiltering() -> Bool {
        let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
        return searchController.isActive && (!searchBarIsEmpty() || searchBarScopeIsFiltering)
    }
    
}


extension TabAppointmentTableViewController: UISearchControllerDelegate {
    
    func willPresentSearchController(_ searchController: UISearchController) {
    }
    func willDismissSearchController(_ searchController: UISearchController) {
    }
}
extension TabAppointmentTableViewController: UISearchBarDelegate {
    // MARK: - UISearchBar Delegate
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        self.activeMode = (selectedScope == 0);
        loadData();
        //self.loadFromRealm(active: activeMode);
        //self.tableView.reloadData();
    }
}
