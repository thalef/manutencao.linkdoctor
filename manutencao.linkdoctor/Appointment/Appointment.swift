//
//  Appointment.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 06/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class Appointment: Object {
    
    @objc dynamic var id: String = "";
    let equipamentosIds = List<String>();
    @objc dynamic var ativo: Bool = false;
    @objc dynamic var atualizadoEm: Date!;
    @objc dynamic var atualizadoEmTimeStamp: Double = 0.0;
    @objc dynamic var codLocal: String = "";
    @objc dynamic var codOS: String = "";
    @objc dynamic var codSituacao: String = "";
    @objc dynamic var codigoQuemCriou: String = "";
    @objc dynamic var criadoEm: Date!;
    @objc dynamic var criadoEmTimeStamp: Double = 0.0;
    @objc dynamic var criadoPor: String = "";
    @objc dynamic var criado_local: Bool = false;
    @objc dynamic var deletado: Bool = false;
    @objc dynamic var descricaoLocal: String = "";
    @objc dynamic var chamadoFinalizado: Bool = false;
    @objc dynamic var codQuemSolicitou: String?;
    @objc dynamic var codTecnico: String?;
    @objc dynamic var descricaoTecnico: String?;
    @objc dynamic var descricaoQuemSolicitou: String?;
    @objc dynamic var descricaoProblema: String = "";
    @objc dynamic var observacao: String?;
    @objc dynamic var descricaoSituacao: String = "";
    @objc dynamic var servidorAtualizadoEm: Date!;
    @objc dynamic var servidorAtualizadoEmTimeStamp: Double = 0.0;
    @objc dynamic var tipoDescricao: String = "";
    @objc dynamic var tipoLocal: String = "";
    @objc dynamic var descricaoEquipamento: String = "";
    @objc dynamic var descricaoSolucao: String = "";
    
    @objc dynamic var codigoQuemAtualizou: String = "";
    @objc dynamic var atualizadoPor: String = "";
    @objc dynamic var precisaSincronizar: Bool = false;
    
    @objc dynamic var app_version_code: Int = 0;
    @objc dynamic var app_version_name: String = "";
    
    //13-06-2019
    @objc dynamic var dataPrevisaoChegadaTimeStamp: Double = 0.0;
    @objc dynamic var dataPrevisaoChegada: Date!;
    @objc dynamic var horaPrevisaoChegada: String = "";
    
    // @objc dynamic var equipamentosIds: [String] = [];
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(id: String, ativo: Bool, atualizadoEm: Date, atualizadoEmTimeStamp: Double,
                     codLocal: String, codOS: String, codSituacao: String, codigoQuemCriou: String,
                     criadoEm: Date, criadoEmTimeStamp: Double, criadoPor: String, criado_local: Bool?,
                     deletado: Bool, descricaoLocal: String, chamadoFinalizado: Bool,codQuemSolicitou: String?,
                     codTecnico: String?, descricaoTecnico: String?,descricaoQuemSolicitou: String?,
                     descricaoProblema: String, observacao: String?, descricaoSituacao: String,
                     servidorAtualizadoEm: Date, servidorAtualizadoEmTimeStamp: Double, tipoDescricao: String,
                     tipoLocal: String, equipamentosIds: [String],
                     descricaoEquipamento: String, descricaoSolucao: String,
                     codigoQuemAtualizou: String?, atualizadoPor: String?,
                     precisaSincronizar: Bool?
        ) {
        self.init();
        self.id = id;
        self.ativo = ativo;
        self.atualizadoEm = atualizadoEm;
        self.atualizadoEmTimeStamp = atualizadoEmTimeStamp;
        self.codLocal = codLocal;
        self.codOS = codOS;
        self.codSituacao = codSituacao;
        self.codigoQuemCriou = codigoQuemCriou;
        self.criadoEm = criadoEm;
        self.criadoEmTimeStamp = criadoEmTimeStamp;
        self.criadoPor = criadoPor;
        self.criado_local = criado_local ?? false;
        self.deletado = deletado;
        self.descricaoLocal = descricaoLocal;
        self.chamadoFinalizado = chamadoFinalizado;
        self.codQuemSolicitou = codQuemSolicitou ?? "";
        self.codTecnico = codTecnico ?? "";
        self.descricaoTecnico = descricaoTecnico ?? "";
        self.descricaoQuemSolicitou = descricaoQuemSolicitou ?? "";
        self.observacao = observacao ?? "";
        self.descricaoProblema = descricaoProblema;
        self.descricaoSituacao = descricaoSituacao;
        self.servidorAtualizadoEm = servidorAtualizadoEm;
        self.servidorAtualizadoEmTimeStamp = servidorAtualizadoEmTimeStamp;
        self.tipoDescricao = tipoDescricao;
        self.tipoLocal = tipoLocal;
        self.equipamentosIds.removeAll();
        //self.equipamentosIds = [];
        self.equipamentosIds.append(objectsIn: Array(equipamentosIds));
        self.descricaoEquipamento = descricaoEquipamento;
        self.descricaoSolucao = descricaoSolucao;
        
        self.codigoQuemAtualizou = codigoQuemAtualizou ?? "";
        self.atualizadoPor = atualizadoPor ?? "";
        self.precisaSincronizar = precisaSincronizar ?? false;
        
    }
    
    
    convenience init(dataAtual: Date, id: String, codigoUsuario: String, descricaoUsuario: String ) {
        self.init();
        self.id = id;
        self.ativo = true;
        self.atualizadoEm = dataAtual;
        self.atualizadoPor = descricaoUsuario;
        self.codigoQuemAtualizou = codigoUsuario;
        self.codigoQuemCriou = codigoUsuario;
        self.criadoEm = dataAtual;
        self.criadoPor = descricaoUsuario;
        self.deletado = false;
        self.precisaSincronizar = true;
        self.criado_local = true;
        self.criadoEmTimeStamp = Double(dataAtual.toEpochInt64());
        self.atualizadoEmTimeStamp = Double(dataAtual.toEpochInt64());
        self.app_version_code = 1;
        self.app_version_name = "ios";
    }
    
    func modificaAntesAtualizar(_ usuarioCod: String, _ usuarioDescricao: String, _ dataInformada:Date) {
        self.codigoQuemAtualizou = usuarioCod;
        self.atualizadoPor = usuarioDescricao;
        self.atualizadoEm = dataInformada;
        self.precisaSincronizar = true;
        self.atualizadoEmTimeStamp = Double(Date().toEpochInt64());
        //this.app_version_code = Utilidades.getAppVersionCode();
        //this.app_version_name = Utilidades.getAppVersionName();
    }
    
    func getEquipamentoInformado() -> String {
        return "Equipamento não informado";
    }
    
    func getThirdLineCard() -> String {
        return "Defeito: " + (self.descricaoProblema.count > 60 ?
            self.descricaoProblema.prefix(55)+"..." : self.descricaoProblema)
            + ("\n" + "Solução: ") +
            (self.descricaoSolucao.count > 60 ?
                (self.descricaoSolucao.prefix(55)+"...") : self.descricaoSolucao
            )
            +
            (
                self.dataPrevisaoChegadaTimeStamp > 0 ? (
                    "\n" + "Previsão de chegada do técnico: "
                        + self.getDataPrevisaoChegadaTecnico()
                        + self.getHoraPrevisaoChegadaTecnico()
                    ) : ""
            )
    }
    
    func getOsAberta() -> String{
        return (self.codOS.count > 0 ) ? "Ordem de serviço criada" : " ";
    }
    
    func getHoraPrevisaoChegadaTecnico() -> String{
        if(horaPrevisaoChegada.count > 0 ){
            return " " + self.horaPrevisaoChegada;
        }else{
            return "";
        }
    }
    
    func getDataPrevisaoChegadaTecnico() -> String{
        if(dataPrevisaoChegada != nil){
           return UtilEssencial.toScreenOnBrFormatWithOutHour(date:dataPrevisaoChegada);
        }else{
            return "Não informado";
        }
    }
    
    func getDataAbriuChamado() -> String{
        return UtilEssencial.toScreenOnBrFormat(date:criadoEm);
    }
    
    func getDataAbriuChamadoEQuem() -> String {
        return "Chamado aberto em " + getDataAbriuChamado() + ( "\nSolicitante: \(self.descricaoQuemSolicitou ?? " Não informado")" );
    }
    
    func getDataAbriuChamadoEQuemSemQuebra() -> String {
        return "Chamado aberto em " + getDataAbriuChamado() + ( " por \(self.descricaoQuemSolicitou ?? "Solicitante não informado")" );
    }
    
    
    func getIdFirstProduct() -> String{
        if (self.equipamentosIds.count > 0) {
            return self.equipamentosIds[0];
        } else {
            return "";
        }
    }
    
    func checkEquipamentsIds() -> String{
        if (self.equipamentosIds.count > 0) {
            return self.equipamentosIds[0];
        }
        return "";
    }
    
}
// MARK: - CRUD methods

extension Appointment {
    
    
    static func convertResponseToObject(_ appointment: AppointmentResponse) -> Appointment {
        
        let newAppointment: Appointment = Appointment(
            id: appointment.id,
            ativo: appointment.ativo,
            atualizadoEm: appointment.atualizadoEm,
            atualizadoEmTimeStamp: appointment.atualizadoEmTimeStamp,
            codLocal: appointment.codLocal,
            codOS: (appointment.codOS ?? ""),
            codSituacao: appointment.codSituacao,
            codigoQuemCriou: appointment.codigoQuemCriou,
            criadoEm: appointment.criadoEm,
            criadoEmTimeStamp: appointment.criadoEmTimeStamp,
            criadoPor: appointment.criadoPor,
            criado_local: (appointment.criado_local ?? false),
            deletado: appointment.deletado,
            descricaoLocal: appointment.descricaoLocal,
            chamadoFinalizado: appointment.chamadoFinalizado,
            codQuemSolicitou: (appointment.codQuemSolicitou ?? ""),
            codTecnico: (appointment.codTecnico ?? ""),
            descricaoTecnico: (appointment.descricaoTecnico ?? ""),
            descricaoQuemSolicitou: (appointment.descricaoQuemSolicitou ?? ""),
            descricaoProblema: appointment.descricaoProblema,
            observacao: (appointment.observacao ?? ""),
            descricaoSituacao: appointment.descricaoSituacao,
            servidorAtualizadoEm: appointment.servidorAtualizadoEm,
            servidorAtualizadoEmTimeStamp: appointment.servidorAtualizadoEmTimeStamp,
            tipoDescricao: appointment.tipoDescricao, tipoLocal: appointment.tipoLocal,
            equipamentosIds: appointment.equipamentosIds,
            descricaoEquipamento: appointment.descricaoEquipamento,
            descricaoSolucao: appointment.descricaoSolucao,
            codigoQuemAtualizou: appointment.codigoQuemAtualizou,
            atualizadoPor: appointment.atualizadoPor,
            precisaSincronizar: appointment.precisaSincronizar
        );
        
        newAppointment.app_version_code = appointment.app_version_code ?? 0;
        newAppointment.app_version_name = appointment.app_version_name ?? "";
        
        newAppointment.dataPrevisaoChegadaTimeStamp = appointment.dataPrevisaoChegadaTimeStamp ?? 0;
        newAppointment.dataPrevisaoChegada = appointment.dataPrevisaoChegada ?? nil;
        newAppointment.horaPrevisaoChegada = appointment.horaPrevisaoChegada ?? "";
        
        return newAppointment;
        
    }
    
    
    static func converListResponseToListObject(_ listToConvert: [AppointmentResponse]) -> [Appointment] {
        var list: [Appointment] = [];
        for obj in listToConvert {
            list.append(convertResponseToObject(obj));
        }
        return list;
    }
    
    
    static func convertObjectToRequest(_ appointment: Appointment) -> AppointmentRequest {
        
        let objToReturn: AppointmentRequest = AppointmentRequest(
            
            id: appointment.id,
            ativo: appointment.ativo,
            atualizadoEm: appointment.atualizadoEm,
            atualizadoEmTimeStamp: appointment.atualizadoEmTimeStamp,
            codLocal: appointment.codLocal,
            codOS: appointment.codOS,
            codSituacao: appointment.codSituacao,
            codigoQuemCriou: appointment.codigoQuemCriou,
            criadoEm: appointment.criadoEm,
            criadoEmTimeStamp: appointment.criadoEmTimeStamp,
            criadoPor: appointment.criadoPor,
            criado_local: appointment.criado_local,
            deletado: appointment.deletado,
            descricaoLocal: appointment.descricaoLocal,
            chamadoFinalizado: appointment.chamadoFinalizado,
            codQuemSolicitou: appointment.codQuemSolicitou,
            codTecnico: appointment.codTecnico,
            descricaoTecnico: appointment.descricaoTecnico,
            descricaoQuemSolicitou: appointment.descricaoQuemSolicitou,
            descricaoProblema: appointment.descricaoProblema,
            observacao: appointment.observacao,
            descricaoSituacao: appointment.descricaoSituacao,
            tipoDescricao: appointment.tipoDescricao,
            equipamentosIds: Array(appointment.equipamentosIds),
            descricaoEquipamento: appointment.descricaoEquipamento,
            descricaoSolucao: appointment.descricaoSolucao,
            tipoLocal: appointment.tipoLocal,
            codigoQuemAtualizou: appointment.codigoQuemAtualizou,
            atualizadoPor: appointment.atualizadoPor,
            precisaSincronizar: appointment.precisaSincronizar,
            app_version_code: appointment.app_version_code,
            app_version_name: appointment.app_version_name,
            dataPrevisaoChegadaTimeStamp: appointment.dataPrevisaoChegadaTimeStamp,
            dataPrevisaoChegada: appointment.dataPrevisaoChegada,
            horaPrevisaoChegada: appointment.horaPrevisaoChegada
        );
        
        return objToReturn;
    }
    
    
    
    static func converListObjectToListRequest(_ listToConvert: [Appointment]) -> [AppointmentRequest] {
        var list: [AppointmentRequest] = [];
        for obj in listToConvert {
            list.append(convertObjectToRequest(obj));
        }
        return list;
    }
    
    static func loadAllFromRealm(in realm: Realm = try! Realm()) -> Results<Appointment> {
        return realm.objects(Appointment.self);
    }
    
    static func loadAllFromRealmActive(in realm: Realm = try! Realm()) -> Results<Appointment> {
        return realm.objects(Appointment.self)
            .filter("\(Constants.CAMPO_ATIVO) = true AND \(Constants.CAMPO_DELETADO) = false")
    }
    
    static func loadNeedSync(in realm: Realm = try! Realm()) -> Results<Appointment> {
        return realm.objects(Appointment.self)
            .filter("\(Constants.CAMPO_PRECISA_SINCRONIZAR) = true")
    }
    
    static func loadFromId(in realm: Realm = try! Realm(), id: String) -> Appointment? {
        return realm.objects(Appointment.self).filter("\(Constants.CAMPO_ID) = '\(id)'").first;
    }
    
    static func loadAllFromCustomerRealm(in realm: Realm = try! Realm(), customerId: String) -> Results<Appointment> {
        return realm.objects(Appointment.self)
            .filter("\(Constants.CAMPO_COD_LOCAL) = '\(customerId)' AND \(Constants.CAMPO_ATIVO) = true AND \(Constants.CAMPO_DELETADO) = false")
            .sorted(byKeyPath: Constants.CAMPO_CRIADO_EM_TIMESTAMP, ascending: false);
    }
    static func loadAllFromCustomerRealmByActive(in realm: Realm = try! Realm(), customerId: String, active: Bool) -> Results<Appointment> {
        return realm.objects(Appointment.self)
            .filter("\(Constants.CAMPO_COD_LOCAL) = '\(customerId)' AND \(Constants.CAMPO_ATIVO) = \(active) AND \(Constants.CAMPO_DELETADO) = false")
            .sorted(byKeyPath: Constants.CAMPO_CRIADO_EM_TIMESTAMP, ascending: false);
    }
    
    static func loadAllFromCustomerToSync(in realm: Realm = try! Realm(), customerId: String, createdLocal: Bool) -> Results<Appointment> {
        return realm.objects(Appointment.self)
            .filter("\(Constants.CAMPO_COD_LOCAL) = '\(customerId)' and \(Constants.CAMPO_CRIADO_LOCAL) = \(createdLocal)");
    }
    
    @discardableResult
    static func updateToArchiveOrActive(id: String, active: Bool, in realm: Realm = try! Realm())
        -> Appointment? {
            let item: Appointment? = realm.objects(Appointment.self).filter("id = '\(id)'").first;
            if(item != nil){
                try! realm.write {
                    //realm.add(item, update: true);
                    item!.ativo = active;
                    item!.modificaAntesAtualizar(
                        Session.getUsuarioLogadoId(), Session.getUsuarioLogadoDescricao(), Date()
                    );
                }
            }
            return item;
    }
    
    @discardableResult
    static func updateToDelete(id: String, active: Bool, in realm: Realm = try! Realm())
        -> Appointment? {
            let item: Appointment? = realm.objects(Appointment.self).filter("id = '\(id)'").first;
            if(item != nil){
                try! realm.write {
                    //realm.add(item, update: true);
                    item!.deletado = active;
                    item!.modificaAntesAtualizar(
                        Session.getUsuarioLogadoId(), Session.getUsuarioLogadoDescricao(), Date()
                    );
                }
            }
            return item;
    }
    
    @discardableResult
    static func add(objectToSave: Appointment, in realm: Realm = try! Realm())
        -> Appointment {
            let item = objectToSave;
            try! realm.write {
                realm.add(item)
            }
            return item
    }
    
    
    @discardableResult
    static func saveOnRealm(objectToSave: Appointment, in realm: Realm = try! Realm())
        -> Appointment {
            let item = objectToSave;
            
            try! realm.write {
                realm.add(item)
                
            }
            return item
    }
    
    @discardableResult
    static func updateIfCustomer(id: String, objectToSave: Appointment, in realm: Realm = try! Realm())
        -> Bool {
            
            let objectToUpdate = realm.objects(Appointment.self).filter("\(Constants.CAMPO_ID) = '\(id)'").first;
            var updated = false;
            
            try! realm.write {
                
                objectToUpdate?.criado_local = false;
                objectToUpdate?.descricaoProblema = objectToSave.descricaoProblema;
                objectToUpdate?.descricaoQuemSolicitou = objectToSave.descricaoQuemSolicitou;
                objectToUpdate?.equipamentosIds.removeAll();
                objectToUpdate?.equipamentosIds.append(objectsIn: Array(objectToSave.equipamentosIds));
                objectToUpdate?.descricaoEquipamento = objectToSave.descricaoEquipamento;
                
                objectToUpdate?.modificaAntesAtualizar(
                    Session.getUsuarioLogadoId(), Session.getUsuarioLogadoDescricao(), Date()
                );
                
                updated = true;
            }
            
            return updated;
    }
    
    @discardableResult
    static func updateIfMedic(id: String, objectToSave: Appointment, in realm: Realm = try! Realm())
        -> Bool {
            
            let objectToUpdate = realm.objects(Appointment.self).filter("\(Constants.CAMPO_ID) = '\(id)'").first;
            var updated = false;
            
            try! realm.write {
                
                objectToUpdate?.criado_local = false;
                objectToUpdate?.codSituacao = objectToSave.codSituacao;
                objectToUpdate?.descricaoSituacao = objectToSave.descricaoSituacao;
                objectToUpdate?.descricaoProblema = objectToSave.descricaoProblema;
                objectToUpdate?.codTecnico = objectToSave.codTecnico;
                objectToUpdate?.descricaoTecnico = objectToSave.descricaoTecnico;
                objectToUpdate?.descricaoQuemSolicitou = objectToSave.descricaoQuemSolicitou;
                
                objectToUpdate?.equipamentosIds.removeAll();
                objectToUpdate?.equipamentosIds.append(objectsIn: Array(objectToSave.equipamentosIds));
                
                objectToUpdate?.descricaoEquipamento = objectToSave.descricaoEquipamento;
                objectToUpdate?.descricaoSolucao = objectToSave.descricaoSolucao;
                
                objectToUpdate?.dataPrevisaoChegada = objectToSave.dataPrevisaoChegada;
                objectToUpdate?.dataPrevisaoChegadaTimeStamp = objectToSave.dataPrevisaoChegadaTimeStamp;
                objectToUpdate?.horaPrevisaoChegada = objectToSave.horaPrevisaoChegada;
                
                objectToUpdate?.modificaAntesAtualizar(
                    Session.getUsuarioLogadoId(), Session.getUsuarioLogadoDescricao(), Date()
                );
                
                updated = true;
            }
            
            return updated;
    }
    
    
    @discardableResult
    static func addOrUpdate(objectToSave: Appointment, in realm: Realm = try! Realm())
        -> Appointment {
            let item = objectToSave;
            try! realm.write {
                realm.add(item, update: .all);
            }
            return item
    }
    
    @discardableResult
    static func addAll(listToSave: [Appointment], in realm: Realm = try! Realm())
        -> [Appointment] {
            realm.beginWrite();
            for user in listToSave
            {
                realm.add(user)
            }
            try! realm.commitWrite()
            return listToSave
    }
    @discardableResult
    static func addOrUpdateAll(listToSave: [Appointment], in realm: Realm = try! Realm())
        -> [Appointment] {
            //try! realm.beginWrite();
            realm.beginWrite();
            realm.add(listToSave, update: .all)
            try! realm.commitWrite()
            return listToSave
    }
    
    //    func toggleCompleted() {
    //        guard let realm = realm else { return }
    //        try! realm.write {
    //            isCompleted = !isCompleted
    //        }
    //    }
    //
    //    func delete() {
    //        guard let realm = realm else { return }
    //        try! realm.write {
    //            realm.delete(self)
    //        }
    //    }
}
