//  EssCardView.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 27/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class EssCardView: UIView {
    
    //    self.layer.shadowColor = UIColor.black.cgColor;
    //    self.layer.cornerRadius = 8;
    //    self.layer.shadowOpacity = 0.35;
    //    self.layer.shadowRadius =  1.0 ;
    //    self.layer.shadowOffset = CGSize.init(width: 0, height: 2);
    //    let backgroundCGColor = backgroundColor?.cgColor;
    //    self.backgroundColor = nil;
    //    self.layer.backgroundColor =  backgroundCGColor;

//
//    viewCarda.clipsToBounds = true;
//    viewCarda.layer.cornerRadius = 8;
//    viewCarda.addShadow(offset: CGSize.init(width: 0, height: 2), color: UIColor.black, radius: 1.0, opacity: 0.35);
//
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius;
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = false
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return  layer.shadowOpacity;
        }
        set {
            layer.shadowOpacity = newValue
            layer.shadowColor = UIColor.darkGray.cgColor
        }
    }
    
    @IBInspectable var shadowColor: CGColor {
        get {
            return layer.shadowColor ?? UIColor.black.cgColor;
        }
        set {
            layer.shadowColor = newValue
            let backgroundCGColor = backgroundColor?.cgColor
            backgroundColor = nil
            layer.backgroundColor =  backgroundCGColor
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius;
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        get {
            return layer.shadowOffset;
        }
        set {
            layer.shadowOffset = newValue
            layer.shadowColor = UIColor.black.cgColor
            layer.masksToBounds = false
        }
    }
    
}

