//
//  LoginTextField.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 08/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation
import UIKit

class LoginTextField: UITextField {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 5
        tintColor = UIColor.black
        backgroundColor = UIColor.white
        
    }
//    
//    override func textRect(forBounds bounds: CGRect) -> CGRect {
//        let insetBounds = CGRect(x: bounds.origin.x + 8, y: bounds.origin.y, width: bounds.size.width - 16, height: bounds.size.height)
//        return insetBounds
//    }
//    
//    override func editingRect(forBounds bounds: CGRect) -> CGRect {
//        let insetBounds = CGRect(x: bounds.origin.x + 8, y: bounds.origin.y, width: bounds.size.width - 16, height: bounds.size.height)
//        return insetBounds
//    }
    
}
