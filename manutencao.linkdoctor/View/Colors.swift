//
//  Colors.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 08/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    static let primaryDark = UIColor(named: "PrimaryDark")!
    static let primaryLight = UIColor(named: "PrimaryLight")!
    static let gradientTop = UIColor(named: "GradientTop")!
    static let gradientBottom = UIColor(named: "GradientBottom")!
}
