//
//  TabServiceOrdersTableViewController.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 28/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//
import Foundation
import UIKit
import RealmSwift
import RxSwift
import RxCocoa

class TabServiceOrdersTableViewController: UITableViewController, UISearchResultsUpdating {
    
    
    var customerId: String = "";
    private var serviceOrderList: Results<ServiceOrder>?
    var notificationToken: NotificationToken? = nil
    let syncProcess = SyncProcessHelper();
    let disposeBag = DisposeBag();
    var searchController: UISearchController!
    var selectedId: String = "";
    
    @IBOutlet weak var emptyView: UIView!
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPullToRefresh();
        loadData();
        setupFields();
    }
    
    func setupPullToRefresh(){
        self.refreshControl?.addTarget(self, action: #selector(syncServer), for: .valueChanged)
    }
    
    @objc func syncServer(refreshControl: UIRefreshControl){
        syncProcess.getDefaultSyncRx(token: Session.getToken(), serviceKey: Session.getServiceKey(),
                                     appCustomerId:  Session.getUsuarioLogadoCodCliente(), tipoUsuario: Session.getUsuarioLogadoTipo(),
                                     beginTimeStamp: Session.getKeySincronizacaoTimestampUltimaString()
            )
            .observeOn(MainScheduler.instance)
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(
                onNext: { el in
            },
                onError: { err in
                    refreshControl.endRefreshing();
            },
                onCompleted: { () in
                    self.setLastSync();
                    refreshControl.endRefreshing();
            },
                onDisposed: { () in
                    refreshControl.endRefreshing();
            }
            ).disposed(by: disposeBag);
        
    }
    
    func setLastSync(){
        UserDefaults.standard.set((0), forKey: Session.KEY_SINCRONIZACAO_TIMESTAMP_SERVER);
        let tempSyncTsServer: Double = Session.getSincronizacaoTsUltimaServer();
        let dateToSave: Date = Date();
        if (tempSyncTsServer == 0 || tempSyncTsServer < Session.getKeySincronizacaoTimestampUltima()) {
            UserDefaults.standard.set((dateToSave.getDeviceDateDisplayBrWithHour()), forKey: Session.KEY_SINCRONIZACAO_DATA_ULTIMA);
            UserDefaults.standard.set((dateToSave.toEpochInt64()), forKey: Session.KEY_SINCRONIZACAO_TIMESTAMP);
        } else {
            UserDefaults.standard.set((dateToSave.getDeviceDateDisplayBrWithHour()), forKey: Session.KEY_SINCRONIZACAO_DATA_ULTIMA);
            UserDefaults.standard.set((dateToSave.toEpochInt64()), forKey: Session.KEY_SINCRONIZACAO_TIMESTAMP);
        }
    }
    
    func showToast(message: String, seconds: Double) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert);
        alert.view.backgroundColor = UIColor.black;
        alert.view.alpha = 0.6;
        alert.view.layer.cornerRadius = 15;
        self.present(alert, animated: true);
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds){
            alert.dismiss(animated: true);
        }
    }
    
    
    func loadFromRealm(){
        serviceOrderList = ServiceOrder.loadAllFromCustomerRealm(customerId: customerId);
    }
    
    func loadData(){
        loadFromRealm();
        notificationToken = serviceOrderList?.observe { [weak self] (changes: RealmCollectionChange) in
            guard let tableView = self?.tableView else { return }
            switch changes {
            case .initial:
                tableView.reloadData()
            case .update(_, let deletions, let insertions, let modifications):
                tableView.beginUpdates()
                tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0)}),
                                     with: .automatic)
                tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.endUpdates()
            case .error(let error):
                fatalError("\(error)")
            }
        }
        
    }
    
    func setupFields(){
        setupSearchBar();
    }
    
    func setupSearchBar(){
        searchController = UISearchController(searchResultsController: nil);
        searchController.searchResultsUpdater = self;
        //        searchController.dimsBackgroundDuringPresentation = false;
        searchController.obscuresBackgroundDuringPresentation = false;
        searchController.searchBar.sizeToFit();
        searchController.searchBar.setValue("Cancelar", forKey: "cancelButtonText");
        searchController.searchBar.placeholder = "Buscar pelo equipamento";
        tableView.tableHeaderView = searchController.searchBar;
        definesPresentationContext = true;
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        if let searchText = searchController.searchBar.text {
            if(searchText.isEmpty){
                loadFromRealm();
            }else{
                if(serviceOrderList?.count ?? 0 > 0 ){
                    serviceOrderList = serviceOrderList?.filter("\(Constants.CAMPO_DESCRICAO_EQUIPAMENTO) contains[cd] %@", searchText);
                }
            }
            self.tableView.reloadData();
        }else{
            loadFromRealm();
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let size = serviceOrderList?.count {
            if(size>0){
                self.emptyView.isHidden = true;
                return size;
            }else{
                self.emptyView.isHidden = false;
                return 0;
            }
        }
        self.emptyView.isHidden = false;
        return serviceOrderList?.count ?? 0
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "ServiceOrdersListCell", for: indexPath) as! ServiceOrdersTableViewCell;
        let serviceOrder = self.serviceOrderList?[(indexPath as NSIndexPath).row];
        let productStock: ProductStock? = ProductStock.loadFromId(id:serviceOrder?.checkEquipamentsIds() ?? "");
        cell.subTitleLabel.text = productStock?.getDescricaoMarcaModeloSerial();
        cell.titleLabel.text = serviceOrder?.descricaoSituacao;
        cell.firstTextLabel.text = serviceOrder?.getDescricaoPrimeiraLinhaCard();
        cell.secondTextLabel.text = serviceOrder?.getDataFimOSDescricao();
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
//    goToServiceOrder
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            self.goToDetail(indexPath);
    }
    func goToDetail(_ indexPath: IndexPath){
        selectedId = self.serviceOrderList?[(indexPath as NSIndexPath).row].id ?? "";
        self.performSegue(withIdentifier: "goToServiceOrder", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "goToServiceOrder") {
            let nextVC = segue.destination as! ServiceOrderVC;
            nextVC.customerId = self.customerId;
            if(selectedId.count > 0){
                nextVC.selectedId = self.selectedId;
                nextVC.editMode = true;
            }else{
                nextVC.selectedId = "";
                nextVC.editMode = false;
            }
        }
        
    }
    
    deinit {
        notificationToken?.invalidate()
    }
    
    
}
