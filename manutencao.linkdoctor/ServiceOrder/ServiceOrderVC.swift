//
//
//  Created by Thiago Carvalho on 09/05/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import UIKit
import RealmSwift
import Network
import RxSwift
import RxCocoa

class ServiceOrderVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    let monitor = NWPathMonitor();
    var queue: DispatchQueue!;
    var isInternetOn: Bool = false;
    var customerMode: Bool = true;
    let syncProcess = SyncProcessHelper();
    let disposeBag = DisposeBag();
    private var appointmentModel: ServiceOrder?;
    private var customerSelected: Customer?
    private var productSelected: ProductStock?
    private var technicianSelected: Tecnico?
    var situacaoSelecionada = "0";
    var descricaoSituacaoSelecionada = "Selecione a situação";
    var equipamentoSelecionado = "";
    var tecnicoSelecionado = "";
    var customerId: String = "";
    var selectedId: String = "";
    var editMode: Bool = false;
    var toolbar:UIToolbar!;
    @IBOutlet weak var tecnicoLabel: UILabel!
    @IBOutlet weak var situacaoTextField: UITextField!
    @IBOutlet weak var clienteTextField: UITextField!
    @IBOutlet weak var tecnicoTextField: UITextField!
    @IBOutlet weak var equipamentoTextView: UITextView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var solucaoTextView: UITextView!
    @IBOutlet weak var solicitanteTextField: UITextField!
    @IBOutlet weak var defeitoTextView: UITextView!
    @IBOutlet weak var solucaoLabel: UILabel!
    @IBOutlet weak var situacaoLabel: UILabel!
    @IBOutlet weak var deleteButton: UIBarButtonItem!
    
    @IBOutlet weak var arquiveButton: UIBarButtonItem!
    @IBOutlet weak var prevChegaTecnicoLabel: UILabel!
    @IBOutlet weak var prevChegaTecnicoStackView: UIStackView!
    @IBOutlet weak var prevChegaTecnicoDataTF: UITextField!
    @IBOutlet weak var prevChegaTecnicoHoraTF: UITextField!
    
    @IBOutlet weak var finalizaOsLabel: UILabel!
    @IBOutlet weak var quandoAcabaDataTF: UITextField!
    let quandoAcabaDataDP = UIDatePicker();
    @IBOutlet weak var quandoAcabaHoraTF: UITextField!
    let quandoAcabaHoraDP = UIDatePicker();
    
    @IBOutlet weak var numConhecimentoEnvio: UITextField!
    @IBOutlet weak var numOrcamentoAssistencia: UITextField!
    @IBOutlet weak var numConhecimentoRetorno: UITextField!
    
    
    @IBOutlet weak var quandoPegouDataTF: UITextField!
    let quandoPegouDataDP = UIDatePicker();
    @IBOutlet weak var quandoEnviouFabricaDataTF: UITextField!
    let quandoEnviouFabricaDataDP = UIDatePicker();
    @IBOutlet weak var quandoChegouFabricaDataTF: UITextField!
    let quandoChegouFabricaDataDP = UIDatePicker();
    @IBOutlet weak var quandoOsFabricaDataTF: UITextField!
    let quandoOsFabricaDataDP = UIDatePicker();
    @IBOutlet weak var quandoClienteAprovouDataTF: UITextField!
    let quandoClienteAprovouDataDP = UIDatePicker();
    @IBOutlet weak var quandoPrevDevoFabricaDataTF: UITextField!
    let quandoPrevDevoFabricaDataDP = UIDatePicker();
    @IBOutlet weak var quandoChegouEmpresaDataTF: UITextField!
    let quandoChegouEmpresaDataDP = UIDatePicker();
    @IBOutlet weak var quandoChegouClienteDataTF: UITextField!
    let quandoChegouClienteDataDP = UIDatePicker();
    
    let previsaoChegadaDP = UIDatePicker();
    let previsaoChegadaHoraDP = UIDatePicker();
    let situacaoPicker = UIPickerView();
    let situacaoPickerData:[(id: String, description: String)] = [
        ("0", "Selecione a situação"),
        ("1", "Aguardando Análise"),
        ("2", "Em Análise"),
        ("3", "Concluído"),
        ("4", "Cancelada")
    ];
    
    var equipamentoData:[ProductStock] = [];
    private var equipamentoList: Results<ProductStock>?
    
    let tecnicoPicker = UIPickerView();
    var tecnicoPickerData:[Tecnico] = [];
    private var tecnicoList: Results<Tecnico>?
    
    @IBOutlet weak var defeitoCardLabel: UILabel!
    @IBOutlet weak var appointmentConstraintContentHeight: NSLayoutConstraint!
    @IBOutlet weak var appointmentConstraintDadosCV: NSLayoutConstraint!
    @IBOutlet weak var appointmentConstraintDefeitoCV: NSLayoutConstraint!
    
    var activeField: UITextField?
    var activeTextView: UITextView?
    var lastOffset: CGPoint!
    var keyboardHeight: CGFloat!
    
    let genericAlert = UIAlertController(title: nil, message: "", preferredStyle: .alert);
    let saveAlert = UIAlertController(title: nil, message: "Salvando...", preferredStyle: .alert);
    let saveLoading = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge);
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = editMode ? "Editar O.S" : "Nova O.S";
        // Add touch gesture for contentView
        self.contentView.addGestureRecognizer(
            UITapGestureRecognizer(target: self, action: #selector(returnTextView(gesture:)))
        )
        //self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 1400);
        setupNotifications();
        setupCustomerMode();
        loadData();
        setupFields();
        setValueFields();
        setupKeyBoardToolBar();
        setupCustomerMode();
        
    }
    
    // MARK: setups
    
    func setupNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(aNotification:)), name: UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil);
        
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                self.isInternetOn = true;
            } else {
                self.isInternetOn = false;
            }
        }
        let queue = DispatchQueue(label: "Monitor");
        monitor.start(queue: queue);
    }
    
    func setValueFields(){
        
        clienteTextField.text = customerSelected?.nome;
        
        if(editMode){
            
            self.arquiveButton.title = appointmentModel!.ativo ? "Arquivar" : "Desarquivar";
            //            self.arquiveButton.isEnabled = true;
            //            self.deleteButton.isEnabled = true;
            situacaoSelecionada = appointmentModel?.codSituacao ?? situacaoPickerData[0].id;
            //situacaoTextField.text = situacaoPickerData[0].description;
            self.setSituacaoText(situacaoPickerData[0].description);
            for situacaoIndex in 0..<situacaoPickerData.count {
                if( situacaoSelecionada == situacaoPickerData[situacaoIndex].id ){
                    //situacaoTextField.text = situacaoPickerData[situacaoIndex].description;
                    self.setSituacaoText(situacaoPickerData[situacaoIndex].description);
                    descricaoSituacaoSelecionada = situacaoPickerData[situacaoIndex].description;
                    situacaoPicker.selectRow(situacaoIndex, inComponent: 0, animated: false);
                    break
                }
            }
            
            tecnicoSelecionado = appointmentModel?.codTecnico ?? "";
            tecnicoTextField.text = "Selecione o técnico";
            technicianSelected = nil;
            for tecIndex in 0..<tecnicoPickerData.count {
                if( tecnicoSelecionado == tecnicoPickerData[tecIndex].id ){
                    tecnicoTextField.text = tecnicoPickerData[tecIndex].nome;
                    tecnicoPicker.selectRow(tecIndex, inComponent: 0, animated: false);
                    technicianSelected = tecnicoPickerData[tecIndex];
                    break
                }
            }
            
            equipamentoTextView.text = "\n" + "Selecione o equipamento";
            if ((appointmentModel?.equipamentosIds) != nil && (appointmentModel?.equipamentosIds.count ?? 0) > 0) {
                equipamentoSelecionado = appointmentModel?.equipamentosIds[0] ?? "";
                equipamentoTextView.text = "";
                
                for eqIndex in 0..<equipamentoData.count {
                    if( equipamentoSelecionado == equipamentoData[eqIndex].id ){
                        equipamentoTextView.text = equipamentoData[eqIndex].getDescricaoTela();
                        productSelected = equipamentoData[eqIndex];
                        break
                    }
                }
            }
            
            defeitoTextView.text = appointmentModel?.descricaoProblema ?? "";
            solucaoTextView.text = appointmentModel?.descricaoSolucao ?? "";
            solicitanteTextField.text = appointmentModel?.descricaoQuemSolicitou ?? "";
            
            prevChegaTecnicoDataTF.text = "";
            if(appointmentModel?.beginDate != nil){
                prevChegaTecnicoDataTF.text = UtilEssencial.toScreenOnBrFormatWithOutHour(
                    date: appointmentModel!.beginDate!
                );
            }
            prevChegaTecnicoHoraTF.text = appointmentModel?.hourBegin ?? "";
            
            quandoAcabaDataTF.text = "";
            if(appointmentModel?.endDate != nil){
                quandoAcabaDataTF.text = UtilEssencial.toScreenOnBrFormatWithOutHour(
                    date: appointmentModel!.endDate!
                );
            }
            quandoAcabaHoraTF.text = appointmentModel?.hourEnd ?? "";
            
            numConhecimentoEnvio.text = appointmentModel?.sendFabricProtocol ?? "";
            numOrcamentoAssistencia.text = appointmentModel?.orderFabricProtocol ?? "";
            numConhecimentoRetorno.text = appointmentModel?.receiveFabricProtocol ?? "";
            
            if(appointmentModel?.getEquipmentDate != nil){
                quandoPegouDataTF.text = UtilEssencial.toScreenOnBrFormatWithOutHour(
                    date: appointmentModel!.getEquipmentDate!
                );
            }
            if(appointmentModel?.sendFabricDate != nil){
                quandoEnviouFabricaDataTF.text = UtilEssencial.toScreenOnBrFormatWithOutHour(
                    date: appointmentModel!.sendFabricDate!
                );
            }
            if(appointmentModel?.arriveFabricDate != nil){
                quandoChegouFabricaDataTF.text = UtilEssencial.toScreenOnBrFormatWithOutHour(
                    date: appointmentModel!.arriveFabricDate!
                );
            }
            if(appointmentModel?.orderFabricDate != nil){
                quandoOsFabricaDataTF.text = UtilEssencial.toScreenOnBrFormatWithOutHour(
                    date: appointmentModel!.orderFabricDate!
                );
            }
            
            if(appointmentModel?.customerApprovalDate != nil){
                quandoClienteAprovouDataTF.text = UtilEssencial.toScreenOnBrFormatWithOutHour(
                    date: appointmentModel!.customerApprovalDate!
                );
            }
            if(appointmentModel?.scheduledFabricDate != nil){
                quandoPrevDevoFabricaDataTF.text = UtilEssencial.toScreenOnBrFormatWithOutHour(
                    date: appointmentModel!.scheduledFabricDate!
                );
            }
            if(appointmentModel?.arriveCompanyDate != nil){
                quandoChegouEmpresaDataTF.text = UtilEssencial.toScreenOnBrFormatWithOutHour(
                    date: appointmentModel!.arriveCompanyDate!
                );
            }
            if(appointmentModel?.customerReceiveDate != nil){
                quandoChegouClienteDataTF.text = UtilEssencial.toScreenOnBrFormatWithOutHour(
                    date: appointmentModel!.customerReceiveDate!
                );
            }
            
            
            
        }else{
            self.arquiveButton.isEnabled = false;
            self.arquiveButton.tintColor = UIColor.clear;
            self.deleteButton.isEnabled = false;
            self.deleteButton.tintColor = UIColor.clear;
            
            //situacaoTextField.text = situacaoPickerData[0].description;
            self.setSituacaoText(situacaoPickerData[0].description);
            situacaoPicker.selectRow(0, inComponent: 0, animated: false);
            situacaoSelecionada = situacaoPickerData[0].id;
            descricaoSituacaoSelecionada = situacaoPickerData[0].description;
            tecnicoTextField.text = "Selecione o técnico";
            tecnicoSelecionado = "";
            technicianSelected = nil;
            equipamentoTextView.text = "Selecione o equipamento";
            equipamentoSelecionado = "";
            
        }
        
        
        if (self.customerMode) {
            
            prevChegaTecnicoDataTF.isUserInteractionEnabled = false;
            prevChegaTecnicoHoraTF.isUserInteractionEnabled = false;
            quandoAcabaDataTF.isUserInteractionEnabled = false;
            quandoAcabaHoraTF.isUserInteractionEnabled = false;
            numConhecimentoEnvio.isUserInteractionEnabled = false;
            numOrcamentoAssistencia.isUserInteractionEnabled = false;
            numConhecimentoRetorno.isUserInteractionEnabled = false;
            quandoPegouDataTF.isUserInteractionEnabled = false
            quandoEnviouFabricaDataTF.isUserInteractionEnabled = false;
            quandoChegouFabricaDataTF.isUserInteractionEnabled = false;
            quandoOsFabricaDataTF.isUserInteractionEnabled = false;
            quandoClienteAprovouDataTF.isUserInteractionEnabled = false;
            quandoPrevDevoFabricaDataTF.isUserInteractionEnabled = false;
            quandoChegouEmpresaDataTF.isUserInteractionEnabled = false;
            quandoChegouClienteDataTF.isUserInteractionEnabled = false;
            
            solucaoTextView.isUserInteractionEnabled = false;
            tecnicoTextField.isUserInteractionEnabled = false;
            if(!editMode){
                prevChegaTecnicoLabel.isHidden = true;
                prevChegaTecnicoStackView.isHidden = true;
                prevChegaTecnicoDataTF.isHidden = true;
                prevChegaTecnicoHoraTF.isHidden = true;
                tecnicoLabel.isHidden = true;
                tecnicoTextField.isHidden = true;
                solucaoTextView.isHidden = true;
                solucaoLabel.isHidden = true;
                situacaoLabel.isHidden = true;
                situacaoTextField.isHidden = true;
                appointmentConstraintDadosCV.constant = 150;
                appointmentConstraintDefeitoCV.constant = 240;
                defeitoCardLabel.text = "Defeito / Solicitante";
            }
            situacaoTextField.isUserInteractionEnabled = false;
            //situacaoTextField.text = situacaoPickerData[1].description;
            setSituacaoText(situacaoPickerData[1].description);
            situacaoPicker.selectRow(1, inComponent: 0, animated: false);
            situacaoSelecionada = situacaoPickerData[1].id;
            descricaoSituacaoSelecionada = situacaoPickerData[1].description;
            
        }else{
            solucaoTextView.isUserInteractionEnabled = true;
        }
        self.arquiveButton.isEnabled = false;
        self.arquiveButton.tintColor = UIColor.clear;
        
    }
    
    func setupFields(){
        
        situacaoTextField.delegate = self;
        clienteTextField.delegate = self;
        tecnicoTextField.delegate = self;
        equipamentoTextView.delegate = self;
        
        solucaoTextView.delegate = self;
        solucaoTextView.textFieldStyle();
        solucaoTextView.textContainer.maximumNumberOfLines = 3;
        solucaoTextView.isScrollEnabled = false;
        solicitanteTextField.delegate = self;
        defeitoTextView.delegate = self;
        defeitoTextView.isScrollEnabled = false;
        defeitoTextView.textFieldStyle();
        defeitoTextView.textContainer.maximumNumberOfLines = 3;
        
        situacaoPicker.tag = 0;
        situacaoPicker.delegate = self;
        situacaoTextField.inputView = situacaoPicker;
        situacaoTextField.text = "Selecione a situação";
        situacaoTextField.dropDownStyle();
        
        clienteTextField.text = "Cliente não localizado";
        clienteTextField.isUserInteractionEnabled = false;
        
        tecnicoPicker.tag = 2;
        tecnicoPicker.delegate = self;
        tecnicoTextField.delegate = self;
        tecnicoTextField.inputView = tecnicoPicker;
        tecnicoTextField.dropDownStyle();
        
        setupArriveDatePicker();
        setupArriveTimePicker();
        setupEndTimePicker();
        
        equipamentoTextView.delegate = self;
        equipamentoTextView.isScrollEnabled = false
        equipamentoTextView.isEditable = true
        equipamentoTextView.isUserInteractionEnabled = true
        equipamentoTextView.isSelectable = true
        equipamentoTextView.dropDownStyle();
        
        genericAlert.view.backgroundColor = UIColor.black;
        genericAlert.view.alpha = 0.6;
        genericAlert.view.layer.cornerRadius = 15;
        genericAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        saveAlert.view.backgroundColor = UIColor.black;
        saveAlert.view.alpha = 0.6;
        saveAlert.view.layer.cornerRadius = 15;
        saveAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        setupLoadingSpin();
        
    }
    
    func setupKeyBoardToolBar(){
        toolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30));
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(
            title: "OK", style: .done, target: self,
            action: #selector(doneButtonAction)
            //action: Selector(("doneButtonAction"))
        );
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        self.situacaoTextField.inputAccessoryView = toolbar;
        self.clienteTextField.inputAccessoryView = toolbar;
        self.tecnicoTextField.inputAccessoryView = toolbar;
        self.defeitoTextView.inputAccessoryView = toolbar;
        self.solucaoTextView.inputAccessoryView = toolbar;
        self.solicitanteTextField.inputAccessoryView = toolbar;
    }
    
    
    func loadFromRealm(){
        customerSelected = Customer.loadFromId(id: customerId);
        if(selectedId.count > 0){
            if(editMode){
                appointmentModel = ServiceOrder.loadFromId(id: selectedId);
            }
        }
        
        tecnicoList = Tecnico.loadAllFromRealmActive();
        if( tecnicoList != nil && (tecnicoList?.count ?? 0) > 0){
            tecnicoPickerData = Array(tecnicoList!);
        }
        
        equipamentoList = ProductStock.loadAllFromRealmActive();
        if( equipamentoList != nil && (equipamentoList?.count ?? 0) > 0){
            equipamentoData = Array(equipamentoList!);
        }
        
    }
    
    func loadData(){
        loadFromRealm();
    }
    
    func hideKeyboard(){
        self.situacaoTextField.resignFirstResponder();
        self.clienteTextField.resignFirstResponder();
        self.tecnicoTextField.resignFirstResponder();
        self.defeitoTextView.resignFirstResponder();
        self.solucaoTextView.resignFirstResponder();
        self.solicitanteTextField.resignFirstResponder();
    }
    
    func enableInteraction(_ enable: Bool){
        self.view.isUserInteractionEnabled = enable;
        self.saveButton.isEnabled = enable;
        self.navigationController?.navigationBar.isUserInteractionEnabled = enable;
    }
    
    
    func saveMode(_ on: Bool){
        self.enableInteraction(!on);
        if(on){
            saveLoading.startAnimating();
        }else{
            saveLoading.stopAnimating();
        }
    }
    
    func setupCustomerMode(){
        self.customerMode = (Session.getUsuarioLogadoTipo() == Constants.USUARIO_TIPO_CLIENTE_FINAL);
        self.deleteButton.isEnabled = (Session.getUsuarioLogadoTipo() == Constants.USUARIO_TIPO_CLIENTE_MEDICO) || (Session.getUsuarioLogadoTipo() == Constants.USUARIO_TIPO_MASTER);
        self.arquiveButton.isEnabled = (Session.getUsuarioLogadoTipo() != Constants.USUARIO_TIPO_CLIENTE_FINAL);
        self.saveButton.isEnabled = (Session.getUsuarioLogadoTipo() == Constants.USUARIO_TIPO_CLIENTE_MEDICO) || (Session.getUsuarioLogadoTipo() == Constants.USUARIO_TIPO_MASTER);
        if( Session.getUsuarioLogadoTipo() != Constants.USUARIO_TIPO_CLIENTE_MEDICO) && (Session.getUsuarioLogadoTipo() != Constants.USUARIO_TIPO_MASTER){
            self.saveButton.tintColor = UIColor.clear;
            self.deleteButton.tintColor = UIColor.clear;
            self.arquiveButton.tintColor = UIColor.clear;
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "selectProductStock") {
            let nextVC = segue.destination as! ProductStockToSelectTbVC;
            nextVC.customerId = self.customerId;
        }
        if (segue.identifier == "selectProductByQrCode") {
            let nextVC = segue.destination as! QRCodeScannerVC;
            nextVC.vcFrom = String(describing: type(of: self));
        }
    }
    
    
    func createAppointmentToSave(_ uuid: String?) -> ServiceOrder {
        
        var tempObject: ServiceOrder!;
        if (uuid != nil && uuid == "" ) {
            tempObject = ServiceOrder();
        } else {
            tempObject = ServiceOrder(
                dataAtual: Date(), id: uuid!,
                codigoUsuario: Session.getUsuarioLogadoId(),
                descricaoUsuario: Session.getUsuarioLogadoDescricao()
            );
            tempObject.codLocal = customerSelected!.id;
            tempObject.descricaoLocal = customerSelected!.nome;
        }
        
        if(!editMode){
            tempObject.tipoDescricao = "(Cliente) ";
            tempObject.tipoLocal = "C";
            tempObject.codSituacao = situacaoSelecionada;
            tempObject.descricaoSituacao = descricaoSituacaoSelecionada;
        }
        tempObject.equipamentosIds.removeAll();
        tempObject.equipamentosIds.append(equipamentoSelecionado)
        tempObject.descricaoEquipamento = productSelected!.descricaoEquipamento;
        tempObject.descricaoProblema = defeitoTextView.text ?? "";
        tempObject.descricaoQuemSolicitou = solicitanteTextField.text ?? "";
        
        if (!customerMode){
            tempObject.codSituacao = situacaoSelecionada;
            tempObject.descricaoSituacao = descricaoSituacaoSelecionada;
            tempObject.descricaoSolucao = solucaoTextView.text ?? "";
            tempObject.codTecnico = tecnicoSelecionado;
            tempObject.descricaoTecnico = technicianSelected != nil ? technicianSelected!.nome : "";
            
            
            if((prevChegaTecnicoDataTF.text?.count ?? 0) > 0){
                let tempDate = UtilEssencial.getDateFromStr(prevChegaTecnicoDataTF.text!);
                if(tempDate != nil){
                    tempObject.beginDate = tempDate;
                    tempObject.beginDateTimeStamp = Double(tempDate!.toEpochInt64());
                }
            }else{
                tempObject.beginDate = nil;
                tempObject.beginDateTimeStamp = 0.0;
            }
            tempObject.hourBegin = prevChegaTecnicoHoraTF.text ?? "";
            
            
            if((quandoAcabaDataTF.text?.count ?? 0) > 0){
                let tempDate = UtilEssencial.getDateFromStr(quandoAcabaDataTF.text!);
                if(tempDate != nil){
                    tempObject.endDate = tempDate;
                    tempObject.endDateTimeStamp = Double(tempDate!.toEpochInt64());
                }
            }else{
                tempObject.endDate = nil;
                tempObject.endDateTimeStamp = 0.0;
            }
            tempObject.hourEnd = quandoAcabaHoraTF.text ?? "";
            
            if((quandoPegouDataTF.text?.count ?? 0) > 0){
                let tempDate = UtilEssencial.getDateFromStr(quandoPegouDataTF.text!);
                if(tempDate != nil){
                    tempObject.getEquipmentDate = tempDate;
                    tempObject.getEquipmentDateTimeStamp = Double(tempDate!.toEpochInt64());
                }
            }else{
                tempObject.getEquipmentDate = nil;
                tempObject.getEquipmentDateTimeStamp = 0.0;
            }
            
            if((quandoEnviouFabricaDataTF.text?.count ?? 0) > 0){
                let tempDate = UtilEssencial.getDateFromStr(quandoEnviouFabricaDataTF.text!);
                if(tempDate != nil){
                    tempObject.sendFabricDate = tempDate;
                    tempObject.sendFabricDateTimeStamp = Double(tempDate!.toEpochInt64());
                }
            }else{
                tempObject.sendFabricDate = nil;
                tempObject.sendFabricDateTimeStamp = 0.0;
            }
            
            if((quandoChegouFabricaDataTF.text?.count ?? 0) > 0){
                let tempDate = UtilEssencial.getDateFromStr(quandoChegouFabricaDataTF.text!);
                if(tempDate != nil){
                    tempObject.arriveFabricDate = tempDate;
                    tempObject.arriveFabricDateTimeStamp = Double(tempDate!.toEpochInt64());
                }
            }else{
                tempObject.arriveFabricDate = nil;
                tempObject.arriveFabricDateTimeStamp = 0.0;
            }
            
            tempObject.sendFabricProtocol = numConhecimentoEnvio.text ?? "";
            
            if((quandoOsFabricaDataTF.text?.count ?? 0) > 0){
                let tempDate = UtilEssencial.getDateFromStr(quandoOsFabricaDataTF.text!);
                if(tempDate != nil){
                    tempObject.orderFabricDate = tempDate;
                    tempObject.orderFabricDateTimeStamp = Double(tempDate!.toEpochInt64());
                }
            }else{
                tempObject.orderFabricDate = nil;
                tempObject.orderFabricDateTimeStamp = 0.0;
            }
            
            tempObject.orderFabricProtocol = numOrcamentoAssistencia.text ?? "";
            tempObject.receiveFabricProtocol = numConhecimentoRetorno.text ?? "";
            
            if((quandoChegouEmpresaDataTF.text?.count ?? 0) > 0){
                let tempDate = UtilEssencial.getDateFromStr(quandoChegouEmpresaDataTF.text!);
                if(tempDate != nil){
                    tempObject.arriveCompanyDate = tempDate;
                    tempObject.arriveCompanyDateTimeStamp = Double(tempDate!.toEpochInt64());
                }
            }else{
                tempObject.arriveCompanyDate = nil;
                tempObject.arriveCompanyDateTimeStamp = 0.0;
            }
            
            
            if((quandoClienteAprovouDataTF.text?.count ?? 0) > 0){
                let tempDate = UtilEssencial.getDateFromStr(quandoClienteAprovouDataTF.text!);
                if(tempDate != nil){
                    tempObject.customerApprovalDate = tempDate;
                    tempObject.customerApprovalDateTimeStamp = Double(tempDate!.toEpochInt64());
                }
            }else{
                tempObject.customerApprovalDate = nil;
                tempObject.customerApprovalDateTimeStamp = 0.0;
            }
            
            if((quandoChegouClienteDataTF.text?.count ?? 0) > 0){
                let tempDate = UtilEssencial.getDateFromStr(quandoChegouClienteDataTF.text!);
                if(tempDate != nil){
                    tempObject.customerReceiveDate = tempDate;
                    tempObject.customerReceiveDateTimeStamp = Double(tempDate!.toEpochInt64());
                }
            }else{
                tempObject.customerReceiveDate = nil;
                tempObject.customerReceiveDateTimeStamp = 0.0;
            }
            
            if((quandoPrevDevoFabricaDataTF.text?.count ?? 0) > 0){
                let tempDate = UtilEssencial.getDateFromStr(quandoPrevDevoFabricaDataTF.text!);
                if(tempDate != nil){
                    tempObject.scheduledFabricDate = tempDate;
                    tempObject.scheduledFabricDateTimeStamp = Double(tempDate!.toEpochInt64());
                }
            }else{
                tempObject.scheduledFabricDate = nil;
                tempObject.scheduledFabricDateTimeStamp = 0.0;
            }
            
        }
        
        return tempObject;
    }
    
    
    func updateOnRealm() {
        
        if (internetOn()) {
            
            /* M = medico , E = enfermagem, T = tecnico,  @ = ultra,master,essencial */
            if (Session.getUsuarioLogadoTipo() == Constants.USUARIO_TIPO_CLIENTE_MEDICO
                || Session.getUsuarioLogadoTipo() == Constants.USUARIO_TIPO_MASTER ) {
                
                updateOnRealmIfMedic();
                
            } else if (Session.getUsuarioLogadoTipo() == Constants.USUARIO_TIPO_CLIENTE_FINAL) {
                
                if (appointmentModel?.codSituacao == "3" || appointmentModel?.codSituacao == "4") {
                    msgCantEditObject(false);
                } else {
                    updateOnRealmIfCustomer();
                }
            } else {
                self.showAlert(vc: self, "Tipo do usuário não definido, faça login novamente.", "OK");
            }
        }
    }
    
    func insertOnRealm() {
        
        if (internetOn()) {
            
            self.saveMode(true);
            let tempDadosDaTela: ServiceOrder = createAppointmentToSave(UUID().uuidString);
            
            tempDadosDaTela.criado_local = true;
            tempDadosDaTela.modificaAntesAtualizar(
                Session.getUsuarioLogadoId(), Session.getUsuarioLogadoDescricao(), Date()
            );
            
            ServiceOrder.add(objectToSave: tempDadosDaTela);
            
            Customer.modifyBeforeUpdate(
                id: customerSelected!.id,
                usuarioCod: Session.getUsuarioLogadoId(), usuarioDescricao: Session.getUsuarioLogadoDescricao(), dataInformada: Date()
            );
            
            self.syncServer();
            //            self.showToastAndReturnScreen(message: "Ordem de serviço salvo", seconds: 2, closeView: true);
            
        }
        
    }
    
    
    func updateOnRealmIfCustomer() {
        
        if (internetOn()) {
            
            //            self.saveMode(true);
            //            let tempDadosDaTela: ServiceOrder = createAppointmentToSave("");
            //            let success = ServiceOrder.updateIfCustomer(id: appointmentModel!.id, objectToSave: tempDadosDaTela);
            //
            //            Customer.modifyBeforeUpdate(
            //                id: customerSelected!.id,
            //                usuarioCod: Session.getUsuarioLogadoId(), usuarioDescricao: Session.getUsuarioLogadoDescricao(), dataInformada: Date()
            //            );
            //
            //            if(success){
            //                self.syncServer();
            //            }else{
            //                self.showToastAndReturnScreen(message: "Erro ao tentar atualizar o chamado no celular", seconds: 2, closeView: false);
            //            }
            //
        }
        
    }
    
    func updateOnRealmIfMedic() {
        
        if (internetOn()) {
            
            self.saveMode(true);
            let tempDadosDaTela: ServiceOrder = createAppointmentToSave("");
            let success = ServiceOrder.updateIfMedic(id: appointmentModel!.id, objectToSave: tempDadosDaTela);
            
            Customer.modifyBeforeUpdate(
                id: customerSelected!.id,
                usuarioCod: Session.getUsuarioLogadoId(), usuarioDescricao: Session.getUsuarioLogadoDescricao(), dataInformada: Date()
            );
            
            if(success){
                self.syncServer();
            }else{
                self.showToastAndReturnScreen(message: "Erro ao tentar atualizar o chamado no celular", seconds: 2, closeView: false);
            }
            
        }
        
    }
    
    func arquiveOnRealmIfMedic() {
        
        if (internetOn() && self.editMode) {
            
            self.saveMode(true);
            
            let success = ServiceOrder.updateToArchiveOrActive(
                id: appointmentModel!.id, active: !appointmentModel!.ativo
            );
            
            Customer.modifyBeforeUpdate(
                id: customerSelected!.id,
                usuarioCod: Session.getUsuarioLogadoId(), usuarioDescricao: Session.getUsuarioLogadoDescricao(), dataInformada: Date()
            );
            
            if((success) != nil){
                self.syncServer();
            }else{
                self.showToastAndReturnScreen(message: "Erro ao tentar atualizar o chamado no celular", seconds: 2, closeView: false);
            }
            
        }
        
    }
    
    
    
    func deleteOnRealmIfMedic() {
        
        if (internetOn() && self.editMode) {
            
            self.saveMode(true);
            let success = ServiceOrder.updateToDelete(id: appointmentModel!.id, active: !appointmentModel!.deletado);
            
            Customer.modifyBeforeUpdate(
                id: customerSelected!.id,
                usuarioCod: Session.getUsuarioLogadoId(), usuarioDescricao: Session.getUsuarioLogadoDescricao(), dataInformada: Date()
            );
            
            if((success) != nil){
                self.syncServer();
            }else{
                self.showToastAndReturnScreen(message: "Erro ao tentar atualizar o chamado no celular", seconds: 2, closeView: false);
            }
            
        }
        
    }
    
    
    
    @objc func syncServer(){
        syncProcess.getDefaultSyncRx(token: Session.getToken(), serviceKey: Session.getServiceKey(),
                                     appCustomerId:  Session.getUsuarioLogadoCodCliente(), tipoUsuario: Session.getUsuarioLogadoTipo(),
                                     beginTimeStamp: Session.getKeySincronizacaoTimestampUltimaString()
        )
            .observeOn(MainScheduler.instance)
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(
                onNext: { el in
            },
                onError: { err in
                    if(self.editMode){
                        self.showToastAndReturnScreen(message: "Erro ao tentar atualizar a ordem de serviço", seconds: 2, closeView: false);
                    }else{
                        self.showToastAndReturnScreen(message: "Erro ao tentar salvar a ordem de serviço", seconds: 2, closeView: false);
                    }
            },
                onCompleted: { () in
                    self.setLastSync();
                    if(self.editMode){
                        if(self.appointmentModel?.deletado == true){
                            self.showToastAndReturnScreen(
                                message: "Ordem de serviço deletado", seconds: 2, closeView: true
                            );
                        }else{
                            self.showToastAndReturnScreen(
                                message: "Ordem de serviço atualizado", seconds: 2, closeView: false
                            );
                        }
                    }else{
                        self.showToastAndReturnScreen(message: "Ordem de serviço salvo", seconds: 2, closeView: true);
                    }
            },
                onDisposed: { () in
            }
        ).disposed(by: disposeBag);
        
    }
    
    func setLastSync(){
        UserDefaults.standard.set((0), forKey: Session.KEY_SINCRONIZACAO_TIMESTAMP_SERVER);
        let tempSyncTsServer: Double = Session.getSincronizacaoTsUltimaServer();
        let dateToSave: Date = Date();
        if (tempSyncTsServer == 0 || tempSyncTsServer < Session.getKeySincronizacaoTimestampUltima()) {
            UserDefaults.standard.set((dateToSave.getDeviceDateDisplayBrWithHour()), forKey: Session.KEY_SINCRONIZACAO_DATA_ULTIMA);
            UserDefaults.standard.set((dateToSave.toEpochInt64()), forKey: Session.KEY_SINCRONIZACAO_TIMESTAMP);
        } else {
            UserDefaults.standard.set((dateToSave.getDeviceDateDisplayBrWithHour()), forKey: Session.KEY_SINCRONIZACAO_DATA_ULTIMA);
            UserDefaults.standard.set((dateToSave.toEpochInt64()), forKey: Session.KEY_SINCRONIZACAO_TIMESTAMP);
        }
    }
    
    func setupArriveDatePicker(){
        let loc = Locale(identifier: "pt_BR");
        self.previsaoChegadaDP.locale = loc;
        self.previsaoChegadaDP.datePickerMode = .date;
        let toolbar = UIToolbar();
        toolbar.sizeToFit();
        let doneButton = UIBarButtonItem(title: "Selecionar", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil);
        let cancelButton = UIBarButtonItem(title: "Fechar", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false);
        self.prevChegaTecnicoDataTF.inputAccessoryView = toolbar;
        self.prevChegaTecnicoDataTF.inputView = self.previsaoChegadaDP;
        
        let toolbarA = UIToolbar();
        toolbarA.sizeToFit();
        let doneButtonA = UIBarButtonItem(title: "Selecionar", style: .plain, target: self, action: #selector(donedatePickerA));
        let spaceButtonA = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil);
        let cancelButtonA = UIBarButtonItem(title: "Fechar", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbarA.setItems([cancelButtonA,spaceButtonA,doneButtonA], animated: false)
        self.quandoAcabaDataDP.locale = loc;
        self.quandoAcabaDataDP.datePickerMode = .date;
        self.quandoAcabaDataTF.inputAccessoryView = toolbarA;
        self.quandoAcabaDataTF.inputView = self.quandoAcabaDataDP;
        
        
        let cancelButtonB = UIBarButtonItem(title: "Fechar", style: .plain, target: self, action: #selector(cancelDatePicker));
        let cancelButtonC = UIBarButtonItem(title: "Fechar", style: .plain, target: self, action: #selector(cancelDatePicker));
        let cancelButtonD = UIBarButtonItem(title: "Fechar", style: .plain, target: self, action: #selector(cancelDatePicker));
        let cancelButtonE = UIBarButtonItem(title: "Fechar", style: .plain, target: self, action: #selector(cancelDatePicker));
        let cancelButtonF = UIBarButtonItem(title: "Fechar", style: .plain, target: self, action: #selector(cancelDatePicker));
        let cancelButtonG = UIBarButtonItem(title: "Fechar", style: .plain, target: self, action: #selector(cancelDatePicker));
        let cancelButtonH = UIBarButtonItem(title: "Fechar", style: .plain, target: self, action: #selector(cancelDatePicker));
        let cancelButtonI = UIBarButtonItem(title: "Fechar", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        
        let toolbarB = UIToolbar();
        toolbarB.sizeToFit();
        let doneButtonB = UIBarButtonItem(title: "Selecionar", style: .plain, target: self, action: #selector(donedatePickerB));
        toolbarB.setItems([cancelButtonB,spaceButton,doneButtonB], animated: false)
        self.quandoPegouDataDP.locale = loc;
        self.quandoPegouDataDP.datePickerMode = .date;
        self.quandoPegouDataTF.inputAccessoryView = toolbarB;
        self.quandoPegouDataTF.inputView = self.quandoPegouDataDP;
        
        let toolbarC = UIToolbar();
        toolbarC.sizeToFit();
        let doneButtonC = UIBarButtonItem(title: "Selecionar", style: .plain, target: self, action: #selector(donedatePickerC));
        toolbarC.setItems([cancelButtonC,spaceButton,doneButtonC], animated: false)
        self.quandoEnviouFabricaDataDP.locale = loc;
        self.quandoEnviouFabricaDataDP.datePickerMode = .date;
        self.quandoEnviouFabricaDataTF.inputAccessoryView = toolbarC;
        self.quandoEnviouFabricaDataTF.inputView = self.quandoEnviouFabricaDataDP;
        
        
        let toolbarD = UIToolbar();
        toolbarD.sizeToFit();
        let doneButtonD = UIBarButtonItem(title: "Selecionar", style: .plain, target: self, action: #selector(donedatePickerD));
        toolbarD.setItems([cancelButtonD,spaceButton,doneButtonD], animated: false)
        self.quandoChegouFabricaDataDP.locale = loc;
        self.quandoChegouFabricaDataDP.datePickerMode = .date;
        self.quandoChegouFabricaDataTF.inputAccessoryView = toolbarD;
        self.quandoChegouFabricaDataTF.inputView = self.quandoChegouFabricaDataDP;
        
        
        let toolbarE = UIToolbar();
        toolbarE.sizeToFit();
        let doneButtonE = UIBarButtonItem(title: "Selecionar", style: .plain, target: self, action: #selector(donedatePickerE));
        toolbarE.setItems([cancelButtonE,spaceButton,doneButtonE], animated: false)
        self.quandoOsFabricaDataDP.locale = loc;
        self.quandoOsFabricaDataDP.datePickerMode = .date;
        self.quandoOsFabricaDataTF.inputAccessoryView = toolbarE;
        self.quandoOsFabricaDataTF.inputView = self.quandoOsFabricaDataDP;
        
        
        let toolbarF = UIToolbar();
        toolbarF.sizeToFit();
        let doneButtonF = UIBarButtonItem(title: "Selecionar", style: .plain, target: self, action: #selector(donedatePickerF));
        toolbarF.setItems([cancelButtonF,spaceButton,doneButtonF], animated: false)
        self.quandoClienteAprovouDataDP.locale = loc;
        self.quandoClienteAprovouDataDP.datePickerMode = .date;
        self.quandoClienteAprovouDataTF.inputAccessoryView = toolbarF;
        self.quandoClienteAprovouDataTF.inputView = self.quandoClienteAprovouDataDP;
        
        
        let toolbarG = UIToolbar();
        toolbarG.sizeToFit();
        let doneButtonG = UIBarButtonItem(title: "Selecionar", style: .plain, target: self, action: #selector(donedatePickerG));
        toolbarG.setItems([cancelButtonG,spaceButton,doneButtonG], animated: false)
        self.quandoPrevDevoFabricaDataDP.locale = loc;
        self.quandoPrevDevoFabricaDataDP.datePickerMode = .date;
        self.quandoPrevDevoFabricaDataTF.inputAccessoryView = toolbarG;
        self.quandoPrevDevoFabricaDataTF.inputView = self.quandoPrevDevoFabricaDataDP;
        
        
        let toolbarH = UIToolbar();
        toolbarH.sizeToFit();
        let doneButtonH = UIBarButtonItem(title: "Selecionar", style: .plain, target: self, action: #selector(donedatePickerH));
        toolbarH.setItems([cancelButtonH,spaceButton,doneButtonH], animated: false)
        self.quandoChegouEmpresaDataDP.locale = loc;
        self.quandoChegouEmpresaDataDP.datePickerMode = .date;
        self.quandoChegouEmpresaDataTF.inputAccessoryView = toolbarH;
        self.quandoChegouEmpresaDataTF.inputView = self.quandoChegouEmpresaDataDP;
        
        
        
        let toolbarI = UIToolbar();
        toolbarI.sizeToFit();
        let doneButtonI = UIBarButtonItem(title: "Selecionar", style: .plain, target: self, action: #selector(donedatePickerI));
        toolbarI.setItems([cancelButtonI,spaceButton,doneButtonI], animated: false)
        self.quandoChegouClienteDataDP.locale = loc;
        self.quandoChegouClienteDataDP.datePickerMode = .date;
        self.quandoChegouClienteDataTF.inputAccessoryView = toolbarI;
        self.quandoChegouClienteDataTF.inputView = self.quandoChegouClienteDataDP;
        
        
        
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter();
        formatter.dateFormat = "dd/MM/yyyy";
        self.prevChegaTecnicoDataTF.text = formatter.string(from: self.previsaoChegadaDP.date);
        self.view.endEditing(true);
    }
    @objc func donedatePickerA(){
        let formatter = DateFormatter();
        formatter.dateFormat = "dd/MM/yyyy";
        self.quandoAcabaDataTF.text = formatter.string(from: self.quandoAcabaDataDP.date);
        self.view.endEditing(true);
    }
    @objc func donedatePickerB(){
        let formatter = DateFormatter();
        formatter.dateFormat = "dd/MM/yyyy";
        self.quandoPegouDataTF.text = formatter.string(from: self.quandoPegouDataDP.date);
        self.view.endEditing(true);
    }
    @objc func donedatePickerC(){
        let formatter = DateFormatter();
        formatter.dateFormat = "dd/MM/yyyy";
        self.quandoEnviouFabricaDataTF.text = formatter.string(from: self.quandoEnviouFabricaDataDP.date);
        self.view.endEditing(true);
    }
    @objc func donedatePickerD(){
        let formatter = DateFormatter();
        formatter.dateFormat = "dd/MM/yyyy";
        self.quandoChegouFabricaDataTF.text = formatter.string(from: self.quandoChegouFabricaDataDP.date);
        self.view.endEditing(true);
    }
    @objc func donedatePickerE(){
        let formatter = DateFormatter();
        formatter.dateFormat = "dd/MM/yyyy";
        self.quandoOsFabricaDataTF.text = formatter.string(from: self.quandoOsFabricaDataDP.date);
        self.view.endEditing(true);
    }
    @objc func donedatePickerF(){
        let formatter = DateFormatter();
        formatter.dateFormat = "dd/MM/yyyy";
        self.quandoClienteAprovouDataTF.text = formatter.string(from: self.quandoClienteAprovouDataDP.date);
        self.view.endEditing(true);
    }
    @objc func donedatePickerG(){
        let formatter = DateFormatter();
        formatter.dateFormat = "dd/MM/yyyy";
        self.quandoPrevDevoFabricaDataTF.text = formatter.string(from: self.quandoPrevDevoFabricaDataDP.date);
        self.view.endEditing(true);
    }
    @objc func donedatePickerH(){
        let formatter = DateFormatter();
        formatter.dateFormat = "dd/MM/yyyy";
        self.quandoChegouEmpresaDataTF.text = formatter.string(from: self.quandoChegouEmpresaDataDP.date);
        self.view.endEditing(true);
    }
    @objc func donedatePickerI(){
        let formatter = DateFormatter();
        formatter.dateFormat = "dd/MM/yyyy";
        self.quandoChegouClienteDataTF.text = formatter.string(from: self.quandoChegouClienteDataDP.date);
        self.view.endEditing(true);
    }
    @objc func cancelDatePicker(){
        self.view.endEditing(true);
    }
    func setupArriveTimePicker(){
        let loc = Locale(identifier: "pt_BR");
        self.previsaoChegadaHoraDP.locale = loc;
        self.previsaoChegadaHoraDP.datePickerMode = .time;
        let toolbar = UIToolbar();
        toolbar.sizeToFit();
        let doneButton = UIBarButtonItem(title: "Selecionar", style: .plain, target: self, action: #selector(doneTimePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil);
        let cancelButton = UIBarButtonItem(title: "Fechar", style: .plain, target: self, action: #selector(cancelTimePicker));
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false);
        self.prevChegaTecnicoHoraTF.inputAccessoryView = toolbar;
        self.prevChegaTecnicoHoraTF.inputView = self.previsaoChegadaHoraDP;
    }
    func setupEndTimePicker(){
        let loc = Locale(identifier: "pt_BR");
        self.quandoAcabaHoraDP.locale = loc;
        self.quandoAcabaHoraDP.datePickerMode = .time;
        let toolbar = UIToolbar();
        toolbar.sizeToFit();
        let doneButton = UIBarButtonItem(title: "Selecionar", style: .plain, target: self, action: #selector(doneTimePickerA));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil);
        let cancelButton = UIBarButtonItem(title: "Fechar", style: .plain, target: self, action: #selector(cancelTimePicker));
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false);
        self.quandoAcabaHoraTF.inputAccessoryView = toolbar;
        self.quandoAcabaHoraTF.inputView = self.quandoAcabaHoraDP;
    }
    
    
    @objc func doneTimePicker(){
        let formatter = DateFormatter();
        formatter.dateFormat = "HH:mm";
        self.prevChegaTecnicoHoraTF.text = formatter.string(from: self.previsaoChegadaHoraDP.date)
        self.view.endEditing(true);
    }
    @objc func doneTimePickerA(){
        let formatter = DateFormatter();
        formatter.dateFormat = "HH:mm";
        self.quandoAcabaHoraTF.text = formatter.string(from: self.quandoAcabaHoraDP.date)
        self.view.endEditing(true);
    }
    
    @objc func cancelTimePicker(){
        self.view.endEditing(true);
    }
    
    func createErroMsg(_ errors: String, _ newError: String) -> String {
        if (errors.count > 0){
            return errors + "\n" + newError;
        }else{
            return errors + " " + newError;
        }
    }
    
    func setSituacaoText(_ txt: String){
        self.situacaoTextField.text = txt + (
            (appointmentModel?.ativo ?? true) == false ? " (Arquivado)" : ""
        );
    }
    
    func internetOn() -> Bool{
        if(!isInternetOn){
            showAlert(vc: self, "Sem internet não é possível realizar a operação.", nil);
        }
        return isInternetOn;
    }
    func isFieldsValidToSave() -> Bool{
        var hasErros = false;
        var errors = "";
        
        if(situacaoSelecionada == "" || situacaoSelecionada == "0"){
            hasErros = true;
            errors = createErroMsg(errors, "Selecione a situação");
        }
        
        if(equipamentoSelecionado == ""){
            errors = createErroMsg(errors, "Selecione um equipamento.");
            hasErros = true;
        }
        
        if(defeitoTextView.text.count == 0){
            errors = createErroMsg(errors, "Informe o defeito.");
            hasErros = true;
        }
        
        //        if( !customerMode && (tecnicoSelecionado == "" || tecnicoSelecionado == "0") ){
        //            errors = createErroMsg(errors, "Selecione o técnico.");
        //            hasErros = true;
        //        }
        
        if(customerId == ""){
            errors = createErroMsg(errors, "Selecione o cliente.");
            hasErros = true;
        }
        
        if(editMode){
            if(selectedId == ""){
                errors = createErroMsg(errors, "Ordem de serviço sem código.");
                hasErros = true;
            }
        }
        
        if(hasErros){
            self.showAlert(vc: self, errors, "OK");
        }
        
        return !hasErros;
    }
    
    @IBAction func saveAction(_ sender: Any) {
        if(isFieldsValidToSave()){
            if(editMode){
                updateOnRealm();
            }else{
                insertOnRealm();
            }
        }
    }
    
    @IBAction func deleteAction(_ sender: Any) {
        deleteOnRealmIfMedic();
    }
    
    @IBAction func arquiveAction(_ sender: Any) {
        arquiveOnRealmIfMedic();
    }
    
    @IBAction func selectProdFromQrCode(_ sender: Any) {
        self.performSegue(withIdentifier: "selectProductByQrCode", sender: self)
    }
    
    @IBAction func getSelectedProdFromQRCode(_ unwindSegue: UIStoryboardSegue) {
        var prodFound: Bool = false;
        let qRCodeScannerVC = unwindSegue.source as? QRCodeScannerVC;
        equipamentoSelecionado = qRCodeScannerVC?.selectedProductId ?? "";
        qRCodeScannerVC?.dismiss(animated: false, completion: nil);
        qRCodeScannerVC?.closeView();
        
        for eqIndex in 0..<equipamentoData.count {
            if( equipamentoSelecionado == equipamentoData[eqIndex].id ){
                equipamentoTextView.text = equipamentoData[eqIndex].getDescricaoTela();
                productSelected = equipamentoData[eqIndex];
                prodFound = true;
                break
            }
        }
        if(!prodFound){
            productSelected = nil;
            equipamentoTextView.text = "Selecione o equipamento";
            equipamentoSelecionado = "";
            showAlert(vc: self, "Nenhum equipamento encontrado com este QRCode.", nil);
        }else{
            showAlert(vc: self, "Equipamento selecionado.", nil);
        }
    }
    
    @IBAction func getSelectedProduct(_ unwindSegue: UIStoryboardSegue) {
        let productStockToSelectTbVC = unwindSegue.source as? ProductStockToSelectTbVC;
        equipamentoSelecionado = productStockToSelectTbVC?.selectedProductId ?? "";
        for eqIndex in 0..<equipamentoData.count {
            if( equipamentoSelecionado == equipamentoData[eqIndex].id ){
                equipamentoTextView.text = equipamentoData[eqIndex].getDescricaoTela();
                productSelected = equipamentoData[eqIndex];
                break
            }
        }
    }
    
    // MARK: OBJC
    @objc func doneButtonAction() {
        self.view.endEditing(true)
    }
    
    @objc func returnTextView(gesture: UIGestureRecognizer) {
        guard (activeField != nil || activeTextView != nil) else {
            return
        }
        activeField?.resignFirstResponder()
        activeField = nil;
        activeTextView?.resignFirstResponder()
        activeTextView = nil;
    }
    
    // MARK: UIPicker
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 0 {
            return situacaoPickerData.count;
        }else  if pickerView.tag == 2 {
            return tecnicoPickerData.count;
        }else{
            return 0;
        }
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0 {
            return situacaoPickerData[row].description;
        }else  if pickerView.tag == 2 {
            return tecnicoPickerData[row].nome;
        }else{
            return "";
        }
    }
    
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if (pickerView.tag == 0) {
            //situacaoTextField.text = situacaoPickerData[row].description;
            self.setSituacaoText(situacaoPickerData[row].description);
            situacaoSelecionada = situacaoPickerData[row].id;
            descricaoSituacaoSelecionada = situacaoPickerData[row].description;
        }else if (pickerView.tag == 2) {
            tecnicoTextField.text = tecnicoPickerData[row].nome;
            tecnicoSelecionado = tecnicoPickerData[row].id;
            technicianSelected = tecnicoPickerData[row];
        }
    }
    
    func showAlert( vc: UIViewController, _ message: String,_ okBtnTitle: String?){
        vc.present(genericAlert, animated: true, completion: nil);
        genericAlert.setValue(message, forKey: "message");
        if(okBtnTitle != nil){
            genericAlert.actions[0].setValue((okBtnTitle ?? "OK"), forKey: "title");
        }
    }
    
    func showToastAndReturnScreen(message: String, seconds: Double, closeView: Bool) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert);
        alert.view.backgroundColor = UIColor.black;
        alert.view.alpha = 0.6;
        alert.view.layer.cornerRadius = 15;
        self.present(alert, animated: true);
        self.saveMode(false);
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds, execute: {
            [weak self] in
            guard let self = self else { return }
            alert.dismiss(animated: true);
            self.saveLoading.stopAnimating();
            if(closeView){
                self.closeView();
            }
        });
    }
    
    
    func msgCantEditObject(_ oldObject:Bool){
        self.showAlert(vc: self, "Não é possível editar um chamado finalizado ou cancelado", "OK");
    }
    
    func setupLoadingSpin(){
        saveLoading.center = view.center;
        saveLoading.color = UIColor.white;
        saveLoading.hidesWhenStopped = true;
        saveLoading.layer.cornerRadius = 05;
        saveLoading.isOpaque = false;
        saveLoading.backgroundColor = UIColor.black;
        saveLoading.alpha = 0.3;
        view.addSubview(saveLoading);
    }
    
    func closeView(){
        self.navigationController?.popViewController(animated: true);
        self.dismiss(animated: true, completion: nil);
    }
    
    func showToast(message: String, seconds: Double) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert);
        alert.view.backgroundColor = UIColor.black;
        alert.view.alpha = 0.6;
        alert.view.layer.cornerRadius = 15;
        self.present(alert, animated: true);
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds){
            alert.dismiss(animated: true);
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil);
    }
    
    
}



// MARK: UITextFieldDelegate
extension ServiceOrderVC: UITextFieldDelegate, UITextViewDelegate {
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeField = textField;
        lastOffset = self.scrollView.contentOffset;
        return true;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        activeField?.resignFirstResponder();
        activeField = nil;
        return true;
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        activeTextView = textView;
        lastOffset = self.scrollView.contentOffset;
        if(textView == self.equipamentoTextView){
            activeField?.resignFirstResponder();
            self.performSegue(withIdentifier: "selectProductStock", sender: self)
            return false;
        }else{
            return true;
        }
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        activeTextView?.resignFirstResponder();
        activeTextView = nil;
        return true;
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return (textField != self.situacaoTextField && textField != self.tecnicoTextField);
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if( textView == self.equipamentoTextView){
            return false;
        }else{
            let existingLines = textView.text.components(separatedBy: CharacterSet.newlines)
            let newLines = text.components(separatedBy: CharacterSet.newlines)
            let linesAfterChange = existingLines.count + newLines.count - 1
            if(text == "\n") {
                return linesAfterChange <= textView.textContainer.maximumNumberOfLines
            }
            let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
            let numberOfChars = newText.count
            return numberOfChars <= 100;
        }
    }
}


// MARK: Keyboard Handling
extension ServiceOrderVC {
    
    @objc func keyboardWillShow(aNotification: NSNotification) {
        let info = aNotification.userInfo!;
        let kbSize: CGSize = (
            (info["UIKeyboardFrameEndUserInfoKey"] as? CGRect)?.size
            )!;
        let contentInsets: UIEdgeInsets = UIEdgeInsets(
            top: 0.0, left: 0.0, bottom: kbSize.height, right: 0.0
        );
        var frame: CGRect!;
        if(activeTextView != nil){
            frame = activeTextView!.frame;
        }
        if(activeField != nil){
            frame = activeField!.frame;
        }
        scrollView.contentInset = contentInsets;
        scrollView.scrollIndicatorInsets = contentInsets;
        var aRect: CGRect = self.view.frame;
        aRect.size.height -= kbSize.height;
        if (frame != nil){
            if !aRect.contains(frame.origin) {
                self.scrollView.scrollRectToVisible(frame, animated: true);
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.3) {
            self.appointmentConstraintContentHeight.constant -= (self.keyboardHeight ?? 0);
            if(self.lastOffset != nil){
                self.scrollView.contentOffset = self.lastOffset;
            }
        }
        keyboardHeight = nil;
    }
}
