//
//  ServiceOrderResponse.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 26/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation

struct ServiceOrderRequest: Codable {
    
    var id: String;
    var codigo: String?;
    var codigoQuemCriou: String;
    var codigoQuemAtualizou: String?;
    var criadoEmTimeStamp: Double;
    var criadoEm: Date;
    var criadoPor: String;
    var atualizadoPor: String?;
    var observacao: String?;
    var ativo: Bool;
    var deletado: Bool;
    var servidorAtualizadoEm: Date;
    var beginDate: Date;
    var beginDateTimeStamp: Double;
    var hourBegin: String;
    var endDate: Date?;
    var endDateTimeStamp: Double?;
    var hourEnd: String?;
    var osFinalizada: Bool?;
    var getEquipmentDate: Date?;
    var sendFabricDate: Date?;
    var arriveFabricDate: Date?;
    var arriveFabricDateTimeStamp: Double?;
    var sendFabricProtocol: String?;
    var orderFabricDate: Date?;
    var orderFabricDateTimeStamp: Double?;
    var orderFabricProtocol: String?;
    var receiveFabricProtocol: String?;
    var arriveCompanyDate: Date?;
    var arriveCompanyDateTimeStamp: Double?;
    var customerApprovalDate: Date?;
    var customerApprovalDateTimeStamp: Double?;
    var customerReceiveDate: Date?;
    var customerReceiveDateTimeStamp: Double?;
    var codOS: String?;
    var scheduledFabricDate: Date?;
    var getEquipmentDateTimeStamp: Double?;
    var sendFabricDateTimeStamp: Double?;
    var scheduledFabricDateTimeStamp: Double?;
    var atualizadoEm: Date;
    var atualizadoEmTimeStamp: Double;
    var servidorAtualizadoEmTimeStamp: Double;
    var codSituacao: String;
    var descricaoSituacao: String;
    var descricaoProblema: String;
    var customerProtocol: String?;
    var codTecnico: String?;
    var descricaoTecnico: String?;
    var codLocal: String;
    var descricaoLocal: String;
    var tipoLocal: String;
    var tipoDescricao: String;
    var equipamentosIds: [String];
    var codQuemRetirouEquipamento: String?;
    var descricaoQuemRetirouEquipamento: String?;
    var codQuemSolicitou: String;
    var descricaoQuemSolicitou: String;
    var servicoSolicitadoDesc: String?;
    var requestDate: Date?;
    var requestDateTimeStamp: Double?;
    var descricaoEquipamento: String;
    var descricaoSolucao: String;
    
    enum CodingKeys: String, CodingKey {
        
        case id;
        case codigo;
        case codigoQuemCriou;
        case codigoQuemAtualizou;
        case criadoEmTimeStamp;
        case criadoEm;
        case criadoPor;
        case atualizadoPor;
        case observacao;
        case ativo;
        case deletado;
        case servidorAtualizadoEm;
        case beginDate;
        case beginDateTimeStamp;
        case hourBegin;
        case endDate;
        case endDateTimeStamp;
        case hourEnd;
        case osFinalizada;
        case getEquipmentDate;
        case sendFabricDate;
        case arriveFabricDate;
        case arriveFabricDateTimeStamp;
        case sendFabricProtocol;
        case orderFabricDate;
        case orderFabricDateTimeStamp;
        case orderFabricProtocol;
        case receiveFabricProtocol;
        case arriveCompanyDate;
        case arriveCompanyDateTimeStamp;
        case customerApprovalDate;
        case customerApprovalDateTimeStamp;
        case customerReceiveDate;
        case customerReceiveDateTimeStamp;
        case codOS;
        case scheduledFabricDate;
        case getEquipmentDateTimeStamp;
        case sendFabricDateTimeStamp;
        case scheduledFabricDateTimeStamp;
        case atualizadoEm;
        case atualizadoEmTimeStamp;
        case servidorAtualizadoEmTimeStamp;
        case codSituacao;
        case descricaoSituacao;
        case descricaoProblema;
        case customerProtocol;
        case codTecnico;
        case descricaoTecnico;
        case codLocal;
        case descricaoLocal;
        case tipoLocal;
        case tipoDescricao;
        case equipamentosIds;
        case codQuemRetirouEquipamento;
        case descricaoQuemRetirouEquipamento;
        case codQuemSolicitou;
        case descricaoQuemSolicitou;
        case servicoSolicitadoDesc;
        case requestDate;
        case requestDateTimeStamp;
        case descricaoEquipamento;
        case descricaoSolucao;
    }
    

}


