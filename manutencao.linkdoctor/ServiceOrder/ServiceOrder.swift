//
//  ServiceOrder.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 06/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class ServiceOrder: Object {
    
    @objc dynamic var id: String = "";
    
    @objc dynamic var codigo: String = "";
    @objc dynamic var codigoQuemAtualizou: String = "";
    @objc dynamic var beginDate: Date?;
    @objc dynamic var beginDateTimeStamp: Double = 0.0;
    @objc dynamic var hourBegin: String = "";
    @objc dynamic var endDate: Date?;
    @objc dynamic var endDateTimeStamp: Double = 0.0;
    @objc dynamic var hourEnd: String = "";
    @objc dynamic var osFinalizada: Bool = false;
    @objc dynamic var getEquipmentDate: Date?;
    @objc dynamic var sendFabricDate: Date?;
    @objc dynamic var arriveFabricDate: Date!;
    @objc dynamic var arriveFabricDateTimeStamp: Double = 0.0;
    @objc dynamic var sendFabricProtocol: String = "";
    @objc dynamic var orderFabricDate: Date?;
    @objc dynamic var orderFabricDateTimeStamp: Double = 0.0;
    @objc dynamic var orderFabricProtocol: String = "";
    @objc dynamic var receiveFabricProtocol: String = "";
    @objc dynamic var arriveCompanyDate: Date?;
    @objc dynamic var arriveCompanyDateTimeStamp: Double = 0.0;
    @objc dynamic var customerApprovalDate: Date!;
    @objc dynamic var customerApprovalDateTimeStamp: Double = 0.0;
    @objc dynamic var customerReceiveDate: Date?;
    @objc dynamic var customerReceiveDateTimeStamp: Double = 0.0;
    @objc dynamic var scheduledFabricDate: Date?;
    @objc dynamic var getEquipmentDateTimeStamp: Double = 0.0;
    @objc dynamic var sendFabricDateTimeStamp: Double = 0.0;
    @objc dynamic var scheduledFabricDateTimeStamp: Double = 0.0;
    @objc dynamic var customerProtocol: String = "";
    @objc dynamic var codQuemRetirouEquipamento: String = "";
    @objc dynamic var descricaoQuemRetirouEquipamento: String = "";
    @objc dynamic var servicoSolicitadoDesc: String = "";
    @objc dynamic var requestDate: Date!;
    @objc dynamic var requestDateTimeStamp: Double = 0.0;
    let equipamentosIds = List<String>();
    @objc dynamic var ativo: Bool = false;
    @objc dynamic var atualizadoEm: Date!;
    @objc dynamic var atualizadoEmTimeStamp: Double = 0.0;
    @objc dynamic var codLocal: String = "";
    @objc dynamic var codOS: String = "";
    @objc dynamic var codSituacao: String = "";
    @objc dynamic var codigoQuemCriou: String = "";
    @objc dynamic var criadoEm: Date!;
    @objc dynamic var criadoEmTimeStamp: Double = 0.0;
    @objc dynamic var criadoPor: String = "";
    @objc dynamic var criado_local: Bool = false;
    @objc dynamic var deletado: Bool = false;
    @objc dynamic var descricaoLocal: String = "";
    @objc dynamic var codQuemSolicitou: String?;
    @objc dynamic var codTecnico: String?;
    @objc dynamic var descricaoTecnico: String?;
    @objc dynamic var descricaoQuemSolicitou: String?;
    @objc dynamic var descricaoProblema: String = "";
    @objc dynamic var observacao: String?;
    @objc dynamic var descricaoSituacao: String = "";
    @objc dynamic var servidorAtualizadoEm: Date!;
    @objc dynamic var servidorAtualizadoEmTimeStamp: Double = 0.0;
    @objc dynamic var tipoDescricao: String = "";
    @objc dynamic var tipoLocal: String = "";
    @objc dynamic var descricaoEquipamento: String = "";
    @objc dynamic var descricaoSolucao: String = "";
    
    @objc dynamic var atualizadoPor: String = "";
    @objc dynamic var precisaSincronizar: Bool = false;
    
    @objc dynamic var app_version_code: Int = 0;
    @objc dynamic var app_version_name: String = "";
    // @objc dynamic var equipamentosIds: [String] = [];
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(
        id: String,
        ativo: Bool,
        atualizadoEm: Date?,
        atualizadoEmTimeStamp: Double,
        codLocal: String,
        codOS: String,
        codSituacao: String,
        codigoQuemCriou: String,
        criadoEm: Date?,
        criadoEmTimeStamp: Double,
        criadoPor: String,
        deletado: Bool,
        descricaoLocal: String,
        codQuemSolicitou: String?,
        codTecnico: String?,
        descricaoTecnico: String?,
        descricaoQuemSolicitou: String?,
        descricaoProblema: String,
        observacao: String?,
        descricaoSituacao: String,
        servidorAtualizadoEm: Date,
        servidorAtualizadoEmTimeStamp: Double,
        tipoDescricao: String,
        tipoLocal: String,
        equipamentosIds: [String],
        codigo: String?,
        codigoQuemAtualizou: String,
        beginDate: Date,
        beginDateTimeStamp: Double,
        hourBegin: String,
        endDate: Date?,
        endDateTimeStamp: Double?,
        hourEnd: String,
        osFinalizada: Bool,
        getEquipmentDate: Date?,
        sendFabricDate: Date?,
        arriveFabricDate: Date?,
        arriveFabricDateTimeStamp: Double?,
        sendFabricProtocol: String,
        orderFabricDate: Date?,
        orderFabricDateTimeStamp: Double?,
        orderFabricProtocol: String,
        receiveFabricProtocol: String,
        arriveCompanyDate: Date?,
        arriveCompanyDateTimeStamp: Double?,
        customerApprovalDate: Date?,
        customerApprovalDateTimeStamp: Double?,
        customerReceiveDate: Date?,
        customerReceiveDateTimeStamp: Double?,
        scheduledFabricDate: Date?,
        getEquipmentDateTimeStamp: Double?,
        sendFabricDateTimeStamp: Double?,
        scheduledFabricDateTimeStamp: Double?,
        customerProtocol: String,
        codQuemRetirouEquipamento: String,
        descricaoQuemRetirouEquipamento: String,
        servicoSolicitadoDesc: String,
        requestDate: Date?,
        requestDateTimeStamp: Double?,
        descricaoEquipamento: String,
        descricaoSolucao: String,
        atualizadoPor: String?,
        precisaSincronizar: Bool?
        
        ) {
        
        self.init();
        self.id = id;
        self.ativo = ativo;
        self.atualizadoEm = atualizadoEm;
        self.atualizadoEmTimeStamp = atualizadoEmTimeStamp;
        self.codLocal = codLocal;
        self.codOS = codOS;
        self.codSituacao = codSituacao;
        self.codigoQuemCriou = codigoQuemCriou;
        self.criadoEm = criadoEm;
        self.criadoEmTimeStamp = criadoEmTimeStamp;
        self.criadoPor = criadoPor;
        self.deletado = deletado;
        self.descricaoLocal = descricaoLocal;
        self.codQuemSolicitou = codQuemSolicitou ?? "";
        self.codTecnico = codTecnico ?? "";
        self.descricaoTecnico = descricaoTecnico ?? "";
        self.descricaoQuemSolicitou = descricaoQuemSolicitou ?? "";
        self.observacao = observacao ?? "";
        self.descricaoProblema = descricaoProblema;
        self.descricaoSituacao = descricaoSituacao;
        self.servidorAtualizadoEm = servidorAtualizadoEm;
        self.servidorAtualizadoEmTimeStamp = servidorAtualizadoEmTimeStamp;
        self.tipoDescricao = tipoDescricao;
        self.tipoLocal = tipoLocal;
        self.equipamentosIds.removeAll();
        self.equipamentosIds.append(objectsIn: Array(equipamentosIds));
        self.codigoQuemAtualizou = codigoQuemAtualizou;
        self.beginDate = beginDate;
        self.beginDateTimeStamp = beginDateTimeStamp;
        self.hourBegin = hourBegin;
        self.endDate = endDate;
        self.endDateTimeStamp = endDateTimeStamp ?? 0.0;
        self.hourEnd = hourEnd;
        self.osFinalizada = osFinalizada;
        self.getEquipmentDate = getEquipmentDate;
        self.sendFabricDate = sendFabricDate;
        self.arriveFabricDate = arriveFabricDate;
        self.arriveFabricDateTimeStamp = arriveFabricDateTimeStamp ?? 0.0;
        self.sendFabricProtocol = sendFabricProtocol;
        self.orderFabricDate = orderFabricDate;
        self.orderFabricDateTimeStamp = orderFabricDateTimeStamp ?? 0.0;
        self.orderFabricProtocol = orderFabricProtocol;
        self.receiveFabricProtocol = receiveFabricProtocol;
        self.arriveCompanyDate = arriveCompanyDate;
        self.arriveCompanyDateTimeStamp = arriveCompanyDateTimeStamp ?? 0.0;
        self.customerApprovalDate = customerApprovalDate;
        self.customerApprovalDateTimeStamp = customerApprovalDateTimeStamp ?? 0.0;
        self.customerReceiveDate = customerReceiveDate;
        self.customerReceiveDateTimeStamp = customerReceiveDateTimeStamp ?? 0.0;
        self.scheduledFabricDate = scheduledFabricDate;
        self.getEquipmentDateTimeStamp = getEquipmentDateTimeStamp ?? 0.0;
        self.sendFabricDateTimeStamp = sendFabricDateTimeStamp ?? 0.0;
        self.scheduledFabricDateTimeStamp = scheduledFabricDateTimeStamp ?? 0.0;
        self.customerProtocol = customerProtocol;
        self.tipoDescricao = tipoDescricao;
        self.codQuemRetirouEquipamento = codQuemRetirouEquipamento;
        self.descricaoQuemRetirouEquipamento = descricaoQuemRetirouEquipamento;
        self.servicoSolicitadoDesc = servicoSolicitadoDesc;
        self.requestDate = requestDate;
        self.requestDateTimeStamp = requestDateTimeStamp ?? 0.0;
        self.descricaoEquipamento = descricaoEquipamento;
        self.descricaoSolucao = descricaoSolucao;
        self.atualizadoPor = atualizadoPor ?? "" ;
        self.precisaSincronizar = precisaSincronizar ?? false ;
        
    }
    
    
    convenience init(
         id: String,
         ativo: Bool,
         atualizadoEm: Date?,
         atualizadoEmTimeStamp: Double,
         codLocal: String,
         codOS: String,
         codSituacao: String,
         codigoQuemCriou: String,
         criadoEm: Date?,
         criadoEmTimeStamp: Double,
         criadoPor: String,
         deletado: Bool,
         descricaoLocal: String,
         codQuemSolicitou: String?,
         codTecnico: String?,
         descricaoTecnico: String?,
         descricaoQuemSolicitou: String?,
         descricaoProblema: String,
         observacao: String?,
         descricaoSituacao: String,
         servidorAtualizadoEm: Date,
         servidorAtualizadoEmTimeStamp: Double
         ) {
         
         self.init();
         self.id = id;
         self.ativo = ativo;
         self.atualizadoEm = atualizadoEm;
         self.atualizadoEmTimeStamp = atualizadoEmTimeStamp;
         self.codLocal = codLocal;
         self.codOS = codOS;
         self.codSituacao = codSituacao;
         self.codigoQuemCriou = codigoQuemCriou;
         self.criadoEm = criadoEm;
         self.criadoEmTimeStamp = criadoEmTimeStamp;
         self.criadoPor = criadoPor;
         self.deletado = deletado;
         self.descricaoLocal = descricaoLocal;
         self.codQuemSolicitou = codQuemSolicitou ?? "";
         self.codTecnico = codTecnico ?? "";
         self.descricaoTecnico = descricaoTecnico ?? "";
         self.descricaoQuemSolicitou = descricaoQuemSolicitou ?? "";
         self.observacao = observacao ?? "";
         self.descricaoProblema = descricaoProblema;
         self.descricaoSituacao = descricaoSituacao;
         self.servidorAtualizadoEm = servidorAtualizadoEm;
         self.servidorAtualizadoEmTimeStamp = servidorAtualizadoEmTimeStamp;
         
     }
    
    convenience init(dataAtual: Date, id: String, codigoUsuario: String, descricaoUsuario: String ) {
           self.init();
           self.id = id;
           self.ativo = true;
           self.atualizadoEm = dataAtual;
           self.atualizadoPor = descricaoUsuario;
           self.codigoQuemAtualizou = codigoUsuario;
           self.codigoQuemCriou = codigoUsuario;
           self.criadoEm = dataAtual;
           self.criadoPor = descricaoUsuario;
           self.deletado = false;
           self.precisaSincronizar = true;
           self.criado_local = true;
           self.criadoEmTimeStamp = Double(dataAtual.toEpochInt64());
           self.atualizadoEmTimeStamp = Double(dataAtual.toEpochInt64());
           self.app_version_code = 1;
           self.app_version_name = "ios";
       }
       
       func modificaAntesAtualizar(_ usuarioCod: String, _ usuarioDescricao: String, _ dataInformada:Date) {
           self.codigoQuemAtualizou = usuarioCod;
           self.atualizadoPor = usuarioDescricao;
           self.atualizadoEm = dataInformada;
           self.precisaSincronizar = true;
           self.atualizadoEmTimeStamp = Double(Date().toEpochInt64());
           //this.app_version_code = Utilidades.getAppVersionCode();
           //this.app_version_name = Utilidades.getAppVersionName();
       }
       
    
    func getDatasDescricao() -> String {
        return "Início da O.S: " + UtilEssencial.toScreenOnBrFormatWithOutHour(date: self.beginDate!) + (self.hourBegin.count > 0 ? " as " + self.hourBegin : "")
            + " Término da O.S: " + UtilEssencial.toScreenOnBrFormatWithOutHour(date: self.endDate!) + (self.hourEnd.count > 0 ? " as " + self.hourEnd : "");
    }
    
    func getDataInicioOSDescricao() -> String {
        return "Início da O.S: " + UtilEssencial.toScreenOnBrFormatWithOutHour(date: self.beginDate!) + (self.hourBegin.count > 0 ? " as " + self.hourBegin : "");
    }
    
    func getDataFimOSDescricao() -> String {
        if let unEndDate = self.endDate {
            return "Término da O.S: " + UtilEssencial.toScreenOnBrFormatWithOutHour(date: unEndDate)
                + (self.hourEnd.count > 0  ? " as " + self.hourEnd : "");
        }else{
            return "Término da O.S: ";
        }
    }
    
    func getDescricaoPrimeiraLinhaCard() -> String {
        return self.getDataInicioOSDescricao() + " " + "\n" + self.getDataPrevisaoDevOSDescricao();
    }
    
    
    //return "Chamado aberto em " + getDataAbriuChamado() + ( "\nSolicitante: \(self.descricaoQuemSolicitou ?? " Não informado")" );
    func getDataPrevisaoDevOSDescricao() -> String {
        if let unWrapObj = self.scheduledFabricDate {
            return "Previsão devolução: " + UtilEssencial.toScreenOnBrFormatWithOutHour(date: unWrapObj);
        }else{
            return "Previsão devolução: ";
        }
    }
    
    func checkEquipamentsIds() -> String {
        if (self.equipamentosIds.count > 0) {
            return self.equipamentosIds[0];
        }
        return "";
    }
    
    func getEquipamentoInformado() -> String {
        return "Equipamento não informado";
    }
    
}
// MARK: - CRUD methods

extension ServiceOrder {
    
    
    static func convertResponseToObject(_ serviceOrder: ServiceOrderResponse) -> ServiceOrder {
        let obj: ServiceOrder = ServiceOrder(
            id: serviceOrder.id,
            ativo:  serviceOrder.ativo,
            atualizadoEm:  serviceOrder.atualizadoEm,
            atualizadoEmTimeStamp:  serviceOrder.atualizadoEmTimeStamp,
            codLocal:  serviceOrder.codLocal,
            codOS: serviceOrder.codOS ?? "",
            codSituacao: serviceOrder.codSituacao,
            codigoQuemCriou: serviceOrder.codigoQuemCriou,
            criadoEm:  serviceOrder.criadoEm,
            criadoEmTimeStamp: serviceOrder.criadoEmTimeStamp,
            criadoPor: serviceOrder.criadoPor,
            deletado: serviceOrder.deletado,
            descricaoLocal: serviceOrder.descricaoLocal,
            codQuemSolicitou: serviceOrder.codQuemSolicitou ,
            codTecnico: serviceOrder.codTecnico ,
            descricaoTecnico: serviceOrder.descricaoTecnico ,
            descricaoQuemSolicitou: serviceOrder.descricaoQuemSolicitou ,
            descricaoProblema: serviceOrder.descricaoProblema,
            observacao: serviceOrder.observacao ,
            descricaoSituacao: serviceOrder.descricaoSituacao,
            servidorAtualizadoEm: serviceOrder.servidorAtualizadoEm,
            servidorAtualizadoEmTimeStamp: serviceOrder.servidorAtualizadoEmTimeStamp
        );
        
        obj.tipoDescricao = serviceOrder.tipoDescricao;
        obj.tipoLocal = serviceOrder.tipoLocal;
        obj.equipamentosIds.removeAll();
        obj.equipamentosIds.append(objectsIn: Array(serviceOrder.equipamentosIds));
        obj.codigo = serviceOrder.codigo ?? "";
        obj.codigoQuemAtualizou = serviceOrder.codigoQuemAtualizou ?? "";
        obj.beginDate = serviceOrder.beginDate;
        obj.beginDateTimeStamp = serviceOrder.beginDateTimeStamp;
        obj.hourBegin = serviceOrder.hourBegin;
        obj.endDate = serviceOrder.endDate ?? nil;
        obj.endDateTimeStamp = serviceOrder.endDateTimeStamp ?? 0.0;
        obj.hourEnd = serviceOrder.hourEnd ?? "";
        obj.osFinalizada = serviceOrder.osFinalizada ?? false;
        obj.getEquipmentDate = serviceOrder.getEquipmentDate ?? nil;
        obj.sendFabricDate = serviceOrder.sendFabricDate ?? nil;
        obj.arriveFabricDate = serviceOrder.arriveFabricDate ?? nil;
        obj.arriveFabricDateTimeStamp = serviceOrder.arriveFabricDateTimeStamp ?? 0.0;
        obj.sendFabricProtocol = serviceOrder.sendFabricProtocol ?? "";
        obj.orderFabricDate = serviceOrder.orderFabricDate ?? nil;
        obj.orderFabricDateTimeStamp = serviceOrder.orderFabricDateTimeStamp ?? 0.0;
        obj.orderFabricProtocol = serviceOrder.orderFabricProtocol ?? "";
        obj.receiveFabricProtocol = serviceOrder.receiveFabricProtocol ?? "";
        obj.arriveCompanyDate = serviceOrder.arriveCompanyDate ?? nil;
        obj.arriveCompanyDateTimeStamp = serviceOrder.arriveCompanyDateTimeStamp ?? 0.0;
        obj.customerApprovalDate = serviceOrder.customerApprovalDate ?? nil;
        obj.customerApprovalDateTimeStamp = serviceOrder.customerApprovalDateTimeStamp ?? 0.0;
        obj.customerReceiveDate = serviceOrder.customerReceiveDate ?? nil;
        obj.customerReceiveDateTimeStamp = serviceOrder.customerReceiveDateTimeStamp ?? 0.0;
        obj.scheduledFabricDate = serviceOrder.scheduledFabricDate ?? nil;
        obj.getEquipmentDateTimeStamp = serviceOrder.getEquipmentDateTimeStamp ?? 0.0;
        obj.sendFabricDateTimeStamp = serviceOrder.sendFabricDateTimeStamp ?? 0.0;
        obj.scheduledFabricDateTimeStamp = serviceOrder.scheduledFabricDateTimeStamp ?? 0.0;
        obj.customerProtocol = serviceOrder.customerProtocol ?? "";
        obj.codQuemRetirouEquipamento = serviceOrder.codQuemRetirouEquipamento ?? "";
        obj.descricaoQuemRetirouEquipamento = serviceOrder.descricaoQuemRetirouEquipamento ?? "";
        obj.servicoSolicitadoDesc = serviceOrder.servicoSolicitadoDesc ?? "";
        obj.requestDate = serviceOrder.requestDate ?? nil;
        obj.requestDateTimeStamp = serviceOrder.requestDateTimeStamp ?? 0.0;
        obj.descricaoEquipamento = serviceOrder.descricaoEquipamento;
        obj.descricaoSolucao = serviceOrder.descricaoSolucao;
        obj.atualizadoPor = serviceOrder.atualizadoPor ?? "";
        
        return obj;
    }
    
    
    static func convertObjectToRequest(_ serviceOrder: ServiceOrder) -> ServiceOrderRequest {
    
        let obj: ServiceOrderRequest = ServiceOrderRequest(
             id: serviceOrder.id,
           codigo: serviceOrder.codigo,
           codigoQuemCriou: serviceOrder.codigoQuemCriou,
           codigoQuemAtualizou: serviceOrder.codigoQuemAtualizou,
           criadoEmTimeStamp: serviceOrder.criadoEmTimeStamp,
           criadoEm: serviceOrder.criadoEm,
           criadoPor: serviceOrder.criadoPor,
           atualizadoPor: serviceOrder.atualizadoPor,
           observacao: serviceOrder.observacao,
           ativo: serviceOrder.ativo,
           deletado: serviceOrder.deletado,
           servidorAtualizadoEm: serviceOrder.servidorAtualizadoEm,
           beginDate: serviceOrder.beginDate!,
           beginDateTimeStamp: serviceOrder.beginDateTimeStamp,
           hourBegin: serviceOrder.hourBegin,
           endDate: serviceOrder.endDate,
           endDateTimeStamp: serviceOrder.endDateTimeStamp,
           hourEnd: serviceOrder.hourEnd,
           osFinalizada: serviceOrder.osFinalizada,
           getEquipmentDate: serviceOrder.getEquipmentDate,
           sendFabricDate: serviceOrder.sendFabricDate,
           arriveFabricDate: serviceOrder.arriveFabricDate,
           arriveFabricDateTimeStamp: serviceOrder.arriveFabricDateTimeStamp,
           sendFabricProtocol: serviceOrder.sendFabricProtocol,
           orderFabricDate: serviceOrder.orderFabricDate,
           orderFabricDateTimeStamp: serviceOrder.orderFabricDateTimeStamp,
           orderFabricProtocol: serviceOrder.orderFabricProtocol,
           receiveFabricProtocol: serviceOrder.receiveFabricProtocol,
           arriveCompanyDate: serviceOrder.arriveCompanyDate,
           arriveCompanyDateTimeStamp: serviceOrder.arriveCompanyDateTimeStamp,
           customerApprovalDate: serviceOrder.customerApprovalDate,
           customerApprovalDateTimeStamp: serviceOrder.customerApprovalDateTimeStamp,
           customerReceiveDate: serviceOrder.customerReceiveDate,
           customerReceiveDateTimeStamp: serviceOrder.customerReceiveDateTimeStamp,
           codOS: serviceOrder.codOS,
           scheduledFabricDate: serviceOrder.scheduledFabricDate,
           getEquipmentDateTimeStamp: serviceOrder.getEquipmentDateTimeStamp,
           sendFabricDateTimeStamp: serviceOrder.sendFabricDateTimeStamp,
           scheduledFabricDateTimeStamp: serviceOrder.scheduledFabricDateTimeStamp,
           atualizadoEm: serviceOrder.atualizadoEm,
           atualizadoEmTimeStamp: serviceOrder.atualizadoEmTimeStamp,
           servidorAtualizadoEmTimeStamp: serviceOrder.servidorAtualizadoEmTimeStamp,
           codSituacao: serviceOrder.codSituacao,
           descricaoSituacao: serviceOrder.descricaoSituacao,
           descricaoProblema: serviceOrder.descricaoProblema,
           customerProtocol: serviceOrder.customerProtocol,
           codTecnico: serviceOrder.codTecnico,
           descricaoTecnico: serviceOrder.descricaoTecnico,
           codLocal: serviceOrder.codLocal,
           descricaoLocal: serviceOrder.descricaoLocal,
           tipoLocal: serviceOrder.tipoLocal,
           tipoDescricao: serviceOrder.tipoDescricao,
           equipamentosIds:  Array(serviceOrder.equipamentosIds),
           codQuemRetirouEquipamento: serviceOrder.codQuemRetirouEquipamento,
           descricaoQuemRetirouEquipamento: serviceOrder.descricaoQuemRetirouEquipamento,
           codQuemSolicitou: serviceOrder.codQuemSolicitou ?? "",
           descricaoQuemSolicitou: serviceOrder.descricaoQuemSolicitou ?? "",
           servicoSolicitadoDesc: serviceOrder.servicoSolicitadoDesc,
           requestDate: serviceOrder.requestDate,
           requestDateTimeStamp: serviceOrder.requestDateTimeStamp,
           descricaoEquipamento: serviceOrder.descricaoEquipamento,
           descricaoSolucao: serviceOrder.descricaoSolucao
           );
        return obj;
    }
    
    static func converListResponseToListObject(_ listToConvert: [ServiceOrderResponse]) -> [ServiceOrder] {
        var list: [ServiceOrder] = [];
        for obj in listToConvert {
            list.append(convertResponseToObject(obj));
        }
        return list;
    }
    
    
    static func converListObjectToListRequest(_ listToConvert: [ServiceOrder]) -> [ServiceOrderRequest] {
        var list: [ServiceOrderRequest] = [];
        for obj in listToConvert {
            list.append(convertObjectToRequest(obj));
        }
        return list;
    }
    
    static func loadNeedSync(in realm: Realm = try! Realm()) -> Results<ServiceOrder> {
        return realm.objects(ServiceOrder.self)
            .filter("\(Constants.CAMPO_PRECISA_SINCRONIZAR) = true")
    }
    
    static func loadAllFromRealm(in realm: Realm = try! Realm()) -> Results<ServiceOrder> {
        return realm.objects(ServiceOrder.self)
    }
    
    static func loadAllFromRealmActive(in realm: Realm = try! Realm()) -> Results<ServiceOrder> {
        return realm.objects(ServiceOrder.self)
            .filter("\(Constants.CAMPO_ATIVO) = true AND \(Constants.CAMPO_DELETADO) = false")
    }
    
    static func loadFromId(in realm: Realm = try! Realm(), id: String) -> ServiceOrder? {
        return realm.objects(ServiceOrder.self).filter("\(Constants.CAMPO_ID) = '\(id)'").first;
    }
    
    static func loadAllFromCustomerRealm(in realm: Realm = try! Realm(), customerId: String) -> Results<ServiceOrder> {
        return realm.objects(ServiceOrder.self)
            .filter("\(Constants.CAMPO_COD_LOCAL) = '\(customerId)' AND \(Constants.CAMPO_ATIVO) = true AND \(Constants.CAMPO_DELETADO) = false")
            .sorted(byKeyPath: Constants.CAMPO_CRIADO_EM_TIMESTAMP, ascending: false);
    }
    
    
    static func loadAllFromCustomerToSync(in realm: Realm = try! Realm(), customerId: String, createdLocal: Bool) -> Results<ServiceOrder> {
        return realm.objects(ServiceOrder.self)
            .filter("\(Constants.CAMPO_COD_LOCAL) = '\(customerId)' and \(Constants.CAMPO_CRIADO_LOCAL) = \(createdLocal)");
    }
    @discardableResult
    static func updateToArchiveOrActive(id: String, active: Bool, in realm: Realm = try! Realm())
        -> ServiceOrder? {
            let item: ServiceOrder? = realm.objects(ServiceOrder.self).filter("id = '\(id)'").first;
            if(item != nil){
                try! realm.write {
                    //realm.add(item, update: true);
                    item!.ativo = active;
                    item!.modificaAntesAtualizar(
                        Session.getUsuarioLogadoId(), Session.getUsuarioLogadoDescricao(), Date()
                    );
                }
            }
            return item;
    }
    
    @discardableResult
    static func updateToDelete(id: String, active: Bool, in realm: Realm = try! Realm())
        -> ServiceOrder? {
            let item: ServiceOrder? = realm.objects(ServiceOrder.self).filter("id = '\(id)'").first;
            if(item != nil){
                try! realm.write {
                    //realm.add(item, update: true);
                    item!.deletado = active;
                    item!.modificaAntesAtualizar(
                        Session.getUsuarioLogadoId(), Session.getUsuarioLogadoDescricao(), Date()
                    );
                }
            }
            return item;
    }
    
    @discardableResult
      static func updateIfMedic(id: String, objectToSave: ServiceOrder, in realm: Realm = try! Realm())
          -> Bool {
              
              let objectToUpdate = realm.objects(ServiceOrder.self).filter("\(Constants.CAMPO_ID) = '\(id)'").first;
              var updated = false;
              
              try! realm.write {
                  
                  objectToUpdate?.criado_local = false;
                  objectToUpdate?.codSituacao = objectToSave.codSituacao;
                  objectToUpdate?.descricaoSituacao = objectToSave.descricaoSituacao;
                  objectToUpdate?.descricaoProblema = objectToSave.descricaoProblema;
                  objectToUpdate?.codTecnico = objectToSave.codTecnico;
                  objectToUpdate?.descricaoTecnico = objectToSave.descricaoTecnico;
                  objectToUpdate?.descricaoQuemSolicitou = objectToSave.descricaoQuemSolicitou;
                  
                  objectToUpdate?.equipamentosIds.removeAll();
                  objectToUpdate?.equipamentosIds.append(objectsIn: Array(objectToSave.equipamentosIds));
                  
                  objectToUpdate?.descricaoEquipamento = objectToSave.descricaoEquipamento;
                  objectToUpdate?.descricaoSolucao = objectToSave.descricaoSolucao;
                  
                  objectToUpdate?.beginDate = objectToSave.beginDate;
                  objectToUpdate?.beginDateTimeStamp = objectToSave.beginDateTimeStamp;
                  objectToUpdate?.hourBegin = objectToSave.hourBegin;
                
                 objectToUpdate?.endDate = objectToSave.endDate;
                 objectToUpdate?.endDateTimeStamp = objectToSave.endDateTimeStamp;
                 objectToUpdate?.hourEnd = objectToSave.hourEnd;
                              
                  
                 objectToUpdate?.getEquipmentDate = objectToSave.getEquipmentDate;
                 objectToUpdate?.sendFabricDate = objectToSave.sendFabricDate;
                 objectToUpdate?.arriveFabricDate = objectToSave.arriveFabricDate;
                 objectToUpdate?.arriveFabricDateTimeStamp = objectToSave.arriveFabricDateTimeStamp;
//                @objc dynamic var osFinalizada: Bool = false;
                
                 objectToUpdate?.sendFabricProtocol = objectToSave.sendFabricProtocol;
                 objectToUpdate?.orderFabricDate = objectToSave.orderFabricDate;
                 objectToUpdate?.orderFabricDateTimeStamp = objectToSave.orderFabricDateTimeStamp;
                 objectToUpdate?.orderFabricProtocol = objectToSave.orderFabricProtocol;
                
                objectToUpdate?.receiveFabricProtocol = objectToSave.receiveFabricProtocol;
                objectToUpdate?.arriveCompanyDate = objectToSave.arriveCompanyDate;
                objectToUpdate?.arriveCompanyDateTimeStamp = objectToSave.arriveCompanyDateTimeStamp;
                objectToUpdate?.customerApprovalDate = objectToSave.customerApprovalDate;
                objectToUpdate?.customerApprovalDateTimeStamp = objectToSave.customerApprovalDateTimeStamp;
                
            
                objectToUpdate?.customerReceiveDate = objectToSave.customerReceiveDate;
                objectToUpdate?.customerReceiveDateTimeStamp = objectToSave.customerReceiveDateTimeStamp;
                objectToUpdate?.scheduledFabricDate = objectToSave.scheduledFabricDate;
                objectToUpdate?.getEquipmentDateTimeStamp = objectToSave.getEquipmentDateTimeStamp;
                
            
                objectToUpdate?.sendFabricDateTimeStamp = objectToSave.sendFabricDateTimeStamp;
                objectToUpdate?.scheduledFabricDateTimeStamp = objectToSave.scheduledFabricDateTimeStamp;
                objectToUpdate?.customerProtocol = objectToSave.customerProtocol;
                
                  objectToUpdate?.modificaAntesAtualizar(
                      Session.getUsuarioLogadoId(), Session.getUsuarioLogadoDescricao(), Date()
                  );
                  
                  updated = true;
              }
              
              return updated;
      }
      
    
    @discardableResult
    static func add(objectToSave: ServiceOrder, in realm: Realm = try! Realm())
        -> ServiceOrder {
            let item = objectToSave;
            try! realm.write {
                realm.add(item)
            }
            return item
    }
    
    @discardableResult
    static func addOrUpdate(objectToSave: ServiceOrder, in realm: Realm = try! Realm())
        -> ServiceOrder {
            let item = objectToSave;
            try! realm.write {
                realm.add(item, update: .all);
            }
            return item
    }
    
    @discardableResult
    static func addAll(listToSave: [ServiceOrder], in realm: Realm = try! Realm())
        -> [ServiceOrder] {
            realm.beginWrite();
            for user in listToSave
            {
                realm.add(user)
            }
            try! realm.commitWrite()
            return listToSave
    }
    @discardableResult
    static func addOrUpdateAll(listToSave: [ServiceOrder], in realm: Realm = try! Realm())
        -> [ServiceOrder] {
            //try! realm.beginWrite();
            realm.beginWrite();
            realm.add(listToSave, update: .all)
            try! realm.commitWrite()
            return listToSave
    }
    
    //    func toggleCompleted() {
    //        guard let realm = realm else { return }
    //        try! realm.write {
    //            isCompleted = !isCompleted
    //        }
    //    }
    //
    //    func delete() {
    //        guard let realm = realm else { return }
    //        try! realm.write {
    //            realm.delete(self)
    //        }
    //    }
}
