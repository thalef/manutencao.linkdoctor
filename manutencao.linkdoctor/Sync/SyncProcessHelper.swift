//
//  SyncProcessHelper.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 16/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation
import RealmSwift
import RxSwift
import RxCocoa

class SyncProcessHelper {
    
    private let loginService = LoginService();
    let disposeBag = DisposeBag();
    
    
    func getDefaultSyncRx(token: String, serviceKey: String,  appCustomerId: String,  tipoUsuario: String, beginTimeStamp: String) -> Observable<SyncModelsHeper> {
        
        if (tipoUsuario == Constants.USUARIO_TIPO_CLIENTE_FINAL) {
            
            return Observable.of(
                
                getCustomerAndSubDocsToSync(
                    token: token, serviceKey: serviceKey, beginTimeStamp: beginTimeStamp, userType: tipoUsuario,
                    appCustomerId: appCustomerId
                ),
                loginService.getTecnicosSyncRx(
                    request: loginService.getTecnicosSyncArrayRequest(beginTimeStamp: beginTimeStamp)
                ),
                loginService.getGenericTablesSyncArrayRx(
                    request: loginService.getGenericTableSyncArrayRequest(beginTimeStamp: beginTimeStamp)
                )
                ).merge()
                .do(
                    onNext: { doOnNext in
                        
                        if(doOnNext.typeSelected==String(describing: type(of: CustomerWithRecordsResponse.self))){
                            var listCustomerToConvert: [CustomerResponse] = [];
                            var listAppointmentToConvert: [AppointmentResponse] = [];
                            var listServiceOrderToConvert: [ServiceOrderResponse] = [];
                            var listProductStockToConvert: [ProductStockResponse] = [];
                            for records in doOnNext.listCustomerWithRecords{
                                listCustomerToConvert.append(records.customer);
                                listAppointmentToConvert.append(contentsOf: records.appointments);
                                listServiceOrderToConvert.append(contentsOf: records.servicesOrders);
                                listProductStockToConvert.append(contentsOf: records.productsInCustomer);
                            }
                            //--------------------- Customer ---------------//
                            let listCustomerToSave = Customer.converListResponseToListObject(listCustomerToConvert);
                            Customer.addOrUpdateAll(listToSave: listCustomerToSave);
                            //--------------------- Appoinment ---------------//
                            let listAppointmentToSave = Appointment.converListResponseToListObject(listAppointmentToConvert);
                            Appointment.addOrUpdateAll(listToSave: listAppointmentToSave);
                            //--------------------- Service Order ---------------//
                            let listServiceOrderToSave = ServiceOrder
                                .converListResponseToListObject(listServiceOrderToConvert);
                            ServiceOrder.addOrUpdateAll(listToSave: listServiceOrderToSave);
                            //--------------------- Product Stock ---------------//
                            let listProductStockToSave = ProductStock
                                .converListResponseToListObject(listProductStockToConvert);
                            ProductStock.addOrUpdateAll(listToSave: listProductStockToSave);
                        }
                        
                        if(doOnNext.typeSelected==String(describing: type(of: GenericTableResponse.self))){
                            
                            var listProductTypesToConvert: [ProductTypeResponse] = [];
                            var listBrandsToConvert: [BrandResponse] = [];
                            var listBrandTypeItemsToConvert: [BrandTypeItemResponse] = [];
                            
                            for records in doOnNext.listGenericTable{
                                listProductTypesToConvert.append(contentsOf: records.productTypes);
                                listBrandsToConvert.append(contentsOf: records.brands);
                                listBrandTypeItemsToConvert.append(contentsOf: records.brandTypeItems);
                            }
                            
                            let listProductTypesToSave = ProductType.converListResponseToListObject(listProductTypesToConvert);
                            ProductType.addOrUpdateAll(listToSave: listProductTypesToSave);
                            let listBrandsToSave = Brand.converListResponseToListObject(listBrandsToConvert);
                            Brand.addOrUpdateAll(listToSave: listBrandsToSave);
                            let listBrandTypeItemsToSave = BrandTypeItem.converListResponseToListObject(listBrandTypeItemsToConvert);
                            BrandTypeItem.addOrUpdateAll(listToSave: listBrandTypeItemsToSave);
                            
                        }
                        
                        if(doOnNext.typeSelected==String(describing: type(of: TecnicoResponse.self))){
                            let listTecnicoToSave = Tecnico.converListResponseToListObject(doOnNext.listTecnico);
                            Tecnico.addOrUpdateAll(listToSave: listTecnicoToSave);
                        }
                        
                        
                }).observeOn(ConcurrentDispatchQueueScheduler(qos: .background));
            
        }else{ // USUARIO MEDICO
            
            return Observable.of(
                
                loginService.getUsersRx(request: loginService.getUsersRxSyncRequest(
                    token: Session.getToken(), serviceKey: Session.getServiceKey(), beginTimeStamp: beginTimeStamp
                    )
                ),
                getCustomersAndSubDocsToSync(
                    token: token, serviceKey: serviceKey, beginTimeStamp: beginTimeStamp, userType: tipoUsuario
                ),
                loginService.getTecnicosSyncRx(
                    request: loginService.getTecnicosSyncArrayRequest(beginTimeStamp: beginTimeStamp)
                ),
                loginService.getGenericTablesSyncArrayRx(
                    request: loginService.getGenericTableSyncArrayRequest(beginTimeStamp: beginTimeStamp)
            ))
                .merge()
                .do(
                    onNext: { doOnNext in
                        
                        if(doOnNext.typeSelected==String(describing: type(of: LoginResponse.self))){
                            let listUserToSave = User.converListResponseToListObject(doOnNext.listUsers);
                            User.addOrUpdateAll(listToSave: listUserToSave);
                        }
                        if(doOnNext.typeSelected==String(describing: type(of: CustomerWithRecordsResponse.self))){
                            
                            var listCustomerToConvert: [CustomerResponse] = [];
                            var listAppointmentToConvert: [AppointmentResponse] = [];
                            var listServiceOrderToConvert: [ServiceOrderResponse] = [];
                            var listProductStockToConvert: [ProductStockResponse] = [];

                            for records in doOnNext.listCustomerWithRecords{
                                listCustomerToConvert.append(records.customer);
                                listAppointmentToConvert.append(contentsOf: records.appointments);
                                listServiceOrderToConvert.append(contentsOf: records.servicesOrders);
                                listProductStockToConvert.append(contentsOf: records.productsInCustomer);
                            }
                            
                            //--------------------- Customer ---------------//
                            let listCustomerToSave = Customer.converListResponseToListObject(listCustomerToConvert);
                            Customer.addOrUpdateAll(listToSave: listCustomerToSave);
                            //--------------------- Appoinment ---------------//
                            let listAppointmentToSave = Appointment.converListResponseToListObject(listAppointmentToConvert);
                            Appointment.addOrUpdateAll(listToSave: listAppointmentToSave);
                            //--------------------- Service Order ---------------//
                            let listServiceOrderToSave = ServiceOrder
                                .converListResponseToListObject(listServiceOrderToConvert);
                            ServiceOrder.addOrUpdateAll(listToSave: listServiceOrderToSave);
                            //--------------------- Product Stock ---------------//
                            let listProductStockToSave = ProductStock
                                .converListResponseToListObject(listProductStockToConvert);
                            ProductStock.addOrUpdateAll(listToSave: listProductStockToSave);
                        }
                        
                        if(doOnNext.typeSelected==String(describing: type(of: GenericTableResponse.self))){
                            
                            var listProductTypesToConvert: [ProductTypeResponse] = [];
                            var listBrandsToConvert: [BrandResponse] = [];
                            var listBrandTypeItemsToConvert: [BrandTypeItemResponse] = [];
                            
                            for records in doOnNext.listGenericTable{
                                listProductTypesToConvert.append(contentsOf: records.productTypes);
                                listBrandsToConvert.append(contentsOf: records.brands);
                                listBrandTypeItemsToConvert.append(contentsOf: records.brandTypeItems);
                            }
                            
                            let listProductTypesToSave = ProductType.converListResponseToListObject(listProductTypesToConvert);
                            ProductType.addOrUpdateAll(listToSave: listProductTypesToSave);
                            let listBrandsToSave = Brand.converListResponseToListObject(listBrandsToConvert);
                            Brand.addOrUpdateAll(listToSave: listBrandsToSave);
                            let listBrandTypeItemsToSave = BrandTypeItem.converListResponseToListObject(listBrandTypeItemsToConvert);
                            BrandTypeItem.addOrUpdateAll(listToSave: listBrandTypeItemsToSave);
                        }
                        if(doOnNext.typeSelected==String(describing: type(of: TecnicoResponse.self))){
                            let listTecnicoToSave = Tecnico.converListResponseToListObject(doOnNext.listTecnico);
                            Tecnico.addOrUpdateAll(listToSave: listTecnicoToSave);
                        }
                        
                }).observeOn(ConcurrentDispatchQueueScheduler(qos: .background));
            
        }
        
    }
    
    
    
    func getFirstTimeSyncRx(token: String, serviceKey: String,  appCustomerId: String,  tipoUsuario: String) -> Observable<SyncModelsHeper> {
        
        if (tipoUsuario == Constants.USUARIO_TIPO_CLIENTE_FINAL) {
            
            return Observable.of(
                loginService.getCustomerUniqueWithRecordsRx(
                    request: loginService.getCustomerUniqueWithRecordsRequest(id:appCustomerId)
                ),
                loginService.getTecnicosRx(
                    request: loginService.getTecnicosRequest()
                ),
                loginService.getGenericTablesArrayRx(request: loginService.getGenericTablesArrayRequest())
                ).merge()
                .do(
                    onNext: { doOnNext in
                        if(doOnNext.typeSelected==String(describing: type(of: CustomerWithRecordsResponse.self))){
                            var listCustomerToConvert: [CustomerResponse] = [];
                            var listAppointmentToConvert: [AppointmentResponse] = [];
                            var listServiceOrderToConvert: [ServiceOrderResponse] = [];
                            var listProductStockToConvert: [ProductStockResponse] = [];
                            for records in doOnNext.listCustomerWithRecords{
                                listCustomerToConvert.append(records.customer);
                                listAppointmentToConvert.append(contentsOf: records.appointments);
                                listServiceOrderToConvert.append(contentsOf: records.servicesOrders);
                                listProductStockToConvert.append(contentsOf: records.productsInCustomer);
                            }
                            //--------------------- Customer ---------------//
                            let listCustomerToSave = Customer.converListResponseToListObject(listCustomerToConvert);
                            Customer.addOrUpdateAll(listToSave: listCustomerToSave);
                            //--------------------- Appoinment ---------------//
                            let listAppointmentToSave = Appointment.converListResponseToListObject(listAppointmentToConvert);
                            Appointment.addOrUpdateAll(listToSave: listAppointmentToSave);
                            //--------------------- Service Order ---------------//
                            let listServiceOrderToSave = ServiceOrder
                                .converListResponseToListObject(listServiceOrderToConvert);
                            ServiceOrder.addOrUpdateAll(listToSave: listServiceOrderToSave);
                            //--------------------- Product Stock ---------------//
                            let listProductStockToSave = ProductStock
                                .converListResponseToListObject(listProductStockToConvert);
                            ProductStock.addOrUpdateAll(listToSave: listProductStockToSave);
                        }
                        
                        if(doOnNext.typeSelected==String(describing: type(of: GenericTableResponse.self))){
                            
                            var listProductTypesToConvert: [ProductTypeResponse] = [];
                            var listBrandsToConvert: [BrandResponse] = [];
                            var listBrandTypeItemsToConvert: [BrandTypeItemResponse] = [];
                            
                            for records in doOnNext.listGenericTable{
                                listProductTypesToConvert.append(contentsOf: records.productTypes);
                                listBrandsToConvert.append(contentsOf: records.brands);
                                listBrandTypeItemsToConvert.append(contentsOf: records.brandTypeItems);
                            }
                            
                            let listProductTypesToSave = ProductType.converListResponseToListObject(listProductTypesToConvert);
                            ProductType.addOrUpdateAll(listToSave: listProductTypesToSave);
                            let listBrandsToSave = Brand.converListResponseToListObject(listBrandsToConvert);
                            Brand.addOrUpdateAll(listToSave: listBrandsToSave);
                            let listBrandTypeItemsToSave = BrandTypeItem.converListResponseToListObject(listBrandTypeItemsToConvert);
                            BrandTypeItem.addOrUpdateAll(listToSave: listBrandTypeItemsToSave);
                            
                        }
                        
                        if(doOnNext.typeSelected==String(describing: type(of: TecnicoResponse.self))){
                            let listTecnicoToSave = Tecnico.converListResponseToListObject(doOnNext.listTecnico);
                            Tecnico.addOrUpdateAll(listToSave: listTecnicoToSave);
                        }
                        
                }).observeOn(ConcurrentDispatchQueueScheduler(qos: .background));
            
        }else{ // USUARIO MEDICO
            
            return Observable.of(
                
                loginService.getUsersRx(
                    request: loginService.getUsersRxRequest(token: Session.getToken(), serviceKey: Session.getServiceKey())
                ),
                loginService.getCustomersWithRecordsRx(request: loginService.getCustomersWithRecordsRequest()),
                loginService.getTecnicosRx(
                    request: loginService.getTecnicosRequest()
                ),
                loginService.getGenericTablesArrayRx(request: loginService.getGenericTablesArrayRequest())
                ).merge()
                .do(
                    onNext: { doOnNext in
                        
                        if(doOnNext.typeSelected==String(describing: type(of: LoginResponse.self))){
                            let listUserToSave = User.converListResponseToListObject(doOnNext.listUsers);
                            User.addOrUpdateAll(listToSave: listUserToSave);
                        }
                        if(doOnNext.typeSelected==String(describing: type(of: CustomerWithRecordsResponse.self))){
                            var listCustomerToConvert: [CustomerResponse] = [];
                            var listAppointmentToConvert: [AppointmentResponse] = [];
                            var listServiceOrderToConvert: [ServiceOrderResponse] = [];
                            var listProductStockToConvert: [ProductStockResponse] = [];
                            for records in doOnNext.listCustomerWithRecords{
                                listCustomerToConvert.append(records.customer);
                                listAppointmentToConvert.append(contentsOf: records.appointments);
                                listServiceOrderToConvert.append(contentsOf: records.servicesOrders);
                                listProductStockToConvert.append(contentsOf: records.productsInCustomer);
                            }
                            //--------------------- Customer ---------------//
                            let listCustomerToSave = Customer.converListResponseToListObject(listCustomerToConvert);
                            Customer.addOrUpdateAll(listToSave: listCustomerToSave);
                            //--------------------- Appoinment ---------------//
                            let listAppointmentToSave = Appointment.converListResponseToListObject(listAppointmentToConvert);
                            Appointment.addOrUpdateAll(listToSave: listAppointmentToSave);
                            //--------------------- Service Order ---------------//
                            let listServiceOrderToSave = ServiceOrder
                                .converListResponseToListObject(listServiceOrderToConvert);
                            ServiceOrder.addOrUpdateAll(listToSave: listServiceOrderToSave);
                            //--------------------- Product Stock ---------------//
                            let listProductStockToSave = ProductStock
                                .converListResponseToListObject(listProductStockToConvert);
                            ProductStock.addOrUpdateAll(listToSave: listProductStockToSave);
                            
                        }
                        
                        if(doOnNext.typeSelected==String(describing: type(of: GenericTableResponse.self))){
                            
                            var listProductTypesToConvert: [ProductTypeResponse] = [];
                            var listBrandsToConvert: [BrandResponse] = [];
                            var listBrandTypeItemsToConvert: [BrandTypeItemResponse] = [];
                            
                            for records in doOnNext.listGenericTable{
                                listProductTypesToConvert.append(contentsOf: records.productTypes);
                                listBrandsToConvert.append(contentsOf: records.brands);
                                listBrandTypeItemsToConvert.append(contentsOf: records.brandTypeItems);
                            }
                            
                            let listProductTypesToSave = ProductType.converListResponseToListObject(listProductTypesToConvert);
                            ProductType.addOrUpdateAll(listToSave: listProductTypesToSave);
                            let listBrandsToSave = Brand.converListResponseToListObject(listBrandsToConvert);
                            Brand.addOrUpdateAll(listToSave: listBrandsToSave);
                            let listBrandTypeItemsToSave = BrandTypeItem.converListResponseToListObject(listBrandTypeItemsToConvert);
                            BrandTypeItem.addOrUpdateAll(listToSave: listBrandTypeItemsToSave);
                        }
                        
                        if(doOnNext.typeSelected==String(describing: type(of: TecnicoResponse.self))){
                            let listTecnicoToSave = Tecnico.converListResponseToListObject(doOnNext.listTecnico);
                            Tecnico.addOrUpdateAll(listToSave: listTecnicoToSave);
                        }
                        
                }).observeOn(ConcurrentDispatchQueueScheduler(qos: .background));
        }
        
    }
    
    func getCustomerAndSubDocsToSync( token: String, serviceKey: String, beginTimeStamp: String, userType: String, appCustomerId: String) -> Single<SyncModelsHeper> {
        return Single<SyncModelsHeper>.create { single -> Disposable in
           _ = self.loadCustomerWithRecordToSyncFromRealm(userType: userType)
                .flatMap { (customerWithRecordSyncRequest) -> Single<SyncModelsHeper> in
                    return self.loginService.postCustomersWithRecordsRx(
                        request: self.loginService.postCustomerUniqueWithRecordsRequest(
                            id: appCustomerId,  body: customerWithRecordSyncRequest, beginTimeStamp: beginTimeStamp)
                    );
                }.subscribe(
                    onSuccess: { el in
                        single(.success((el)));
                },
                    onError: { err in
                        single(.error(err));
                }
            )
            return Disposables.create();
        }
    }
    
    func getCustomersAndSubDocsToSync( token: String, serviceKey: String, beginTimeStamp: String, userType: String) -> Single<SyncModelsHeper> {
        return Single<SyncModelsHeper>.create { single -> Disposable in
            _ = self.loadCustomerWithRecordToSyncFromRealm(userType: userType)
                .flatMap { (customerWithRecordSyncRequest) -> Single<SyncModelsHeper> in
                    return self.loginService.postCustomersWithRecordsRx(
                        request: self.loginService.postCustomersWithRecordsRequest(
                            body: customerWithRecordSyncRequest, beginTimeStamp: beginTimeStamp)
                    );
                }.subscribe(
                    onSuccess: { el in
                        single(.success((el)));
                },
                    onError: { err in
                        single(.error(err));
                }
            )
            return Disposables.create();
        }
    }
    
    func loadCustomerWithRecordToSyncFromRealm(userType: String) -> Single<CustomerWithRecordSyncRequest> {

        return Single<CustomerWithRecordSyncRequest>.create { single -> Disposable in
            let result: CustomerWithRecordSyncRequest = CustomerWithRecordSyncRequest();
            let customersToSync: Results<Customer> = Customer.loadNeedSync();
            let appointmentsToSync: Results<Appointment> = Appointment.loadNeedSync();
            let serviceOrdersToSync: Results<ServiceOrder> = ServiceOrder.loadNeedSync();
            let patientToSyncSize: Int = customersToSync.count;
            
            for i in 0..<patientToSyncSize {
                if (customersToSync[i].criado_local) {
                    result.customersToInsert.append(
                        self.createCustomerWithRecordSync(
                            userType: userType, customer: customersToSync[i], appointmentsToSync: appointmentsToSync, serviceOrdersToSync: serviceOrdersToSync
                        )
                    )
                } else {
                    result.customersToUpdate.append(
                        self.createCustomerWithRecordSync(
                            userType: userType, customer: customersToSync[i], appointmentsToSync: appointmentsToSync, serviceOrdersToSync: serviceOrdersToSync
                        )
                    )
                }
            }
            single(.success(result));
            return Disposables.create();
        }
    }
    
    func createCustomerWithRecordSync( userType: String, customer: Customer, appointmentsToSync: Results<Appointment>, serviceOrdersToSync: Results<ServiceOrder> ) -> CustomerWithRecordSync {
        if (userType == Constants.USUARIO_TIPO_CLIENTE_FINAL) {
            return CustomerWithRecordSync(
                customer: Customer.convertObjectToRequest(customer),
                appointmentsToInsert: Appointment.converListObjectToListRequest(
                    Array(Appointment.loadAllFromCustomerToSync(customerId: customer.id, createdLocal: true))
                ),
                appointmentsToUpdate:  Appointment.converListObjectToListRequest(
                    Array(Appointment.loadAllFromCustomerToSync(customerId: customer.id, createdLocal: false))
                )
            );
        } else {
            return CustomerWithRecordSync(
                customer: Customer.convertObjectToRequest(customer),
                appointmentsToInsert: Appointment.converListObjectToListRequest(
                    Array(Appointment.loadAllFromCustomerToSync(customerId: customer.id, createdLocal: true))
                ),
                appointmentsToUpdate: Appointment.converListObjectToListRequest(
                    Array(Appointment.loadAllFromCustomerToSync(customerId: customer.id, createdLocal: false))
                ),
                serviceOrdersToInsert: ServiceOrder.converListObjectToListRequest(
                    Array(ServiceOrder.loadAllFromCustomerToSync(customerId: customer.id, createdLocal: true))
                ),
                serviceOrdersToUpdate: ServiceOrder.converListObjectToListRequest(
                    Array(ServiceOrder.loadAllFromCustomerToSync(customerId: customer.id, createdLocal: false))
                )
            );
        }
    }
    
}
