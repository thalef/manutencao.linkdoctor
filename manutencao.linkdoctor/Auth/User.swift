//
//  User.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 16/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
import Foundation
import RealmSwift

@objcMembers class User: Object {
    
    @objc dynamic var id: String = "";
    //@objc dynamic var listAppCompanyId: List<String> = List<String>();
    let listAppCompanyId = List<String>();
    @objc dynamic var codigo: Int = 0;
    @objc dynamic var descricao: String = "";
    @objc dynamic var email: String = "";
    @objc dynamic var usuario: String = "";
    @objc dynamic var senha: String = "";
    @objc dynamic var appCompanyId: String = "";
    @objc dynamic var appCompanyLocalId: String = "";
    @objc dynamic var appUserGroupId: String = "";
    @objc dynamic var token: String = "";
    @objc dynamic var tipoUsuario: String = "";
    @objc dynamic var tipoUsuarioWeb: String = "";
    @objc dynamic var ativo: Bool = true;
    @objc dynamic var deletado: Bool = false;
    @objc dynamic var podeLogarApp: Bool = false;
    @objc dynamic var podeLogarWeb: Bool = false;
    @objc dynamic var medico: Bool = false;
    @objc dynamic var enfermagem: Bool = false;
    @objc dynamic var tecnico: Bool = false;
    @objc dynamic var servidorAtualizadoEmTimeStamp: Double = 0;
    @objc dynamic var versao: Int = 0;
    @objc dynamic var posicao: Int = 0;
    
    
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(
        id: String, codigo: Int, descricao: String, listAppCompanyId: [String],
        email: String, usuario: String, senha: String, appCompanyLocalId: String,
        appUserGroupId: String, token: String, appCompanyId: String, tipoUsuario: String,
        tipoUsuarioWeb: String, ativo: Bool, deletado: Bool, podeLogarApp: Bool,
        podeLogarWeb: Bool, medico: Bool, enfermagem: Bool, tecnico: Bool,
        servidorAtualizadoEmTimeStamp: Double, versao: Int, posicao: Int)
    {
        self.init();
        self.id = id;
        self.listAppCompanyId.removeAll();
        self.listAppCompanyId.append(objectsIn: Array(listAppCompanyId));
        self.codigo = codigo;
        self.descricao = descricao;
        self.email = email;
        self.usuario = usuario;
        self.senha = senha;
        self.appCompanyId = appCompanyId;
        self.appCompanyLocalId = appCompanyLocalId;
        self.appUserGroupId = appUserGroupId;
        self.token = token;
        self.tipoUsuario = tipoUsuario;
        self.tipoUsuarioWeb = tipoUsuarioWeb;
        self.ativo = ativo;
        self.deletado = deletado;
        self.podeLogarApp = podeLogarApp;
        self.podeLogarWeb = podeLogarWeb;
        self.medico = medico;
        self.enfermagem = enfermagem;
        self.tecnico = tecnico;
        self.servidorAtualizadoEmTimeStamp = servidorAtualizadoEmTimeStamp;
        self.versao = versao;
        self.posicao = posicao;
        
    }
}

// MARK: - CRUD methods

extension User {
    
    static func convertResponseToObject(_ userLogged: LoginResponse) -> User {
         return User(id: userLogged.id, codigo: userLogged.codigo, descricao: userLogged.descricao, listAppCompanyId: userLogged.listAppCompanyId, email: userLogged.email, usuario: userLogged.usuario, senha: "", appCompanyLocalId: userLogged.appCompanyLocalId, appUserGroupId: userLogged.appUserGroupId, token: userLogged.token ?? "", appCompanyId: userLogged.appCompanyId, tipoUsuario: userLogged.tipoUsuario, tipoUsuarioWeb: userLogged.tipoUsuarioWeb, ativo: userLogged.ativo, deletado: userLogged.deletado, podeLogarApp: userLogged.podeLogarApp, podeLogarWeb: userLogged.podeLogarWeb, medico: userLogged.medico, enfermagem: userLogged.enfermagem, tecnico: userLogged.tecnico, servidorAtualizadoEmTimeStamp: userLogged.servidorAtualizadoEmTimeStamp, versao: userLogged.versao, posicao: userLogged.posicao);
    }
    
    static func converListResponseToListObject(_ listUsers: [LoginResponse]) -> [User] {
        var list: [User] = [];
        for user in listUsers {
            list.append(convertResponseToObject(user));
        }
        return list;
    }
    
    
    static func loadFromId(in realm: Realm = try! Realm(), id: String) -> User? {
        return realm.objects(User.self).filter("\(Constants.CAMPO_ID) = '\(id)'").first;
    }
    
    static func loadAllActiveFromCustomerRealm(in realm: Realm = try! Realm(), customerId: String) -> Results<User> {
        return realm.objects(User.self)
            .filter("\(Constants.CAMPO_APP_COMPANY_ID) = '\(customerId)' AND \(Constants.CAMPO_ATIVO) = true AND \(Constants.CAMPO_DELETADO) = false")
            .sorted(byKeyPath: Constants.CAMPO_DESCRICAO, ascending: true);
    }
    static func loadAllFromCustomerRealm(in realm: Realm = try! Realm(), customerId: String) -> Results<User> {
        return realm.objects(User.self)
            .filter("\(Constants.CAMPO_APP_COMPANY_ID) = '\(customerId)'AND \(Constants.CAMPO_DELETADO) = false")
            .sorted(byKeyPath: Constants.CAMPO_DESCRICAO, ascending: true);
    }
    
    static func loadAllFromRealm(in realm: Realm = try! Realm()) -> Results<User> {
        return realm.objects(User.self);
    }
    
    @discardableResult
    static func add(objectToSave: User, in realm: Realm = try! Realm())
        -> User {
            let item = objectToSave;
            try! realm.write {
                realm.add(item)
            }
            return item
    }
    
    @discardableResult
    static func addOrUpdate(objectToSave: User, in realm: Realm = try! Realm())
        -> User {
            let item = objectToSave;
            try! realm.write {
                realm.add(item, update: .all);
            }
            return item
    }
    
    @discardableResult
    static func addAll(listToSave: [User], in realm: Realm = try! Realm())
        -> [User] {
            realm.beginWrite();
            for user in listToSave
            {
                realm.add(user)
            }
            try! realm.commitWrite()
            return listToSave
    }
    @discardableResult
    static func addOrUpdateAll(listToSave: [User], in realm: Realm = try! Realm())
        -> [User] {
            realm.beginWrite();
                realm.add(listToSave, update: .all)
            try! realm.commitWrite()
            return listToSave
    }
    
}
