//  Session
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 16/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation


class Session {
    
static let KEY_IS_LOGGED_IN = "userLogged";
static let KEY_TOKEN_API = "token";
static let KEY_SERVICE_KEY_APY = "serviceKey";
static let KEY_USUARIO_LOGADO_ID = "usuarioLogadoId";
static let KEY_USUARIO_LOGADO_DESCRICAO = "usuarioLogadoDescricao";
static let KEY_USUARIO_LOGADO_EMAIL = "usuarioLogadoEmail";
static let KEY_USUARIO_LOGADO_TIPO = "usuarioLogadoTipo";
static let KEY_USUARIO_LOGADO_COD_CLIENTE = "usuarioLogadoCodCliente";
static let KEY_SINCRONIZACAO_DATA_ULTIMA = "dataUltimaSincronizacao";
static let KEY_SINCRONIZACAO_TIMESTAMP = "timeStampUltimaSincronizacao";
static let KEY_SINCRONIZACAO_TIMESTAMP_SERVER = "timeStampUltimaSincronizacaoServer";
static let KEY_PUSH_NOTIFICATION_KEY = "KEY_PUSH_NOTIFICATION_KEY";
static let KEY_RESET_GENERIC_TABELA = "resetGenericTable";
static let KEY_FIRST_LOGIN = "isFirstLogin";

}


extension Session {

    static func addUser(userToSave: User) {
        UserDefaults.standard.set(userToSave.token, forKey: Session.KEY_TOKEN_API);
        UserDefaults.standard.set(Constants.apiKeyUrlRestHost, forKey: Session.KEY_SERVICE_KEY_APY);
        UserDefaults.standard.set(userToSave.id, forKey: Session.KEY_USUARIO_LOGADO_ID);
        UserDefaults.standard.set(userToSave.descricao, forKey: Session.KEY_USUARIO_LOGADO_DESCRICAO);
        UserDefaults.standard.set(userToSave.email, forKey: Session.KEY_USUARIO_LOGADO_EMAIL);
        UserDefaults.standard.set(userToSave.tipoUsuario, forKey: Session.KEY_USUARIO_LOGADO_TIPO);
        UserDefaults.standard.set(userToSave.appCompanyId, forKey: Session.KEY_USUARIO_LOGADO_COD_CLIENTE);
        UserDefaults.standard.set((Date().toEpochInt64()), forKey: Session.KEY_SINCRONIZACAO_TIMESTAMP);
        UserDefaults.standard.set((Date().getDeviceDateDisplayBrWithHour()), forKey: Session.KEY_SINCRONIZACAO_DATA_ULTIMA);
    }
    
    
    static func isFirstLogin() -> Bool {
        return UserDefaults.standard.bool(forKey: Session.KEY_FIRST_LOGIN);
    }
    
    static func isLoggedIn() -> Bool {
        return UserDefaults.standard.bool(forKey: Session.KEY_IS_LOGGED_IN);
    }
    
    static func getToken() -> String {
        //return UserDefaults.standard.
        return UserDefaults.standard.string(forKey: Session.KEY_TOKEN_API) ?? "";
    }
    
    static func getServiceKey() -> String {
        return UserDefaults.standard.string(forKey: Session.KEY_SERVICE_KEY_APY) ?? "";
    }
    
    static func getUsuarioLogadoId() -> String {
        return UserDefaults.standard.string(forKey: Session.KEY_USUARIO_LOGADO_ID) ?? "";
    }
    
    static func getUsuarioLogadoCodCliente() -> String {
        return UserDefaults.standard.string(forKey: Session.KEY_USUARIO_LOGADO_COD_CLIENTE) ?? "";
    }
    
    static func getUsuarioLogadoDescricao() -> String {
        return UserDefaults.standard.string(forKey: Session.KEY_USUARIO_LOGADO_DESCRICAO) ?? "";
    }
    
    static func getUsuarioLogadoTipo() -> String {
        return UserDefaults.standard.string(forKey: Session.KEY_USUARIO_LOGADO_TIPO) ?? "";
    }
    static func getUsuarioLogadoEmail() -> String {
        return UserDefaults.standard.string(forKey: Session.KEY_USUARIO_LOGADO_EMAIL) ?? "";
    }
    static func getSincronizacaoDataUltima() -> String {
        return UserDefaults.standard.string(forKey: Session.KEY_SINCRONIZACAO_DATA_ULTIMA) ?? "";
    }
    
    static func getPushNotificationKey() -> String {
        return UserDefaults.standard.string(forKey: Session.KEY_PUSH_NOTIFICATION_KEY) ?? "";
    }
    
    static func getKeySincronizacaoTimestampUltima() -> Double {
        return UserDefaults.standard.double(forKey: Session.KEY_SINCRONIZACAO_TIMESTAMP);
    }
    
    static func getKeySincronizacaoTimestampUltimaString() -> String {
        return String(UserDefaults.standard.double(forKey: Session.KEY_SINCRONIZACAO_TIMESTAMP));
    }
    static func getSincronizacaoTsUltimaServer() -> Double {
        return UserDefaults.standard.double(forKey: Session.KEY_SINCRONIZACAO_TIMESTAMP_SERVER);
    }
    
    
}

