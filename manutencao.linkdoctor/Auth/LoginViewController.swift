//
//  LoginViewController.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 08/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import UIKit
import CommonCrypto

class LoginViewController: UIViewController {
    
    var toolbar:UIToolbar!;
    
    @IBOutlet weak var emailTextField: LoginTextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: LoginButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupKeyBoardToolBar();
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        emailTextField.layer.borderColor = UIColor.white.cgColor;
        emailTextField.text = "";
        passwordTextField.text = "";
        //TODO
//        emailTextField.text = "antonionunes@linkdoctor.com.br";
//        passwordTextField.text = "antonionunes@";
//        emailTextField.text = "thalef1@gmail.com";
//        passwordTextField.text = "thalef";
        

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if(UserDefaults.standard.bool(forKey: "userLogged")){
            self.performSegue(withIdentifier: "loginSuccessSegue", sender: self)
        }
    }
    
    func setupKeyBoardToolBar(){
        toolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30));
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(
            title: "OK", style: .done, target: self,
            action: #selector(doneButtonAction)
        );
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        self.emailTextField.inputAccessoryView = toolbar;
        self.passwordTextField.inputAccessoryView = toolbar;
    }
    
    
    @IBAction func loginTapped(_ sender: UIButton) {
        setLoggingIn(true)
        if(emailTextField.text?.count == 0 || passwordTextField.text?.count == 0 ){
            showLoginFailure(message: "Informe o email e senha");
            setLoggingIn(false);
        }else{
            LoginService.login(
                username: emailTextField.text ?? "",
                password: passwordTextField.text != nil ? sha1(passwordTextField.text!) : "",
                               completion: handleLoginResponse(success:error:))
        }
    }
    
    func handleLoginResponse(success: LoginResponse?, error: Error?) {
        setLoggingIn(false);
        if error != nil {
            showLoginFailure(message: error?.localizedDescription ?? "")
        } else {
            guard let userLogged = success else {
                showLoginFailure(message: HttpStatusError.notFound.localizedDescription);
                return
            }
            let userToSave = User.convertResponseToObject(userLogged);
            UserDefaults.standard.set(true, forKey: Session.KEY_IS_LOGGED_IN);
            UserDefaults.standard.set(true, forKey: Session.KEY_FIRST_LOGIN);
            User.add(objectToSave: userToSave);
            Session.addUser(userToSave: userToSave);
            self.performSegue(withIdentifier: "loginSuccessSegue", sender: self)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "loginSuccessSegue") {
            //let tabCustomerViewController = self.storyboard!.instantiateViewController(withIdentifier: "NavCustomerTableView") as! UINavigationController;
            //let tabCustomerViewController = segue.destination as! TabCustomerViewController
            //tabCustomerViewController.customerId = self.selectedCustomerId;
        }
    }
    
    func setLoggingIn(_ loggingIn: Bool) {
        if loggingIn {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
        emailTextField.isEnabled = !loggingIn
        passwordTextField.isEnabled = !loggingIn
        loginButton.isEnabled = !loggingIn
    }
    
    func showLoginFailure(message: String) {
        let alertVC = UIAlertController(title: "Erro ao autenticar", message: message, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        show(alertVC, sender: nil)
    }
    
    func sha1(_ value: String) -> String {
        let data = Data(value.utf8)
        var digest = [UInt8](repeating: 0, count:Int(CC_SHA1_DIGEST_LENGTH))
        data.withUnsafeBytes {
            _ = CC_SHA1($0.baseAddress, CC_LONG(data.count), &digest)
        }
        //let hexBytes = digest.map { String(format: "%02hhx", $0) }
        //return hexBytes.joined()
        return Data(digest).base64EncodedString()
    }
    
    @objc func doneButtonAction() {
        self.view.endEditing(true)
    }
    
    
}

