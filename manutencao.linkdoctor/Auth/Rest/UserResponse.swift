//
//  UserResponse.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 16/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation


struct UserResponse: Codable {
    
    let id: String ;
    let listAppCompanyId = List<String>();
    let codigo: Int = 0;
    let descricao: String = "";
    let email: String = "";
    let usuario: String = "";
    let senha: String = "";
    let appCompanyId: String = "";
    let appCompanyLocalId: String = "";
    let appUserGroupId: String = "";
    let token: String = "";
    let tipoUsuario: String = "";
    let tipoUsuarioWeb: String = "";
    let ativo: Bool = true;
    let deletado: Bool = false;
    let podeLogarApp: Bool = false;
    let podeLogarWeb: Bool = false;
    let medico: Bool = false;
    let enfermagem: Bool = false;
    let tecnico: Bool = false;
    let servidorAtualizadoEmTimeStamp: Double = 0;
    let versao: Int = 0;
    let posicao: Int = 0;
    
    enum CodingKeys: String, CodingKey {
        ///case deletadoa
        case listAppCompanyId
        case id
        case codigo
        case descricao
        case email
        case usuario
        case senha
        case appCompanyId
        case appCompanyLocalId
        case appUserGroupId
        case token
        case tipoUsuario
        case tipoUsuarioWeb
        case ativo
        case deletado
        case podeLogarApp
        case podeLogarWeb
        case medico
        case enfermagem
        case tecnico
        case servidorAtualizadoEmTimeStamp
        case versao
        case posicao
    }
    
}
