//
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 06/06/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation

struct UserUpdateRequest: Codable {
    
    var id: String;
    var descricao: String;
    var ativo: Bool
    
    init(){
        self.id = "";
        self.descricao = "";
        self.ativo = true;
    }
    
    
    enum CodingKeys: String, CodingKey {
        case id;
        case descricao;
        case ativo;
    }
    
}
