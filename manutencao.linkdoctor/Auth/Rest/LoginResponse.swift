//
//  LoginResponse.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 08/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation


struct LoginResponse: Codable {

    let id: String;
    let codigo: Int;
    let descricao: String;
    let email: String;
    let usuario: String;
    let listAppCompanyId: [String];
    let appCompanyId: String;
    let appCompanyLocalId: String;
    let appUserGroupId: String;
    let token: String?;
    let tipoUsuario: String;
    let tipoUsuarioWeb: String;
    let ativo: Bool;
    let deletado: Bool;
    let podeLogarApp: Bool;
    let podeLogarWeb: Bool;
    let medico: Bool;
    let enfermagem: Bool;
    let tecnico: Bool;
    let servidorAtualizadoEmTimeStamp: Double;
    let versao: Int;
    let posicao: Int;
    
    enum CodingKeys: String, CodingKey {
        case id
        case codigo
        case descricao
        case email
        case usuario
        case listAppCompanyId
        case appCompanyId
        case appCompanyLocalId
        case appUserGroupId
        case token
        case tipoUsuario
        case tipoUsuarioWeb
        case ativo
        case deletado
        case podeLogarApp
        case podeLogarWeb
        case medico
        case enfermagem
        case tecnico
        case servidorAtualizadoEmTimeStamp
        case versao
        case posicao
    }
    
}
