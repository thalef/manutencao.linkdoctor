//
//  LoginService.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 08/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RealmSwift

enum HttpStatusError: Error {
    case success
    case authenticationError
    case userOrPasswordError
    case badRequest
    case badUrlRequest
    case outdated
    case failed
    case noData
    case notFound
    case unableToDecode
    case serverError
    case methodNotAllowed
    case objectAlreadyExist
}

extension HttpStatusError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .success: return "sucesso"
        case .authenticationError: return "You need to be authenticated first."
        case .userOrPasswordError: return "Usuário ou senha inválido."
        case .badRequest: return "Bad request"
        case .badUrlRequest: return "URL: Caminho não encontrado."
        case .outdated: return "The url you requested is outdated."
        case .failed: return "Falha ao tentar realizar a requisição."
        case .noData: return "Response returned with no data to decode."
        case .notFound: return "Objeto não encontrado."
        case .unableToDecode: return "Erro ao transformar o resultado"
        case .serverError: return "Erro no servidor."
        case .methodNotAllowed: return "Metodo não permitido"
        case .objectAlreadyExist: return "Já existe um registro para este código."
        }
    }
}

extension Data {
    func toString() -> String? {
        return String(data: self, encoding: .utf8)
    }
}

class LoginService{
    
    struct Auth {
        static var accountId = 0
        static var requestToken = ""
        static var sessionId = ""
    }
    
    
    enum Endpoints {
        static let base = Constants.URL_REST_BASE;
        static let baseUser = Constants.URL_USUARIOS;
        static let checkLogin = Constants.URL_CHECK_LOGIN
        static let apiKeyParam = "";
        
        case login
        case getUsers
        case getUsersSync(String)
        case putUser(String)
        case postUser
        case getCustomers
        case getCustomerById(String)
        case syncCustomers(String)
        case syncCustomerById(String, String)
        case putBoundQrCode(String)
        case putProductStock
        case deleteProductStock(String)
        
        case getGenericTable
        case getGenericTableSync(String)
        case getTecnico
        case getTecnicoSync(String)
        
        var stringValue: String {
            switch self {
            case .login: return Endpoints.checkLogin
                
            case .getUsers: return Endpoints.baseUser
            case .getUsersSync(let beginTimeStamp):
                return Constants.URL_USUARIOS_SYNC + "?beginTimeStamp=" + beginTimeStamp
            case .putUser(let idPath):
                return Constants.URL_USER + Constants.BARRA + idPath; //+ "?formatadoParaArray=S"
            case .postUser:
                return Constants.URL_USER; //+ "?formatadoParaArray=S"
                
            case .getCustomers: return Constants.PATH_CUSTOMER_WITH_RECORDS
            case .getCustomerById(let idPath):
                return Constants.PATH_CUSTOMER_WITH_RECORDS_BY_ID + idPath + "?formatadoParaArray=S"
                
            case .putBoundQrCode(let idPath):
                return Constants.PATH_PRODUCT_STOCK_BOUND_QR_CODE + idPath; //+ "?formatadoParaArray=S"
            case .putProductStock:
                return Constants.URL_PRODUCT_STOCK;
            case .deleteProductStock(let idPath):
                return Constants.URL_PRODUCT_STOCK_DELETE + idPath; //+ "?formatadoParaArray=S"
                
            case .syncCustomers(let beginTimeStamp): return Constants.PATH_CUSTOMER_SYNC_RX + "?beginTimeStamp=" + beginTimeStamp
            case .syncCustomerById(let idPath, let beginTimeStamp):
                return Constants.PATH_CUSTOMER_SYNC_RX_BY_ID + idPath + "?beginTimeStamp=" + beginTimeStamp
                
            case .getGenericTable: return Constants.URL_TABELAS_TIPO + "?formatadoParaArray=S"
            case .getGenericTableSync(let beginTimeStamp):
                return Constants.URL_TABELAS_TIPO_SYNC + "?formatadoParaArray=S&beginTimeStamp=" + beginTimeStamp
                
            case .getTecnico: return Constants.URL_TECNICOS
            case .getTecnicoSync(let beginTimeStamp):
                return Constants.URL_TECNICOS_SYNC + "?beginTimeStamp=" + beginTimeStamp
                
            }
        }
        
        var url: URL {
            return URL(string: stringValue)!
        }
    }
    
    
    func getUsersRxRequest(token: String, serviceKey: String) -> URLRequest{
        let url: URL =  Endpoints.getUsers.url;
        var request = URLRequest(url: url);
        request.httpMethod = "GET";
        request.addValue("application/json", forHTTPHeaderField: "Content-Type");
        request.addValue(token, forHTTPHeaderField: Constants.REST_PARAM_AUTHORIZATION);
        request.addValue(serviceKey, forHTTPHeaderField: Constants.REST_PARAM_SERVICE_KEY);
        return request;
    }
    
    func getUsersRxSyncRequest(token: String, serviceKey: String, beginTimeStamp: String) -> URLRequest{
        var request = URLRequest(url: Endpoints.getUsersSync(beginTimeStamp).url);
        request.httpMethod = "GET";
        request.addValue("application/json", forHTTPHeaderField: "Content-Type");
        request.addValue(token, forHTTPHeaderField: Constants.REST_PARAM_AUTHORIZATION);
        request.addValue(serviceKey, forHTTPHeaderField: Constants.REST_PARAM_SERVICE_KEY);
        return request;
    }
    
    
    func putUserRequest(userId: String, body: UserUpdateRequest) -> URLRequest{
        var requesta = URLRequest(url: Endpoints.putUser(userId).url);
        requesta.httpMethod = "PUT";
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let encoder = JSONEncoder();
        encoder.dateEncodingStrategy = .formatted(formatter);
        requesta.httpBody = try! encoder.encode(body);
        requesta.addValue("application/json", forHTTPHeaderField: "Content-Type");
        requesta.addValue(Session.getToken(), forHTTPHeaderField: Constants.REST_PARAM_AUTHORIZATION);
        requesta.addValue(Session.getServiceKey(), forHTTPHeaderField: Constants.REST_PARAM_SERVICE_KEY);
        requesta.addValue("close", forHTTPHeaderField: Constants.REST_PARAM_CONNECTION);
        return requesta;
    }
    func postUserRequest(body: UserCreateRequest) -> URLRequest{
        var requesta = URLRequest(url: Endpoints.postUser.url);
        requesta.httpMethod = "POST";
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let encoder = JSONEncoder();
        encoder.dateEncodingStrategy = .formatted(formatter);
        requesta.httpBody = try! encoder.encode(body);
        requesta.addValue("application/json", forHTTPHeaderField: "Content-Type");
        requesta.addValue(Session.getToken(), forHTTPHeaderField: Constants.REST_PARAM_AUTHORIZATION);
        requesta.addValue(Session.getServiceKey(), forHTTPHeaderField: Constants.REST_PARAM_SERVICE_KEY);
        requesta.addValue("close", forHTTPHeaderField: Constants.REST_PARAM_CONNECTION);
        return requesta;
    }
    
    
    func deleteProductStockRequest(id: String, body: ProductStockEditRequest) -> URLRequest{
        var requesta = URLRequest(url: Endpoints.deleteProductStock(id).url);
        requesta.httpMethod = "PUT";
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let encoder = JSONEncoder();
        encoder.dateEncodingStrategy = .formatted(formatter);
        requesta.httpBody = try! encoder.encode(body);
        requesta.addValue("application/json", forHTTPHeaderField: "Content-Type");
        requesta.addValue(Session.getToken(), forHTTPHeaderField: Constants.REST_PARAM_AUTHORIZATION);
        requesta.addValue(Session.getServiceKey(), forHTTPHeaderField: Constants.REST_PARAM_SERVICE_KEY);
        requesta.addValue("close", forHTTPHeaderField: Constants.REST_PARAM_CONNECTION);
        return requesta;
    }
    func putProductStockRequest(id: String, body: ProductStockEditRequest) -> URLRequest{
        var requesta = URLRequest(url: Endpoints.putProductStock.url);
        requesta.httpMethod = "PUT";
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let encoder = JSONEncoder();
        encoder.dateEncodingStrategy = .formatted(formatter);
        requesta.httpBody = try! encoder.encode(body);
        requesta.addValue("application/json", forHTTPHeaderField: "Content-Type");
        requesta.addValue(Session.getToken(), forHTTPHeaderField: Constants.REST_PARAM_AUTHORIZATION);
        requesta.addValue(Session.getServiceKey(), forHTTPHeaderField: Constants.REST_PARAM_SERVICE_KEY);
        requesta.addValue("close", forHTTPHeaderField: Constants.REST_PARAM_CONNECTION);
        return requesta;
    }
    //
    func getTecnicosRequest() -> URLRequest{
        var requesta = URLRequest(url: Endpoints.getTecnico.url);
        requesta.httpMethod = "GET";
        requesta.addValue("application/json", forHTTPHeaderField: "Content-Type");
        requesta.addValue(Session.getToken(), forHTTPHeaderField: Constants.REST_PARAM_AUTHORIZATION);
        requesta.addValue(Session.getServiceKey(), forHTTPHeaderField: Constants.REST_PARAM_SERVICE_KEY);
        return requesta;
    }
    
    func getTecnicosSyncArrayRequest(beginTimeStamp: String) -> URLRequest{
        var requesta = URLRequest(url: Endpoints.getTecnicoSync(beginTimeStamp).url);
        requesta.httpMethod = "GET";
        requesta.addValue("application/json", forHTTPHeaderField: "Content-Type");
        requesta.addValue("close", forHTTPHeaderField: Constants.REST_PARAM_CONNECTION);
        requesta.addValue(Session.getToken(), forHTTPHeaderField: Constants.REST_PARAM_AUTHORIZATION);
        requesta.addValue(Session.getServiceKey(), forHTTPHeaderField: Constants.REST_PARAM_SERVICE_KEY);
        return requesta;
    }
    
    func getGenericTablesArrayRequest() -> URLRequest{
        var requesta = URLRequest(url: Endpoints.getGenericTable.url);
        requesta.httpMethod = "GET";
        requesta.addValue("application/json", forHTTPHeaderField: "Content-Type");
        requesta.addValue(Session.getToken(), forHTTPHeaderField: Constants.REST_PARAM_AUTHORIZATION);
        requesta.addValue(Session.getServiceKey(), forHTTPHeaderField: Constants.REST_PARAM_SERVICE_KEY);
        return requesta;
    }
    
    func getGenericTableSyncArrayRequest(beginTimeStamp: String) -> URLRequest{
        var requesta = URLRequest(url: Endpoints.getGenericTableSync(beginTimeStamp).url);
        requesta.httpMethod = "GET";
        requesta.addValue("application/json", forHTTPHeaderField: "Content-Type");
        requesta.addValue("close", forHTTPHeaderField: Constants.REST_PARAM_CONNECTION);
        requesta.addValue(Session.getToken(), forHTTPHeaderField: Constants.REST_PARAM_AUTHORIZATION);
        requesta.addValue(Session.getServiceKey(), forHTTPHeaderField: Constants.REST_PARAM_SERVICE_KEY);
        return requesta;
    }
    
    
    func putBoundQrCodeRequest(qrCodeId: String, body: ProductBoundQrCodeRequest) -> URLRequest{
        var requesta = URLRequest(url: Endpoints.putBoundQrCode(qrCodeId).url);
        requesta.httpMethod = "PUT";
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let encoder = JSONEncoder();
        encoder.dateEncodingStrategy = .formatted(formatter);
        requesta.httpBody = try! encoder.encode(body);
        requesta.addValue("application/json", forHTTPHeaderField: "Content-Type");
        requesta.addValue(Session.getToken(), forHTTPHeaderField: Constants.REST_PARAM_AUTHORIZATION);
        requesta.addValue(Session.getServiceKey(), forHTTPHeaderField: Constants.REST_PARAM_SERVICE_KEY);
        requesta.addValue("close", forHTTPHeaderField: Constants.REST_PARAM_CONNECTION);
        return requesta;
    }
    
    
    func postCustomersWithRecordsRequest(body: CustomerWithRecordSyncRequest, beginTimeStamp: String) -> URLRequest{
        var requesta = URLRequest(url: Endpoints.syncCustomers(beginTimeStamp).url);
        requesta.httpMethod = "POST";
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let encoder = JSONEncoder();
        encoder.dateEncodingStrategy = .formatted(formatter);
        requesta.httpBody = try! encoder.encode(body);
        requesta.addValue("application/json", forHTTPHeaderField: "Content-Type");
        requesta.addValue(Session.getToken(), forHTTPHeaderField: Constants.REST_PARAM_AUTHORIZATION);
        requesta.addValue(Session.getServiceKey(), forHTTPHeaderField: Constants.REST_PARAM_SERVICE_KEY);
        requesta.addValue("close", forHTTPHeaderField: Constants.REST_PARAM_CONNECTION);
        return requesta;
    }
    
    func postCustomerUniqueWithRecordsRequest(id: String, body: CustomerWithRecordSyncRequest, beginTimeStamp: String) -> URLRequest{
        var requesta = URLRequest(url: Endpoints.syncCustomerById(id, beginTimeStamp).url);
        requesta.httpMethod = "POST";
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let encoder = JSONEncoder();
        encoder.dateEncodingStrategy = .formatted(formatter);
        requesta.httpBody = try! encoder.encode(body);
        requesta.addValue("application/json", forHTTPHeaderField: "Content-Type");
        requesta.addValue(Session.getToken(), forHTTPHeaderField: Constants.REST_PARAM_AUTHORIZATION);
        requesta.addValue(Session.getServiceKey(), forHTTPHeaderField: Constants.REST_PARAM_SERVICE_KEY);
        requesta.addValue("close", forHTTPHeaderField: Constants.REST_PARAM_CONNECTION);
        return requesta;
    }
    
    func getCustomersWithRecordsRequest() -> URLRequest{
        var requesta = URLRequest(url: Endpoints.getCustomers.url);
        requesta.httpMethod = "GET";
        requesta.addValue("application/json", forHTTPHeaderField: "Content-Type");
        requesta.addValue(Session.getToken(), forHTTPHeaderField: Constants.REST_PARAM_AUTHORIZATION);
        requesta.addValue(Session.getServiceKey(), forHTTPHeaderField: Constants.REST_PARAM_SERVICE_KEY);
        return requesta;
    }
    
    func getCustomerUniqueWithRecordsRequest(id: String) -> URLRequest{
        var requesta = URLRequest(url: Endpoints.getCustomerById(id).url);
        requesta.httpMethod = "GET";
        requesta.addValue("application/json", forHTTPHeaderField: "Content-Type");
        requesta.addValue(Session.getToken(), forHTTPHeaderField: Constants.REST_PARAM_AUTHORIZATION);
        requesta.addValue(Session.getServiceKey(), forHTTPHeaderField: Constants.REST_PARAM_SERVICE_KEY);
        return requesta;
    }
    
    func getCustomerAndSubDocsToSyncRequest(id: String) -> URLRequest{
        var requesta = URLRequest(url: Endpoints.getCustomers.url);
        requesta.httpMethod = "GET";
        requesta.addValue("application/json", forHTTPHeaderField: "Content-Type");
        requesta.addValue(Session.getToken(), forHTTPHeaderField: Constants.REST_PARAM_AUTHORIZATION);
        requesta.addValue(Session.getServiceKey(), forHTTPHeaderField: Constants.REST_PARAM_SERVICE_KEY);
        return requesta;
    }
    
    func send(requesta: URLRequest) -> Observable<[LoginResponse]> {
        var request = URLRequest(url: Endpoints.getUsers.url);
        request.httpMethod = "GET";
        request.addValue("application/json", forHTTPHeaderField: "Content-Type");
        request.addValue(Session.getToken(), forHTTPHeaderField: Constants.REST_PARAM_AUTHORIZATION);
        request.addValue(Session.getServiceKey(), forHTTPHeaderField: Constants.REST_PARAM_SERVICE_KEY);
        return Observable<[LoginResponse]>.create { observer in
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                do {
                    let responseObject: [LoginResponse] = try JSONDecoder().decode([LoginResponse].self, from: data ?? Data());
                    observer.onNext(responseObject)
                } catch let error {
                    observer.onError(error)
                }
                observer.onCompleted()
            }
            task.resume()
            
            return Disposables.create {
                task.cancel()
            }
        }
    }
    
    func parseUserResponse(data: Data) -> [LoginResponse]{
        var responseObject: [LoginResponse] = [LoginResponse]();
        let decoder = JSONDecoder();
        do {
            responseObject = try decoder.decode([LoginResponse].self, from: data);
            return responseObject;
        } catch {
            return responseObject;
        }
        
    }
    
    //
    func postUserRx(request : URLRequest) -> Single<LoginResponse> {
        return Single<LoginResponse>.create { single -> Disposable in
            let task = URLSession.shared.dataTask(with:request) { data, response, error in
                if let data = data  {
                    let httpResponse = response as? HTTPURLResponse;
                    if (200...299).contains(httpResponse!.statusCode){
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                        let decoder = JSONDecoder();
                        decoder.dateDecodingStrategy = .formatted(formatter);
                        do {
                            let responseObject: LoginResponse = try decoder.decode(LoginResponse.self, from: data)
                            single(.success(responseObject));
                        } catch let errorCatch {
                            single(.error(errorCatch));
                        }
                    }else{
                        var errorStatusToReturn = HttpStatusError.failed;
                        if(httpResponse?.statusCode == 422){
                            errorStatusToReturn = HttpStatusError.objectAlreadyExist;
                        }
                        if(httpResponse?.statusCode == 500){
                            errorStatusToReturn = HttpStatusError.serverError;
                        }
                        single(.error(errorStatusToReturn));
                    }
                }else{
                    single(.error( HttpStatusError.serverError));
                }
            }
            task.resume();
            return Disposables.create { task.cancel(); }
        }
    }
    
    func putUserRx(request : URLRequest) -> Single<LoginResponse> {
        return Single<LoginResponse>.create { single -> Disposable in
            let task = URLSession.shared.dataTask(with:request) { data, response, error in
                if let data = data  {
                    let httpResponse = response as? HTTPURLResponse;
                    if (200...299).contains(httpResponse!.statusCode){
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                        let decoder = JSONDecoder();
                        decoder.dateDecodingStrategy = .formatted(formatter);
                        do {
                            let responseObject: LoginResponse = try decoder.decode(LoginResponse.self, from: data)
                            single(.success(responseObject));
                        } catch let errorCatch {
                            single(.error(errorCatch));
                        }
                    }else{
                        var errorStatusToReturn = HttpStatusError.failed;
                        if(httpResponse?.statusCode == 422){
                            errorStatusToReturn = HttpStatusError.objectAlreadyExist;
                        }
                        if(httpResponse?.statusCode == 500){
                            errorStatusToReturn = HttpStatusError.serverError;
                        }
                        single(.error(errorStatusToReturn));
                    }
                }else{
                    single(.error( HttpStatusError.serverError));
                }
            }
            task.resume();
            return Disposables.create { task.cancel(); }
        }
    }
    
    
    
    func putProductStockRequestRx(request : URLRequest) -> Single<ProductStockResponse> {
        return Single<ProductStockResponse>.create { single -> Disposable in
            let task = URLSession.shared.dataTask(with:request) { data, response, error in
                if let data = data  {
                    let httpResponse = response as? HTTPURLResponse;
                    if (200...299).contains(httpResponse!.statusCode){
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                        let decoder = JSONDecoder();
                        decoder.dateDecodingStrategy = .formatted(formatter);
                        do {
                            let responseObject: ProductStockResponse = try decoder.decode(ProductStockResponse.self, from: data)
                            single(.success(responseObject));
                        } catch let errorCatch {
                            single(.error(errorCatch));
                        }
                    }else{
                        var errorStatusToReturn = HttpStatusError.failed;
                        if(httpResponse?.statusCode == 422){
                            errorStatusToReturn = HttpStatusError.objectAlreadyExist;
                        }
                        if(httpResponse?.statusCode == 500){
                            errorStatusToReturn = HttpStatusError.serverError;
                        }
                        single(.error(errorStatusToReturn));
                    }
                }else{
                    single(.error( HttpStatusError.serverError));
                }
            }
            task.resume();
            return Disposables.create { task.cancel(); }
        }
    }
    

    func putBoundQrCodeRx(request : URLRequest) -> Single<ProductStockResponse> {
        return Single<ProductStockResponse>.create { single -> Disposable in
            let task = URLSession.shared.dataTask(with:request) { data, response, error in
                if let data = data  {
                    let httpResponse = response as? HTTPURLResponse;
                    if (200...299).contains(httpResponse!.statusCode){
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                        let decoder = JSONDecoder();
                        decoder.dateDecodingStrategy = .formatted(formatter);
                        do {
                            let responseObject: ProductStockResponse = try decoder.decode(ProductStockResponse.self, from: data)
                            single(.success(responseObject));
                        } catch let errorCatch {
                            single(.error(errorCatch));
                        }
                    }else{
                        var errorStatusToReturn = HttpStatusError.failed;
                        if(httpResponse?.statusCode == 422){
                            errorStatusToReturn = HttpStatusError.objectAlreadyExist;
                        }
                        if(httpResponse?.statusCode == 500){
                            errorStatusToReturn = HttpStatusError.serverError;
                        }
                        single(.error(errorStatusToReturn));
                    }
                }else{
                    single(.error( HttpStatusError.serverError));
                }
            }
            task.resume();
            return Disposables.create { task.cancel(); }
        }
    }
    
    func getTecnicosRx(request : URLRequest) -> Single<SyncModelsHeper> {
        return Single<SyncModelsHeper>.create { single -> Disposable in
            let task = URLSession.shared.dataTask(with:request) { data, response, error in
                if let data = data  {
                    let httpResponse = response as? HTTPURLResponse;
                    if (200...299).contains(httpResponse!.statusCode){
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                        let decoder = JSONDecoder();
                        decoder.dateDecodingStrategy = .formatted(formatter);
                        do {
                            let responseObject: [TecnicoResponse] = try decoder.decode([TecnicoResponse].self, from: data)
                            let modelHelper: SyncModelsHeper = SyncModelsHeper();
                            modelHelper.listTecnico.append(contentsOf: responseObject);
                            modelHelper.typeSelected = String(describing: type(of: TecnicoResponse.self));
                            single(.success(modelHelper));
                        } catch let errorCatch {
                            single(.error(errorCatch));
                        }
                    }else{
                        var errorStatusToReturn = HttpStatusError.failed;
                        if(httpResponse?.statusCode == 422){
                            errorStatusToReturn = HttpStatusError.userOrPasswordError;
                        }
                        if(httpResponse?.statusCode == 500){
                            errorStatusToReturn = HttpStatusError.serverError;
                        }
                        single(.error(errorStatusToReturn));
                    }
                }else{
                    single(.error( HttpStatusError.serverError));
                }
            }
            task.resume();
            return Disposables.create { task.cancel(); }
        }
    }
    
    func getTecnicosSyncRx(request : URLRequest) -> Single<SyncModelsHeper> {
        return Single<SyncModelsHeper>.create { single -> Disposable in
            let task = URLSession.shared.dataTask(with:request) { data, response, error in
                if let data = data  {
                    let httpResponse = response as? HTTPURLResponse;
                    if (200...299).contains(httpResponse!.statusCode){
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                        let decoder = JSONDecoder();
                        decoder.dateDecodingStrategy = .formatted(formatter);
                        do {
                            let responseObject: [TecnicoResponse] = try decoder.decode([TecnicoResponse].self, from: data)
                            let modelHelper: SyncModelsHeper = SyncModelsHeper();
                            modelHelper.listTecnico.append(contentsOf: responseObject);
                            modelHelper.typeSelected = String(describing: type(of: TecnicoResponse.self));
                            single(.success(modelHelper));
                        } catch let errorCatch {
                            single(.error(errorCatch));
                        }
                    }else{
                        var errorStatusToReturn = HttpStatusError.failed;
                        if(httpResponse?.statusCode == 422){
                            errorStatusToReturn = HttpStatusError.userOrPasswordError;
                        }
                        if(httpResponse?.statusCode == 500){
                            errorStatusToReturn = HttpStatusError.serverError;
                        }
                        single(.error(errorStatusToReturn));
                    }
                }else{
                    single(.error( HttpStatusError.serverError));
                }
            }
            task.resume();
            return Disposables.create { task.cancel(); }
        }
    }
    
    func getGenericTablesArrayRx(request : URLRequest) -> Single<SyncModelsHeper> {
        return Single<SyncModelsHeper>.create { single -> Disposable in
            let task = URLSession.shared.dataTask(with:request) { data, response, error in
                if let data = data  {
                    let httpResponse = response as? HTTPURLResponse;
                    if (200...299).contains(httpResponse!.statusCode){
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                        let decoder = JSONDecoder();
                        decoder.dateDecodingStrategy = .formatted(formatter);
                        do {
                            let responseObject: [GenericTableResponse] = try decoder.decode([GenericTableResponse].self, from: data)
                            let modelHelper: SyncModelsHeper = SyncModelsHeper();
                            modelHelper.listGenericTable.append(contentsOf: responseObject);
                            modelHelper.typeSelected = String(describing: type(of: GenericTableResponse.self));
                            single(.success(modelHelper));
                        } catch let errorCatch {
                            single(.error(errorCatch));
                        }
                    }else{
                        var errorStatusToReturn = HttpStatusError.failed;
                        if(httpResponse?.statusCode == 422){
                            errorStatusToReturn = HttpStatusError.userOrPasswordError;
                        }
                        if(httpResponse?.statusCode == 500){
                            errorStatusToReturn = HttpStatusError.serverError;
                        }
                        single(.error(errorStatusToReturn));
                    }
                }else{
                    single(.error( HttpStatusError.serverError));
                }
            }
            task.resume();
            return Disposables.create { task.cancel(); }
        }
    }
    
    
    
    
    func getGenericTablesSyncArrayRx(request : URLRequest) -> Single<SyncModelsHeper> {
        return Single<SyncModelsHeper>.create { single -> Disposable in
            let task = URLSession.shared.dataTask(with:request) { data, response, error in
                if let data = data  {
                    let httpResponse = response as? HTTPURLResponse;
                    if (200...299).contains(httpResponse!.statusCode){
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                        let decoder = JSONDecoder();
                        decoder.dateDecodingStrategy = .formatted(formatter);
                        do {
                            let responseObject: [GenericTableResponse] = try decoder.decode([GenericTableResponse].self, from: data)
                            let modelHelper: SyncModelsHeper = SyncModelsHeper();
                            modelHelper.listGenericTable.append(contentsOf: responseObject);
                            modelHelper.typeSelected = String(describing: type(of: GenericTableResponse.self));
                            single(.success(modelHelper));
                        } catch let errorCatch {
                            single(.error(errorCatch));
                        }
                    }else{
                        var errorStatusToReturn = HttpStatusError.failed;
                        if(httpResponse?.statusCode == 422){
                            errorStatusToReturn = HttpStatusError.userOrPasswordError;
                        }
                        if(httpResponse?.statusCode == 500){
                            errorStatusToReturn = HttpStatusError.serverError;
                        }
                        single(.error(errorStatusToReturn));
                    }
                }else{
                    single(.error( HttpStatusError.serverError));
                }
            }
            task.resume();
            return Disposables.create { task.cancel(); }
        }
    }
    
    
    
    
    func getCustomersRx(request : URLRequest) -> Single<SyncModelsHeper> {
        
        return Single<SyncModelsHeper>.create { single -> Disposable in
            let task = URLSession.shared.dataTask(with:request) { data, response, error in
                if let data = data  {
                    let httpResponse = response as? HTTPURLResponse;
                    if (200...299).contains(httpResponse!.statusCode){
                        let decoder = JSONDecoder();
                        do {
                            decoder.keyDecodingStrategy = .convertFromSnakeCase;
                            let responseObject: [CustomerResponse] = try decoder.decode([CustomerResponse].self, from: data)
                            let modelHelper: SyncModelsHeper = SyncModelsHeper();
                            modelHelper.listCustomer.append(contentsOf: responseObject);
                            modelHelper.typeSelected = String(describing: type(of: CustomerResponse.self));
                            single(.success(modelHelper));
                        } catch let errorCatch {
                            single(.error(errorCatch));
                        }
                    }else{
                        var errorStatusToReturn = HttpStatusError.failed;
                        if(httpResponse?.statusCode == 422){
                            errorStatusToReturn = HttpStatusError.userOrPasswordError;
                        }
                        if(httpResponse?.statusCode == 500){
                            errorStatusToReturn = HttpStatusError.serverError;
                        }
                        single(.error(errorStatusToReturn));
                    }
                    
                }else{
                    single(.error( HttpStatusError.serverError));
                }
            }
            task.resume();
            return Disposables.create { task.cancel(); }
        }
    }
    
    
    func getCustomerAndSubDocsToSync(request : URLRequest) -> Single<SyncModelsHeper> {
        
        return Single<SyncModelsHeper>.create { single -> Disposable in
            let task = URLSession.shared.dataTask(with:request) { data, response, error in
                if let data = data  {
                    let httpResponse = response as? HTTPURLResponse;
                    if (200...299).contains(httpResponse!.statusCode){
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                        let decoder = JSONDecoder();
                        decoder.dateDecodingStrategy = .formatted(formatter);
                        do {
                            let responseObject: [CustomerWithRecordsResponse] = try decoder.decode([CustomerWithRecordsResponse].self, from: data)
                            let modelHelper: SyncModelsHeper = SyncModelsHeper();
                            modelHelper.listCustomerWithRecords.append(contentsOf: responseObject);
                            modelHelper.typeSelected = String(describing: type(of: CustomerWithRecordsResponse.self));
                            single(.success(modelHelper));
                        } catch let errorCatch {
                            single(.error(errorCatch));
                        }
                    }else{
                        var errorStatusToReturn = HttpStatusError.failed;
                        if(httpResponse?.statusCode == 422){
                            errorStatusToReturn = HttpStatusError.userOrPasswordError;
                        }
                        if(httpResponse?.statusCode == 500){
                            errorStatusToReturn = HttpStatusError.serverError;
                        }
                        single(.error(errorStatusToReturn));
                    }
                }else{
                    single(.error( HttpStatusError.serverError));
                }
            }
            task.resume();
            return Disposables.create { task.cancel(); }
        }
    }
    
    func getCustomerUniqueWithRecordsRx(request : URLRequest) -> Single<SyncModelsHeper> {
        return Single<SyncModelsHeper>.create { single -> Disposable in
            let task = URLSession.shared.dataTask(with:request) { data, response, error in
                if let data = data  {
                    let httpResponse = response as? HTTPURLResponse;
                    if (200...299).contains(httpResponse!.statusCode){
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                        let decoder = JSONDecoder();
                        decoder.dateDecodingStrategy = .formatted(formatter);
                        do {
                            let responseObject: [CustomerWithRecordsResponse] = try decoder.decode([CustomerWithRecordsResponse].self, from: data)
                            let modelHelper: SyncModelsHeper = SyncModelsHeper();
                            modelHelper.listCustomerWithRecords.append(contentsOf: responseObject);
                            modelHelper.typeSelected = String(describing: type(of: CustomerWithRecordsResponse.self));
                            single(.success(modelHelper));
                        } catch let errorCatch {
                            single(.error(errorCatch));
                        }
                    }else{
                        var errorStatusToReturn = HttpStatusError.failed;
                        if(httpResponse?.statusCode == 422){
                            errorStatusToReturn = HttpStatusError.userOrPasswordError;
                        }
                        if(httpResponse?.statusCode == 500){
                            errorStatusToReturn = HttpStatusError.serverError;
                        }
                        single(.error(errorStatusToReturn));
                    }
                }else{
                    single(.error( HttpStatusError.serverError));
                }
            }
            task.resume();
            return Disposables.create { task.cancel(); }
        }
    }
    
    
    
    func postCustomersWithRecordsRx(request : URLRequest) -> Single<SyncModelsHeper> {
        return Single<SyncModelsHeper>.create { single -> Disposable in
            let task = URLSession.shared.dataTask(with:request) { data, response, error in
                if let data = data  {
                    let httpResponse = response as? HTTPURLResponse;
                    if (200...299).contains(httpResponse!.statusCode){
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                        let decoder = JSONDecoder();
                        decoder.dateDecodingStrategy = .formatted(formatter);
                        do {
                            let responseObject: [CustomerWithRecordsResponse] = try decoder.decode([CustomerWithRecordsResponse].self, from: data)
                            let modelHelper: SyncModelsHeper = SyncModelsHeper();
                            modelHelper.listCustomerWithRecords.append(contentsOf: responseObject);
                            modelHelper.typeSelected = String(describing: type(of: CustomerWithRecordsResponse.self));
                            single(.success(modelHelper));
                        } catch let errorCatch {
                            single(.error(errorCatch));
                        }
                    }else{
                        var errorStatusToReturn = HttpStatusError.failed;
                        if(httpResponse?.statusCode == 422){
                            errorStatusToReturn = HttpStatusError.userOrPasswordError;
                        }
                        if(httpResponse?.statusCode == 500){
                            errorStatusToReturn = HttpStatusError.serverError;
                        }
                        single(.error(errorStatusToReturn));
                    }
                }else{
                    single(.error( HttpStatusError.serverError));
                }
            }
            task.resume();
            return Disposables.create { task.cancel(); }
        }
    }
    
    func getCustomersWithRecordsRx(request : URLRequest) -> Single<SyncModelsHeper> {
        return Single<SyncModelsHeper>.create { single -> Disposable in
            let task = URLSession.shared.dataTask(with:request) { data, response, error in
                if let data = data  {
                    let httpResponse = response as? HTTPURLResponse;
                    if (200...299).contains(httpResponse!.statusCode){
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                        let decoder = JSONDecoder();
                        decoder.dateDecodingStrategy = .formatted(formatter);
                        do {
                            let responseObject: [CustomerWithRecordsResponse] = try decoder.decode([CustomerWithRecordsResponse].self, from: data)
                            let modelHelper: SyncModelsHeper = SyncModelsHeper();
                            modelHelper.listCustomerWithRecords.append(contentsOf: responseObject);
                            modelHelper.typeSelected = String(describing: type(of: CustomerWithRecordsResponse.self));
                            single(.success(modelHelper));
                        } catch let errorCatch {
                            single(.error(errorCatch));
                        }
                    }else{
                        var errorStatusToReturn = HttpStatusError.failed;
                        if(httpResponse?.statusCode == 422){
                            errorStatusToReturn = HttpStatusError.userOrPasswordError;
                        }
                        if(httpResponse?.statusCode == 500){
                            errorStatusToReturn = HttpStatusError.serverError;
                        }
                        single(.error(errorStatusToReturn));
                    }
                }else{
                    single(.error( HttpStatusError.serverError));
                }
            }
            task.resume();
            return Disposables.create { task.cancel(); }
        }
    }
    
    func getUsersRx(request : URLRequest) -> Single<SyncModelsHeper> {
        return Single<SyncModelsHeper>.create { single -> Disposable in
            let task = URLSession.shared.dataTask(with:request) { data, response, error in
                if let data = data  {
                    let httpResponse = response as? HTTPURLResponse;
                    if (200...299).contains(httpResponse!.statusCode){
                        let decoder = JSONDecoder();
                        do {
                            let responseObject: [LoginResponse] = try decoder.decode([LoginResponse].self, from: data);
                            let modelHelper: SyncModelsHeper = SyncModelsHeper();
                            modelHelper.listUsers.append(contentsOf: responseObject);
                            modelHelper.typeSelected = String(describing: type(of: LoginResponse.self));
                            single(.success(modelHelper));
                        } catch {
                            let errorResponse = try! decoder.decode([LoginResponse].self, from: data) as! Error;
                            single(.error(errorResponse));
                        }
                    }else{
                        var errorStatusToReturn = HttpStatusError.failed;
                        if(httpResponse?.statusCode == 422){
                            errorStatusToReturn = HttpStatusError.userOrPasswordError;
                        }
                        if(httpResponse?.statusCode == 500){
                            errorStatusToReturn = HttpStatusError.serverError;
                        }
                        single(.error(errorStatusToReturn));
                    }
                    
                }else{
                    single(.error( HttpStatusError.serverError));
                }
            }
            task.resume();
            return Disposables.create {
                task.cancel();
            }
        }
    }
    
    class func taskForGETRequest<ResponseType: Decodable>(url: URL, responseType: ResponseType.Type, token: String, serviceKey: String, completion: @escaping (ResponseType?, Error?) -> Void) -> URLSessionDataTask {
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue(token, forHTTPHeaderField: Constants.REST_PARAM_AUTHORIZATION)
        request.addValue(serviceKey, forHTTPHeaderField: Constants.REST_PARAM_SERVICE_KEY)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            //let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else {
                DispatchQueue.main.async {
                    completion(nil, error)
                }
                return
            }
            let httpResponse = response as? HTTPURLResponse;
            if (200...299).contains(httpResponse!.statusCode){
                let decoder = JSONDecoder()
                do {
                    let responseObject = try decoder.decode(ResponseType.self, from: data)
                    DispatchQueue.main.async {
                        completion(responseObject, nil)
                    }
                } catch {
                    do {
                        let errorResponse = try decoder.decode([LoginResponse].self, from: data) as! Error
                        DispatchQueue.main.async {
                            completion(nil, errorResponse)
                        }
                    } catch {
                        DispatchQueue.main.async {
                            completion(nil, HttpStatusError.unableToDecode)
                        }
                    }
                }
            }else{
                var errorStatusToReturn = HttpStatusError.failed;
                if(httpResponse?.statusCode == 422){
                    errorStatusToReturn = HttpStatusError.userOrPasswordError;
                }
                if(httpResponse?.statusCode == 500){
                    errorStatusToReturn = HttpStatusError.serverError;
                }
                DispatchQueue.main.async {
                    completion(nil, errorStatusToReturn)
                }
            }
        }
        task.resume()
        return task
    }
    
    
    class func taskForPOSTRequest<RequestType: Encodable, ResponseType: Decodable>(url: URL, responseType: ResponseType.Type, body: RequestType, completion: @escaping (ResponseType?, Error?) -> Void) {
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = try! JSONEncoder().encode(body)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data else {
                DispatchQueue.main.async {
                    completion(nil, error)
                }
                return
            }
            
            let httpResponse = response as? HTTPURLResponse;
            if (200...299).contains(httpResponse!.statusCode){
                let decoder = JSONDecoder()
                do {
                    let responseObject = try decoder.decode(ResponseType.self, from: data)
                    DispatchQueue.main.async {
                        completion(responseObject, nil)
                    }
                } catch {
                    do {
                        let errorResponse = try decoder.decode(LoginResponse.self, from: data) as! Error
                        DispatchQueue.main.async {
                            completion(nil, errorResponse)
                        }
                    } catch {
                        DispatchQueue.main.async {
                            completion(nil, HttpStatusError.unableToDecode)
                        }
                    }
                }
            }else{
                
                var errorStatusToReturn = HttpStatusError.failed;
                if(httpResponse?.statusCode == 422){
                    errorStatusToReturn = HttpStatusError.userOrPasswordError;
                }
                if(httpResponse?.statusCode == 500){
                    errorStatusToReturn = HttpStatusError.serverError;
                }
                DispatchQueue.main.async {
                    completion(nil, errorStatusToReturn)
                }
            }
        }
        task.resume()
    }
    
    class func login(username: String, password: String, completion: @escaping (LoginResponse?, Error?) -> Void) {
        let body = LoginRequest(
            username: username, password: password, deviceDescription: "ios",
            iosPushToken: Session.getPushNotificationKey()
        )
        taskForPOSTRequest(url: Endpoints.login.url, responseType: LoginResponse.self, body: body) { response, error in
            if let response = response {
                completion(response, nil)
            } else {
                completion(nil, error)
            }
        }
    }
    
    
}
