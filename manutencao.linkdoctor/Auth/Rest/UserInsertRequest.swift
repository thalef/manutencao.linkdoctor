//
//  ProductBoundQrCodeRequest.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 06/06/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation

struct UserCreateRequest: Codable {
    
    var id: String;
    var descricao: String;
    var senhaInserir: String;
    var email: String;
    var usuario: String;
    var appCompanyId: String;
    var descricaoCliente: String;
    var ativo: Bool
    
    
    init(){
        self.id = "";
        self.descricao = "";
        self.senhaInserir = "";
        self.email = "";
        self.usuario = "";
        self.appCompanyId = "";
        self.descricaoCliente = "";
        self.ativo = true;
    }
    
    
    enum CodingKeys: String, CodingKey {
        case id;
        case descricao;
        case senhaInserir;
        case email;
        case usuario;
        case appCompanyId;
        case descricaoCliente;
        case ativo;
    }
    
}
