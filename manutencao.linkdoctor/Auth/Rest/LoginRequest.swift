//
//  LoginRequest.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 08/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation

struct LoginRequest: Codable {
    
    let username: String;
    let password: String;
    let deviceDescription: String;
    let iosPushToken: String;
    
    enum CodingKeys: String, CodingKey {
        case username
        case password
        case deviceDescription
        case iosPushToken
    }
}
