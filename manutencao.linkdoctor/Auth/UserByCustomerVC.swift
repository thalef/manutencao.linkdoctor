//
//  AppointmentViewController.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 09/05/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import UIKit
import RealmSwift
import Network
import RxSwift
import RxCocoa

class UserByCustomerVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    let monitor = NWPathMonitor();
    var queue: DispatchQueue!;
    var isInternetOn: Bool = false;
    var customerMode: Bool = true;
    let syncProcess = SyncProcessHelper();
    let disposeBag = DisposeBag();
    private var userModel: User?;
    private var customerSelected: Customer?
    private let loginService = LoginService();
    
    var situacaoSelecionada = "";
    var descricaoSituacaoSelecionada = "Selecione o tipo";
    let situacaoPicker = UIPickerView();
    let situacaoPickerData:[(id: String, description: String)] = [
        ("", "Selecione o tipo"),
        ("M", "Diretoria"),
        ("E", "Administração"),
        ("T", "Técnico")
    ];
    var equipamentoSelecionado = "";
    var tecnicoSelecionado = "";
    var customerId: String = "";
    var selectedUserId: String = "";
    var editMode: Bool = false;
    var toolbar:UIToolbar!;
//    @IBOutlet weak var tecnicoLabel: UILabel!
//    @IBOutlet weak var situacaoTextField: UITextField!
//    @IBOutlet weak var clienteTextField: UITextField!
//    @IBOutlet weak var tecnicoTextField: UITextField!
//    @IBOutlet weak var equipamentoTextView: UITextView!
//    @IBOutlet weak var contentView: UIView!
//    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var clienteTextField: UITextField!
    @IBOutlet weak var nomeTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var senhaLabel: UILabel!
    
    @IBOutlet weak var senhaTextField: UITextField!
    @IBOutlet weak var isUserActive: UISwitch!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
//    @IBOutlet weak var saveButton: UIBarButtonItem!
//    @IBOutlet weak var solucaoTextView: UITextView!
//    @IBOutlet weak var solicitanteTextField: UITextField!
//    @IBOutlet weak var defeitoTextView: UITextView!
//    @IBOutlet weak var solucaoLabel: UILabel!
//    @IBOutlet weak var situacaoLabel: UILabel!
    
//    @IBOutlet weak var defeitoCardLabel: UILabel!
//    @IBOutlet weak var appointmentConstraintContentHeight: NSLayoutConstraint!
//    @IBOutlet weak var appointmentConstraintDadosCV: NSLayoutConstraint!
//    @IBOutlet weak var appointmentConstraintDefeitoCV: NSLayoutConstraint!
    @IBOutlet weak var labelUserType: UILabel!
    @IBOutlet weak var fieldUserType: UITextField!
    
    var activeField: UITextField?
    var activeTextView: UITextView?
    var lastOffset: CGPoint!
    var keyboardHeight: CGFloat!
    
    let genericAlert = UIAlertController(title: nil, message: "", preferredStyle: .alert);
    let saveAlert = UIAlertController(title: nil, message: "Salvando...", preferredStyle: .alert);
    let saveLoading = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge);
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = editMode ? "Editar usuário" : "Novo usuário";
        // Add touch gesture for contentView
        self.contentView.addGestureRecognizer(
            UITapGestureRecognizer(target: self, action: #selector(returnTextView(gesture:)))
        )
        //self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 1400);
        setupNotifications();
        setupCustomerMode();
        loadData();
        setupFields();
        setValueFields();
        setupKeyBoardToolBar();
        
    }
    
    // MARK: setups
    
    func setupNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(aNotification:)), name: UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil);
        
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                self.isInternetOn = true;
            } else {
                self.isInternetOn = false;
            }
        }
        let queue = DispatchQueue(label: "Monitor");
        monitor.start(queue: queue);
    }
    
    func setValueFields(){
        
        clienteTextField.text = customerSelected?.nome;
        clienteTextField.isEnabled = false;
        isUserActive.setOn(userModel?.ativo ?? false, animated: false);
        
        if(editMode){
            nomeTextField.text = userModel?.descricao ?? "";
            emailTextField.text = userModel?.email ?? "";
            emailTextField.isEnabled = false;
            senhaLabel.isHidden = true;
            senhaTextField.isHidden = true;
            
            situacaoSelecionada = userModel?.tipoUsuario ?? situacaoPickerData[0].id;
            //situacaoTextField.text = situacaoPickerData[0].description;
            self.fieldUserType.text = situacaoPickerData[0].description;
            for situacaoIndex in 0..<situacaoPickerData.count {
                if( situacaoSelecionada == situacaoPickerData[situacaoIndex].id ){
                    //situacaoTextField.text = situacaoPickerData[situacaoIndex].description;
                    self.fieldUserType.text = situacaoPickerData[situacaoIndex].description;
                    descricaoSituacaoSelecionada = situacaoPickerData[situacaoIndex].description;
                    situacaoPicker.selectRow(situacaoIndex, inComponent: 0, animated: false);
                    break
                }
            }
            
        }else{
            senhaLabel.isHidden = false;
            senhaTextField.isHidden = false;
            nomeTextField.text = "";
            emailTextField.text = "";
            emailTextField.isEnabled = true;
            
            self.fieldUserType.text = (situacaoPickerData[0].description);
            situacaoPicker.selectRow(0, inComponent: 0, animated: false);
            situacaoSelecionada = situacaoPickerData[0].id;
            descricaoSituacaoSelecionada = situacaoPickerData[0].description;
            
        }
        
    }
    
    func setupFields(){
        
        nomeTextField.delegate = self;
        emailTextField.delegate = self;
        senhaTextField.delegate = self;
        fieldUserType.delegate = self;
        
        fieldUserType.tag = 0;
        fieldUserType.delegate = self;
        fieldUserType.inputView = situacaoPicker;
        fieldUserType.text = "Selecione o Tipo";
        fieldUserType.dropDownStyle();
        
        clienteTextField.text = "Cliente não localizado";
        clienteTextField.isUserInteractionEnabled = false;
        
        genericAlert.view.backgroundColor = UIColor.black;
        genericAlert.view.alpha = 0.6;
        genericAlert.view.layer.cornerRadius = 15;
        genericAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        saveAlert.view.backgroundColor = UIColor.black;
        saveAlert.view.alpha = 0.6;
        saveAlert.view.layer.cornerRadius = 15;
        saveAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        setupLoadingSpin();
        
    }
    
    func setupKeyBoardToolBar(){
        toolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30));
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(
            title: "OK", style: .done, target: self,
            action: #selector(doneButtonAction)
            //action: Selector(("doneButtonAction"))
        );
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        self.nomeTextField.inputAccessoryView = toolbar;
        self.emailTextField.inputAccessoryView = toolbar;
        self.senhaTextField.inputAccessoryView = toolbar;
        self.fieldUserType.inputAccessoryView = toolbar;
    }
    
    
    func loadFromRealm(){
        customerSelected = Customer.loadFromId(id: customerId);
        self.labelUserType.isHidden = true;
        self.fieldUserType.isEnabled = false;
        self.fieldUserType.isHidden = true;
        if(customerId == "f6fef3d8-8839-48c2-898e-a7648b961e29"){
            self.labelUserType.isHidden = false;
            self.fieldUserType.isHidden = false;
            self.fieldUserType.isEnabled = true;
               
        }
        if(selectedUserId.count > 0){
            if(editMode){
                userModel = User.loadFromId(id: selectedUserId);
            }
        }
    
    }
    
    func loadData(){
        loadFromRealm();
    }
    
    func hideKeyboard(){
        self.fieldUserType.resignFirstResponder();
        self.nomeTextField.resignFirstResponder();
        self.emailTextField.resignFirstResponder();
    }
    
    func enableInteraction(_ enable: Bool){
        self.view.isUserInteractionEnabled = enable;
        self.saveButton.isEnabled = enable;
        self.navigationController?.navigationBar.isUserInteractionEnabled = enable;
    }
    
    
    func saveMode(_ on: Bool){
        self.enableInteraction(!on);
        if(on){
            saveLoading.startAnimating();
        }else{
            saveLoading.stopAnimating();
        }
    }
    
    func setupCustomerMode(){
        self.customerMode = (Session.getUsuarioLogadoTipo() == Constants.USUARIO_TIPO_CLIENTE_FINAL);
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "selectProductStock") {
            let nextVC = segue.destination as! ProductStockToSelectTbVC;
            nextVC.customerId = self.customerId;
        }
        if (segue.identifier == "selectProductByQrCode") {
            let nextVC = segue.destination as! QRCodeScannerVC;
            nextVC.vcFrom = String(describing: type(of: self));
        }
    }
    
    
    func createUserToInsert() -> UserCreateRequest {
        var tempObject: UserCreateRequest!;
        tempObject = UserCreateRequest();
        tempObject.id = UUID().uuidString;
        tempObject.appCompanyId = customerSelected!.id;
        tempObject.descricaoCliente = customerSelected!.nome;
        tempObject.email = emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "";
        tempObject.usuario = emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "";
        tempObject.descricao = nomeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "";
        tempObject.senhaInserir = senhaTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "";
        tempObject.ativo = isUserActive.isOn;
        return tempObject;
    }
    
    func createUserToUpdate(_ uuid: String?) -> UserUpdateRequest {
        
        var tempObject: UserUpdateRequest!;
        if (uuid != nil && uuid == "" ) {
            tempObject = UserUpdateRequest();
        } else {
            tempObject = UserUpdateRequest();
            tempObject.id = uuid ?? "";
        }
        tempObject.descricao = nomeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "";
        tempObject.ativo = isUserActive.isOn;    
        return tempObject;
    }
    
    
    func updateOnRealm() {
    
        if (internetOn()) {
            
            /* M = medico , E = enfermagem, T = tecnico,  @ = ultra,master,essencial */
            if (Session.getUsuarioLogadoTipo() == Constants.USUARIO_TIPO_CLIENTE_MEDICO
                || Session.getUsuarioLogadoTipo() == Constants.USUARIO_TIPO_MASTER ) {
                
                updateOnRealmIfMedic();
                
            } else if (Session.getUsuarioLogadoTipo() == Constants.USUARIO_TIPO_CLIENTE_FINAL) {
                
//                if (appointmentModel?.codSituacao == "3" || appointmentModel?.codSituacao == "4") {
//                    msgCantEditObject(false);
//                } else {
//                    updateOnRealmIfCustomer();
//                }
            } else {
                self.showAlert(vc: self, "Tipo do usuário não definido, faça login novamente.", "OK");
            }
        }
    }
    
    func insertOnRealm() {
        if (internetOn()) {
            
            self.saveMode(true);
            let tempDadosDaTela: UserCreateRequest = createUserToInsert();

            _ = self.loginService.postUserRx(request: self.loginService.postUserRequest(body: tempDadosDaTela))
                .observeOn(MainScheduler.instance)
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .subscribe(
                    onSuccess: { el in
                        let userToSave = User.convertResponseToObject(el);
                        User.addOrUpdate(objectToSave: userToSave);
                        self.saveMode(false);
                        self.showToastAndReturnScreen(message: "Usuário criado.", seconds: 2, closeView: true);
                        self.syncServer();
                },
                    onError: { err in
                        if(err == HttpStatusError.objectAlreadyExist){
                            self.showAlert(vc: self, "Erro ao tentar criar o usuário.", nil);
                        }else{
                            self.showAlert(vc: self, "Erro ao tentar criar o usuário", nil);
                        }
                        
                        self.saveMode(false);
                }
            )
        }
        
    }
    
    
    func updateOnRealmIfCustomer() {
        
        if (internetOn()) {
            
//            self.saveMode(true);
//            let tempDadosDaTela: Appointment = createAppointmentToSave("");
//            let success = Appointment.updateIfCustomer(id: appointmentModel!.id, objectToSave: tempDadosDaTela);
//
//            Customer.modifyBeforeUpdate(
//                id: customerSelected!.id,
//                usuarioCod: Session.getUsuarioLogadoId(), usuarioDescricao: Session.getUsuarioLogadoDescricao(), dataInformada: Date()
//            );
//
//            if(success){
//                self.syncServer();
//            }else{
//                self.showToastAndReturnScreen(message: "Erro ao tentar atualizar o chamado no celular", seconds: 2, closeView: false);
//            }
            
        }
        
    }
    
    func updateOnRealmIfMedic() {
        
        if (internetOn()) {
            self.saveMode(true);
            let tempDadosDaTela: UserUpdateRequest = createUserToUpdate(selectedUserId);
                _ = self.loginService.putUserRx(
                    request: self.loginService.putUserRequest(userId: selectedUserId, body: tempDadosDaTela)
                    )
                    .observeOn(MainScheduler.instance)
                    .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                    .subscribe(
                        onSuccess: { el in
                            let userToSave = User.convertResponseToObject(el);
                            User.addOrUpdate(objectToSave: userToSave);
                            self.saveMode(false);
                            self.showToastAndReturnScreen(message: "Usuário atualizado.", seconds: 2, closeView: true);
                            self.syncServer();
                    },
                        onError: { err in
                            if(err == HttpStatusError.objectAlreadyExist){
                                self.showAlert(vc: self, "Erro ao tentar atualizar o usuário.", nil);
                            }else{
                                self.showAlert(vc: self, "Erro ao tentar atualizar o usuário", nil);
                            }
                            
                            self.saveMode(false);
                    }
                )
        }
        
    }
    
    
    @objc func syncServer(){
        syncProcess.getDefaultSyncRx(token: Session.getToken(), serviceKey: Session.getServiceKey(),
                                     appCustomerId:  Session.getUsuarioLogadoCodCliente(), tipoUsuario: Session.getUsuarioLogadoTipo(),
                                     beginTimeStamp: Session.getKeySincronizacaoTimestampUltimaString()
            )
            .observeOn(MainScheduler.instance)
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(
                onNext: { el in
            },
                onError: { err in
                    if(self.editMode){
                        self.showToastAndReturnScreen(message: "Erro ao tentar atualizar o usuário", seconds: 2, closeView: false);
                    }else{
                        self.showToastAndReturnScreen(message: "Erro ao tentar salvar o usuário", seconds: 2, closeView: false);
                    }
            },
                onCompleted: { () in
                    self.setLastSync();
                    if(self.editMode){
                        self.showToastAndReturnScreen(
                            message: "usuário atualizado", seconds: 2, closeView: false
                        );
                    }else{
                        self.showToastAndReturnScreen(message: "usuário salvo", seconds: 2, closeView: true);
                    }
            },
                onDisposed: { () in
            }
            ).disposed(by: disposeBag);
        
    }
    
    func setLastSync(){
        UserDefaults.standard.set((0), forKey: Session.KEY_SINCRONIZACAO_TIMESTAMP_SERVER);
        let tempSyncTsServer: Double = Session.getSincronizacaoTsUltimaServer();
        let dateToSave: Date = Date();
        if (tempSyncTsServer == 0 || tempSyncTsServer < Session.getKeySincronizacaoTimestampUltima()) {
            UserDefaults.standard.set((dateToSave.getDeviceDateDisplayBrWithHour()), forKey: Session.KEY_SINCRONIZACAO_DATA_ULTIMA);
            UserDefaults.standard.set((dateToSave.toEpochInt64()), forKey: Session.KEY_SINCRONIZACAO_TIMESTAMP);
        } else {
            UserDefaults.standard.set((dateToSave.getDeviceDateDisplayBrWithHour()), forKey: Session.KEY_SINCRONIZACAO_DATA_ULTIMA);
            UserDefaults.standard.set((dateToSave.toEpochInt64()), forKey: Session.KEY_SINCRONIZACAO_TIMESTAMP);
        }
    }
    

    func createErroMsg(_ errors: String, _ newError: String) -> String {
        if (errors.count > 0){
            return errors + "\n" + newError;
        }else{
            return errors + " " + newError;
        }
    }
    
    
    func internetOn() -> Bool{
        if(!isInternetOn){
            showAlert(vc: self, "Sem internet não é possível realizar a operação.", nil);
        }
        return isInternetOn;
    }
    func isFieldsValidToSave() -> Bool{
        var hasErros = false;
        var errors = "";
        
        if(nomeTextField.text?.count == 0){
            errors = createErroMsg(errors, "Informe o nome do usuário.");
            hasErros = true;
        }
        if(emailTextField.text?.count == 0){
            errors = createErroMsg(errors, "Informe o email.");
            hasErros = true;
        }
        
        if(customerId == ""){
            errors = createErroMsg(errors, "Selecione o cliente.");
            hasErros = true;
        }
        
        if(editMode){
            if(selectedUserId == ""){
                errors = createErroMsg(errors, "Chamado sem código.");
                hasErros = true;
            }
        }else{
            if(senhaTextField.text?.count == 0){
                errors = createErroMsg(errors, "Informe a senha inicial.");
                hasErros = true;
            }
        }
        
        if(hasErros){
            self.showAlert(vc: self, errors, "OK");
        }
        
        return !hasErros;
    }
    
    @IBAction func saveAction(_ sender: Any) {
            if(isFieldsValidToSave()){
                if(editMode){
                    updateOnRealm();
                }else{
                    insertOnRealm();
                }
        }
    }
    
    
    
    // MARK: OBJC
    @objc func doneButtonAction() {
        self.view.endEditing(true)
    }
    
    @objc func returnTextView(gesture: UIGestureRecognizer) {
        guard (activeField != nil || activeTextView != nil) else {
            return
        }
        activeField?.resignFirstResponder()
        activeField = nil;
        activeTextView?.resignFirstResponder()
        activeTextView = nil;
    }
    
    
    
    // MARK: UIPicker
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 0 {
            return situacaoPickerData.count;
        }else{
            return 0;
        }
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0 {
            return situacaoPickerData[row].description;
        }else{
            return "";
        }
    }
    
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if (pickerView.tag == 0) {
            //situacaoTextField.text = situacaoPickerData[row].description;
            self.fieldUserType.text = situacaoPickerData[row].description;
            situacaoSelecionada = situacaoPickerData[row].id;
            descricaoSituacaoSelecionada = situacaoPickerData[row].description;
        }
    }
    
    func showAlert( vc: UIViewController, _ message: String,_ okBtnTitle: String?){
        vc.present(genericAlert, animated: true, completion: nil);
        genericAlert.setValue(message, forKey: "message");
        if(okBtnTitle != nil){
            genericAlert.actions[0].setValue((okBtnTitle ?? "OK"), forKey: "title");
        }
    }
    
    func showToastAndReturnScreen(message: String, seconds: Double, closeView: Bool) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert);
        alert.view.backgroundColor = UIColor.black;
        alert.view.alpha = 0.6;
        alert.view.layer.cornerRadius = 15;
        self.present(alert, animated: true);
        self.saveMode(false);
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds, execute: {
            [weak self] in
            guard let self = self else { return }
            alert.dismiss(animated: true);
            self.saveLoading.stopAnimating();
            if(closeView){
                self.closeView();
            }
        });
    }
    
    
    func msgCantEditObject(_ oldObject:Bool){
        self.showAlert(vc: self, "Não é possível editar um chamado finalizado ou cancelado", "OK");
    }
    
    func setupLoadingSpin(){
        saveLoading.center = view.center;
        saveLoading.color = UIColor.white;
        saveLoading.hidesWhenStopped = true;
        saveLoading.layer.cornerRadius = 05;
        saveLoading.isOpaque = false;
        saveLoading.backgroundColor = UIColor.black;
        saveLoading.alpha = 0.3;
        view.addSubview(saveLoading);
    }
    
    func closeView(){
        self.navigationController?.popViewController(animated: true);
        self.dismiss(animated: true, completion: nil);
    }
    
    func showToast(message: String, seconds: Double) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert);
        alert.view.backgroundColor = UIColor.black;
        alert.view.alpha = 0.6;
        alert.view.layer.cornerRadius = 15;
        self.present(alert, animated: true);
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds){
            alert.dismiss(animated: true);
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil);
    }
    
    
}



// MARK: UITextFieldDelegate
extension UserByCustomerVC: UITextFieldDelegate, UITextViewDelegate {
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeField = textField;
        lastOffset = self.scrollView.contentOffset;
        return true;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        activeField?.resignFirstResponder();
        activeField = nil;
        return true;
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        activeTextView = textView;
        lastOffset = self.scrollView.contentOffset;
        return true;
//        if(textView == self.equipamentoTextView){
//            activeField?.resignFirstResponder();
//            self.performSegue(withIdentifier: "selectProductStock", sender: self)
//            return false;
//        }else{
//            return true;
//        }
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        activeTextView?.resignFirstResponder();
        activeTextView = nil;
        return true;
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return (textField != self.fieldUserType);
    }
    
    
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        return (textField != self.situacaoTextField && textField != self.tecnicoTextField);
//    }
//
//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        if( textView == self.equipamentoTextView){
//            return false;
//        }else{
//            let existingLines = textView.text.components(separatedBy: CharacterSet.newlines)
//            let newLines = text.components(separatedBy: CharacterSet.newlines)
//            let linesAfterChange = existingLines.count + newLines.count - 1
//            if(text == "\n") {
//                return linesAfterChange <= textView.textContainer.maximumNumberOfLines
//            }
//            let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
//            let numberOfChars = newText.count
//            return numberOfChars <= 100;
//        }
//    }
}


// MARK: Keyboard Handling
extension UserByCustomerVC {
    
    @objc func keyboardWillShow(aNotification: NSNotification) {
        let info = aNotification.userInfo!;
        let kbSize: CGSize = (
            (info["UIKeyboardFrameEndUserInfoKey"] as? CGRect)?.size
            )!;
        let contentInsets: UIEdgeInsets = UIEdgeInsets(
            top: 0.0, left: 0.0, bottom: kbSize.height, right: 0.0
        );
        var frame: CGRect!;
        if(activeTextView != nil){
            frame = activeTextView!.frame;
        }
        if(activeField != nil){
            frame = activeField!.frame;
        }
        scrollView.contentInset = contentInsets;
        scrollView.scrollIndicatorInsets = contentInsets;
        var aRect: CGRect = self.view.frame;
        aRect.size.height -= kbSize.height;
        if (frame != nil){
            if !aRect.contains(frame.origin) {
                self.scrollView.scrollRectToVisible(frame, animated: true);
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.3) {
//            self.appointmentConstraintContentHeight.constant -= (self.keyboardHeight ?? 0);
//            if(self.lastOffset != nil){
//                self.scrollView.contentOffset = self.lastOffset;
//            }
        }
        keyboardHeight = nil;
    }
    

}
