//
//  CustomerResponse.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 26/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//
import Foundation


struct CustomerResponse: Codable {

    var id: String;
    var nome: String;
    var endereco: enderecoResponse?;
    
    var precisaSincronizar: Bool = false;
    var ativo: Bool = true;
    var deletado: Bool = false;
    var razaoSocial: String = "";
    var tipoPessoa: String = "";
    var cpf: String? = "";
    var cnpj: String? = "";
    var telefonePrincipal: String? = "";
    var celularPrincipal: String? = "";
    var email: String = "";
    var responsavel: String? = "";
    var responsavelContato: String? = "";
    var observacao: String? = "";
    var clienteEm: Date!;
    var clienteEmTimeStamp: Double? = 0.0;
    var ultimaVisitaTecnicaEm: Date!;
    var ultimaVisitaTecnicaEmTimeStamp: Double? = 0.0;
    var ultimaOrdemServicoEm: Date!;
    var ultimaOrdemServicoEmTimeStamp: Double? = 0.0;
    var codigoQuemCriou: String? = "";
    var codigoQuemAtualizou: String? = "";
    var criadoEm: Date!;
    var criadoPor: String? = "";
    var atualizadoPor: String? = "";
    var servidorAtualizadoEm: Date!;
    var atualizadoEm: Date!;
    var versao: Int? = 0;
    var versao_local: Int? = 0;
    var app_version_code: Int? =  0;
    var app_version_name: String? = "";
    var registro_talvez_duplicado: Bool? = false;
    var criado_local: Bool? = false;
    
    var criadoEmTimeStamp = 0.0;
    var atualizadoEmTimeStamp = 0.0;
    var servidorAtualizadoEmTimeStamp = 0.0;
    
    enum CodingKeys: String, CodingKey {
        case id
        case nome
        case endereco
        
        case precisaSincronizar;
        case ativo;
        case razaoSocial;
        case tipoPessoa;
        case cpf;
        case cnpj;
        case telefonePrincipal;
        case celularPrincipal;
        case email;
        case responsavel;
        case responsavelContato;
        case observacao;
        case clienteEm;
        case clienteEmTimeStamp;
        case ultimaVisitaTecnicaEm;
        case ultimaVisitaTecnicaEmTimeStamp;
        case ultimaOrdemServicoEm;
        case ultimaOrdemServicoEmTimeStamp;
        case codigoQuemCriou;
        case codigoQuemAtualizou;
        case criadoEm;
        case criadoPor;
        case atualizadoPor;
        case deletado;
        case servidorAtualizadoEm;
        case atualizadoEm;
        case versao;
        case versao_local;
        case app_version_code;
        case app_version_name;
        case registro_talvez_duplicado;
        case criadoEmTimeStamp;
        case atualizadoEmTimeStamp;
        case servidorAtualizadoEmTimeStamp;
        case criado_local;
    }
    
}


struct enderecoResponse: Codable {
    
    let bairro: String?;
    let cep: String?;
    let cidade: String?;
    let complemento: String?;
    let logradouro: String?;
    let numero: String?;
    let uf: String?;
    
    enum CodingKeys: String, CodingKey {
        case bairro;
        case cep;
        case cidade;
        case complemento;
        case logradouro;
        case numero;
        case uf;
    }
    
}

