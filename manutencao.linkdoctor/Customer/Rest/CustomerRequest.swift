//
//  CustomerRequest.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 01/06/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation

struct CustomerRequest: Codable {
    
    var id: String;
    var precisaSincronizar: Bool;
    var ativo: Bool;
    var deletado: Bool;
    var atualizadoPor: String = "";
    var atualizadoEm: Date!;
    var atualizadoEmTimeStamp: Double;
    var app_version_code: Int =  0;
    var app_version_name: String = "";
    var criado_local: Bool;
    
    var tipoPessoa: String;
    var cnpj: String;
    var cpf: String;
    var razaoSocial: String;
    var nome: String;
    var telefonePrincipal: String;
    var celularPrincipal: String;
    var email: String;
    var cep: String;
    var logradouro: String;
    var numero: String;
    var complemento: String;
    var bairro: String;

    enum CodingKeys: String, CodingKey {
        case id
        case precisaSincronizar
        case ativo
        case deletado
        case atualizadoPor
        case atualizadoEm
        case atualizadoEmTimeStamp
        case app_version_code
        case app_version_name
        case criado_local
        case tipoPessoa
        case cnpj
        case cpf
        case razaoSocial
        case nome
        case telefonePrincipal
        case celularPrincipal
        case email
        case cep
        case logradouro
        case numero
        case complemento
        case bairro
    }
    
}

