//
//  CustomerWithRecordSyncRequest.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 29/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation


class CustomerWithRecordSyncRequest: Codable {
    
    var deviceDescription: String;
    var appVersionCode: Int;
    var appVersionName: String;
    var customersToInsert: [CustomerWithRecordSync] = [];
    var customersToUpdate: [CustomerWithRecordSync] = [];
    
    init(customer: Customer, customersToInsert: [CustomerWithRecordSync], customersToUpdate: [CustomerWithRecordSync]){
        self.deviceDescription = "iOS";
        self.appVersionCode = 1;
        self.appVersionName = "iOS";
        self.customersToInsert.removeAll();
        self.customersToInsert.append(contentsOf: Array(customersToInsert));
        self.customersToUpdate.removeAll();
        self.customersToUpdate.append(contentsOf: Array(customersToUpdate));
    }
    
    init(){
        self.deviceDescription = "iOS";
        self.appVersionCode = 1;
        self.appVersionName = "iOS";
        self.customersToInsert.removeAll();
        self.customersToUpdate.removeAll();
    }
    
    enum CodingKeys: String, CodingKey {
        case deviceDescription
        case appVersionCode
        case appVersionName
        case customersToInsert
        case customersToUpdate
    }
    
    
}
