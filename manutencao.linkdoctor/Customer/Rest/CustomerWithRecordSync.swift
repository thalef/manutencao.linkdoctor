//
//  CustomerWithRecord.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 29/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation

class CustomerWithRecordSync: Codable {
    
    var customer: CustomerRequest;
    var appointmentsToInsert: [AppointmentRequest] = [];
    var appointmentsToUpdate: [AppointmentRequest] = [];
    var serviceOrdersToInsert: [ServiceOrderRequest] = [];
    var serviceOrdersToUpdate: [ServiceOrderRequest] = [];
    
    
    init(customer: CustomerRequest){
        self.customer = customer;
    }
    
    init(customer: CustomerRequest, appointmentsToInsert: [AppointmentRequest], appointmentsToUpdate: [AppointmentRequest]){
        self.customer = customer;
        self.appointmentsToInsert.removeAll();
        self.appointmentsToInsert.append(contentsOf: Array(appointmentsToInsert));
        self.appointmentsToUpdate.removeAll();
        self.appointmentsToUpdate.append(contentsOf: Array(appointmentsToUpdate));
//        self.serviceOrdersToInsert.removeAll();
//        self.serviceOrdersToInsert.append(contentsOf: Array([]));
//        self.serviceOrdersToUpdate.removeAll();
//        self.serviceOrdersToUpdate.append(contentsOf: Array([]));
    }
    init(customer: CustomerRequest, appointmentsToInsert: [AppointmentRequest], appointmentsToUpdate: [AppointmentRequest],
         serviceOrdersToInsert: [ServiceOrderRequest], serviceOrdersToUpdate: [ServiceOrderRequest]){
        self.customer = customer;
        self.appointmentsToInsert.removeAll();
        self.appointmentsToInsert.append(contentsOf: Array(appointmentsToInsert));
        self.appointmentsToUpdate.removeAll();
        self.appointmentsToUpdate.append(contentsOf: Array(appointmentsToUpdate));
        self.serviceOrdersToInsert.removeAll();
        self.serviceOrdersToInsert.append(contentsOf: Array(serviceOrdersToInsert));
        self.serviceOrdersToUpdate.removeAll();
        self.serviceOrdersToUpdate.append(contentsOf: Array(serviceOrdersToUpdate));
    }
    
    enum CodingKeys: String, CodingKey {
        case customer
        case appointmentsToInsert
        case appointmentsToUpdate
        case serviceOrdersToInsert
        case serviceOrdersToUpdate
    }
}
