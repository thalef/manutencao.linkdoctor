//
//  CustomerWithRecordsResponse.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 26/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation


struct CustomerWithRecordsResponse: Codable {
    
    let appointments: [AppointmentResponse];
    let customer: CustomerResponse;
    let productsInCustomer: [ProductStockResponse];
    let productsStockMoviments: [String];
    let servicesOrders: [ServiceOrderResponse];
    
    enum CodingKeys: String, CodingKey {
        case appointments
        case customer
        case productsInCustomer
        case productsStockMoviments
        case servicesOrders
    }
    
}
