//
//  FirstViewController.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 06/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import UIKit
import RealmSwift

class TabCustomerViewController: UIViewController {
    
    private var customerSelected: Customer?;
    private var productTypeList: Results<ProductType>?;
    private var usersFromCustomer: Results<User>?;
    private var productTypeSelected: ProductType?;
    var customerMode: Bool = true;
    var companyNotMasterMode: Bool = true;
    var customerId: String = "";
    var qrCodeSelected: String = "";
    let genericAlert = UIAlertController(title: nil, message: "", preferredStyle: .alert);
    let saveLoading = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge);
    
    @IBOutlet weak var customerTitleLabel: UILabel!
    @IBOutlet weak var customerSubTitleLabel: UILabel!
    @IBOutlet weak var boundProductByQRCodeBtn: UIButton!
    @IBOutlet weak var editCustomerBtn: UIButton!
    
    @IBOutlet weak var userListCounterTitleLabel: UILabel!
    @IBOutlet weak var userListCounterLabel: UILabel!
    @IBOutlet weak var userListByCustomerBtn: UIButton!
    @IBOutlet weak var userListCard: EssCardView!
    
    @IBOutlet weak var customerStackView: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData();
        setupCustomerMode();
        setupFields();
        self.navigationController?.navigationBar.prefersLargeTitles  = true;
        self.navigationController?.navigationBar.isHidden = false;
    }
    
    func loadData(){
        customerSelected = Customer.loadFromId(id: customerId);
        productTypeList = ProductType.loadAllFromRealmActive();
        usersFromCustomer = User.loadAllFromCustomerRealm(customerId: customerId);
        //usersFromCustomer = User.loadAllFromRealm();
        
    }
    
    func setupFields(){
        self.tabBarController?.title = customerSelected?.nome.uppercased() ?? "Dados do cliente";
        customerTitleLabel.text = customerSelected?.nome.uppercased() ?? "Nome não informado";
        customerSubTitleLabel.text = customerSelected?.formatAdressToScreen() ?? "Endereço não informado";
        genericAlert.view.backgroundColor = UIColor.black;
        genericAlert.view.alpha = 0.6;
        genericAlert.view.layer.cornerRadius = 15;
        genericAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        setupLoadingSpin();
        
        
        if(self.customerMode ){
            self.boundProductByQRCodeBtn.isHidden = true;
            self.userListByCustomerBtn.isHidden = true;
            self.userListCard.isHidden = true;
            self.editCustomerBtn.isHidden = true;
        }else{
            self.boundProductByQRCodeBtn.isHidden = false;
            self.userListByCustomerBtn.isHidden = false;
            self.editCustomerBtn.isHidden = false;
            self.userListCard.isHidden = false;
            if( (usersFromCustomer?.count ?? 0) > 0){
                userListCounterLabel.text = "\(usersFromCustomer?.count ?? 0) Usuários criados";
            }else{
                userListCounterLabel.text = "0 Usuários criados";
            }
        }
        if(companyNotMasterMode){
            self.userListCard.isHidden = true;
        }
        
    }
    
    func setupCustomerMode(){
        self.customerMode = (Session.getUsuarioLogadoTipo() == Constants.USUARIO_TIPO_CLIENTE_FINAL);
        self.companyNotMasterMode = (!self.customerMode && (Session.getUsuarioLogadoTipo() == Constants.USUARIO_TIPO_EMPRESA_ADM || Session.getUsuarioLogadoTipo() == Constants.USUARIO_TIPO_EMPRESA_TECNICO))
    }
    @IBAction func boundProductByQRCode(_ sender: Any) {
        self.performSegue(withIdentifier: "boundProductByQRCodeSegue", sender: self)
    }
    @IBAction func editCustomer(_ sender: Any) {
        self.performSegue(withIdentifier: "goToEditCustomer", sender: self)
    }
    
    @IBAction func getSelectedProdFromQRCodeToBound(_ unwindSegue: UIStoryboardSegue) {
        var prodFound: Bool = false;
        let qRCodeScannerVC = unwindSegue.source as? QRCodeScannerVC;
        productTypeSelected = nil;
        qrCodeSelected = qRCodeScannerVC?.selectedProductId ?? "";
        qRCodeScannerVC?.dismiss(animated: false, completion: nil);
        qRCodeScannerVC?.closeView();
        prodFound = qrCodeSelected.count > 0;
        
        if(!prodFound){
            showAlert(vc: self, "Não foi possível ler o QRCode.", nil);
        }else{
            if( (productTypeList?.count ?? 0) > 0){
                showToastAndReturnScreen(message: "QRCode lido.", seconds: 2);
            }else{
                showAlert(vc: self, "O QRcode foi lido mas\nnão existe equipamentos para vincular.\n Sincroniza o app e tente novamente.", nil);
            }
        }
        
    }
    
    @IBAction func userListCustomer(_ sender: Any) {
        self.performSegue(withIdentifier: "goToUserListByCustomerVCSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "boundProductByQRCodeSegue") {
            let nextVC = segue.destination as! QRCodeScannerVC;
            nextVC.vcFrom = String(describing: type(of: self));
        }
        if (segue.identifier == "goToQrCodeBoundVCSegue") {
            let nextVC = segue.destination as! ProductBoundQrCodeVC;
            nextVC.customerId = customerId;
            nextVC.selectedQRCodeId = qrCodeSelected;
        }
        if (segue.identifier == "goToUserListByCustomerVCSegue") {
            let nextVC = segue.destination as! UserListByCustomerVC;
            nextVC.customerId = customerId;
        }
        if (segue.identifier == "goToEditCustomer") {
            let nextVC = segue.destination as! CustomerVC;
            nextVC.selectedCustomerId = customerId;
            nextVC.editMode = true;
        }
        
        qrCodeSelected = "";
    }
    
    func showAlert( vc: UIViewController, _ message: String,_ okBtnTitle: String?){
        vc.present(genericAlert, animated: true, completion: nil);
        genericAlert.setValue(message, forKey: "message");
        if(okBtnTitle != nil){
            genericAlert.actions[0].setValue((okBtnTitle ?? "OK"), forKey: "title");
        }
    }
    
    func loadingMode(_ on: Bool){
        if(on){
            saveLoading.startAnimating();
        }else{
            saveLoading.stopAnimating();
        }
    }
    
    func setupLoadingSpin(){
        saveLoading.center = view.center;
        saveLoading.color = UIColor.white;
        saveLoading.hidesWhenStopped = true;
        saveLoading.layer.cornerRadius = 05;
        saveLoading.isOpaque = false;
        saveLoading.backgroundColor = UIColor.black;
        saveLoading.alpha = 0.3;
        view.addSubview(saveLoading);
    }
    
    func showToastAndReturnScreen(message: String, seconds: Double) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert);
        alert.view.backgroundColor = UIColor.black;
        alert.view.alpha = 0.6;
        alert.view.layer.cornerRadius = 15;
        self.present(alert, animated: true);
        self.loadingMode(false);
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds, execute: {
            [weak self] in
            guard let self = self else { return }
            alert.dismiss(animated: true);
            self.saveLoading.stopAnimating();
            self.performSegue(withIdentifier: "goToQrCodeBoundVCSegue", sender: self)
        });
    }
    
    
}

