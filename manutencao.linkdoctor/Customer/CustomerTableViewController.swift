//
//  CustomerTableViewController.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 06/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import UIKit
import RealmSwift
import RxSwift
import RxCocoa


class CustomerTableViewController: UITableViewController , UISearchResultsUpdating {
    
    
    //UISearchBarDelegate {
    
    var selectedCustomerId: String = "";
    private var customersList: Results<Customer>?
    var notificationToken: NotificationToken? = nil
    let syncProcess = SyncProcessHelper();
    let disposeBag = DisposeBag();
    let firstLoadingAlert = UIAlertController(title: nil, message: "Carregando dados pela primeira vez", preferredStyle: .alert);
    let firstLoginLoadingSping = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge);
    
    var searchController: UISearchController!
    
    @IBOutlet weak var newCustomerBtn: UIBarButtonItem!
    
    @IBOutlet weak var menuQuitButton: UIBarButtonItem!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true;
        
        if((Session.getUsuarioLogadoTipo() == Constants.USUARIO_TIPO_CLIENTE_FINAL)){
            self.title = "Filiais";
        }
        setupFields();
        setupPullToRefresh();
        loadData();
        loadFromServer();
        
        if #available(iOS 13.0, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.backgroundColor = UIColor.primaryDark;
            navigationController?.navigationBar.standardAppearance = navBarAppearance
            navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.tableView.reloadData();
    }
    
    func loadFromServer(){
        if(UserDefaults.standard.bool(forKey: Session.KEY_FIRST_LOGIN)){
            firstLogin();
        }else{
            defaultLogin();
//            if((Session.getUsuarioLogadoTipo() == Constants.USUARIO_TIPO_CLIENTE_FINAL)){
//            self.performSegue(withIdentifier: "customerSelectedSegue", sender: self);
//            }
        }
    }
    
    func setupRealm(){
        //realm = try! Realm();
    }
    
    func setupPullToRefresh(){
        self.refreshControl?.addTarget(self, action: #selector(syncServer), for: .valueChanged)
    }
    
    func firstLogin(){
//        let vc: UIViewController = self;
//        showFirstLoadAlert(true, vc: vc);
        self.firstLoginLoadingSping.startAnimating();
        syncProcess.getFirstTimeSyncRx(token: Session.getToken(), serviceKey: Session.getServiceKey(), appCustomerId: Session.getUsuarioLogadoCodCliente(), tipoUsuario: Session.getUsuarioLogadoTipo())
            .observeOn(MainScheduler.instance)
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(
                onNext: { el in
            },
                onError: { err in
                    print(err)
                    self.firstLoginLoadingSping.stopAnimating();
                    self.firstLoginFailed(showMessage: true, goToLogin: false, goToLoginWitDelay: true);
                    
            },
                onCompleted: { () in
                    UserDefaults.standard.set(false, forKey: Session.KEY_FIRST_LOGIN);
                    LoginSwitcher.updateRootVC();
//                    self.showFirstLoadAlert(false, vc: vc);
                    self.firstLoginLoadingSping.stopAnimating();
            },
                onDisposed: { () in
            }
            ).disposed(by: disposeBag);
    }
    
    func firstLoginFailed(showMessage: Bool, goToLogin: Bool, goToLoginWitDelay: Bool){
        UserDefaults.standard.set(false, forKey: Session.KEY_IS_LOGGED_IN);
        UserDefaults.standard.set(true, forKey: Session.KEY_FIRST_LOGIN);
        UserDefaults.standard.set("", forKey: Session.KEY_TOKEN_API);
        UserDefaults.standard.set("", forKey: Session.KEY_USUARIO_LOGADO_ID);
        UserDefaults.standard.set("", forKey: Session.KEY_USUARIO_LOGADO_DESCRICAO);
        UserDefaults.standard.set("", forKey: Session.KEY_USUARIO_LOGADO_EMAIL);
        UserDefaults.standard.set("", forKey: Session.KEY_USUARIO_LOGADO_TIPO);
        UserDefaults.standard.set("", forKey: Session.KEY_USUARIO_LOGADO_COD_CLIENTE);
        UserDefaults.standard.set(0, forKey: Session.KEY_SINCRONIZACAO_TIMESTAMP);
        UserDefaults.standard.set(nil, forKey: Session.KEY_SINCRONIZACAO_DATA_ULTIMA);
        let realm = try! Realm();
        try! realm.write {
            realm.deleteAll();
        }
        if(showMessage){
            self.firstLoadingAlert.setValue("Erro ao tentar carregar os dados pela primeira vez. Entre em contato com o suporte@essencial-ti.com.br.", forKey: "message");
            firstLoadingAlert.actions[0].setValue("Fechando o app...", forKey: "title");
            if(goToLoginWitDelay){
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 15){
                    self.performSegue(withIdentifier: "customerLogoutSegue", sender: self)
                }
            }
        }
        if(goToLogin){
            self.performSegue(withIdentifier: "customerLogoutSegue", sender: self)
        }
    }
    
    func defaultLogin(){
        firstLoginLoadingSping.startAnimating();
        syncProcess.getDefaultSyncRx(
            token: Session.getToken(), serviceKey: Session.getServiceKey(),
            appCustomerId: Session.getUsuarioLogadoCodCliente(), tipoUsuario: Session.getUsuarioLogadoTipo(),
            beginTimeStamp: Session.getKeySincronizacaoTimestampUltimaString()
            )
            .observeOn(MainScheduler.instance)
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(
                onNext: { el in
            },
                onError: { err in
                    self.showToast(message: "Erro ao tentar sincronizar.", seconds: 2.0);
            },
                onCompleted: { () in
                    self.setLastSync();
            },
                onDisposed: { () in
                    self.firstLoginLoadingSping.stopAnimating();
            }
            ).disposed(by: disposeBag);
    }
    
    @objc func syncServer(refreshControl: UIRefreshControl){
        syncProcess.getDefaultSyncRx(token: Session.getToken(), serviceKey: Session.getServiceKey(),
            appCustomerId:  Session.getUsuarioLogadoCodCliente(), tipoUsuario: Session.getUsuarioLogadoTipo(),
            beginTimeStamp: Session.getKeySincronizacaoTimestampUltimaString()
            )
            .observeOn(MainScheduler.instance)
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(
                onNext: { el in
            },
                onError: { err in
                    refreshControl.endRefreshing();
                    self.showToast(message: "Erro ao sincronizar.", seconds: 2.0);
            },
                onCompleted: { () in
                    self.setLastSync();
                    refreshControl.endRefreshing();
            },
                onDisposed: { () in
                    refreshControl.endRefreshing();
            }
            ).disposed(by: disposeBag);
        
    }
    
    func setLastSync(){
        UserDefaults.standard.set((0), forKey: Session.KEY_SINCRONIZACAO_TIMESTAMP_SERVER);
        let tempSyncTsServer: Double = Session.getSincronizacaoTsUltimaServer();
        let dateToSave: Date = Date();
        if (tempSyncTsServer == 0 || tempSyncTsServer < Session.getKeySincronizacaoTimestampUltima()) {
            UserDefaults.standard.set((dateToSave.getDeviceDateDisplayBrWithHour()), forKey: Session.KEY_SINCRONIZACAO_DATA_ULTIMA);
            UserDefaults.standard.set((dateToSave.toEpochInt64()), forKey: Session.KEY_SINCRONIZACAO_TIMESTAMP);
        } else {
            UserDefaults.standard.set((dateToSave.getDeviceDateDisplayBrWithHour()), forKey: Session.KEY_SINCRONIZACAO_DATA_ULTIMA);
            UserDefaults.standard.set((dateToSave.toEpochInt64()), forKey: Session.KEY_SINCRONIZACAO_TIMESTAMP);
        }
    }
    
    func loadFromRealm(){
        customersList = Customer.loadAllFromRealmActive();
    }
    
    func loadData(){
        loadFromRealm();
        notificationToken = customersList?.observe { [weak self] (changes: RealmCollectionChange) in
            guard let tableView = self?.tableView else { return }
            switch changes {
            case .initial:
                // Results are now populated and can be accessed without blocking the UI
                tableView.reloadData()
            case .update(_, let deletions, let insertions, let modifications):
                // Query results have changed, so apply them to the UITableView
                tableView.beginUpdates()
                tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0)}),
                                     with: .automatic)
                tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.endUpdates()
            case .error(let error):
                // An error occurred while opening the Realm file on the background worker thread
                fatalError("\(error)")
            }
        }
    }
    
    
    func updateSearchResults(for searchController: UISearchController) {
        if let searchText = searchController.searchBar.text {
            if(searchText.isEmpty){
                loadFromRealm();
            }else{
                customersList = customersList?.filter("\(Constants.CAMPO_NOME) contains[cd] %@", searchText);
            }
            self.tableView.reloadData();
        }else{
            loadFromRealm();
        }
    }
    
    func setupFields(){
        setupSearchBar();
        firstLoadingAlert.view.backgroundColor = UIColor.black;
        firstLoadingAlert.view.alpha = 0.6;
        firstLoadingAlert.view.layer.cornerRadius = 15;
        firstLoadingAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        setupLoadingSpin();
        if( Session.getUsuarioLogadoTipo() != Constants.USUARIO_TIPO_CLIENTE_MEDICO && Session.getUsuarioLogadoTipo() != Constants.USUARIO_TIPO_MASTER && Session.getUsuarioLogadoTipo() != Constants.USUARIO_TIPO_EMPRESA_ADM){
            self.newCustomerBtn.isEnabled = false;
            self.newCustomerBtn.tintColor = UIColor.clear;
        }
        
    }
    
    func setupSearchBar(){
        searchController = UISearchController(searchResultsController: nil);
        searchController.searchResultsUpdater = self;
//        searchController.dimsBackgroundDuringPresentation = false;
        searchController.obscuresBackgroundDuringPresentation = false;
        
        searchController.searchBar.sizeToFit();
        searchController.searchBar.setValue("Cancelar", forKey: "cancelButtonText");
        searchController.searchBar.placeholder = "Buscar";
        tableView.tableHeaderView = searchController.searchBar;
        definesPresentationContext = true;
    }
    
    @IBAction func menuQuitButtonAction(_ sender: Any) {
        self.firstLoginFailed(showMessage: false, goToLogin: true, goToLoginWitDelay:  false);
    }
    
    func showFirstLoadAlert(_ show: Bool, vc: UIViewController){
        if(show){
            vc.present(firstLoadingAlert, animated: true, completion: nil);
            firstLoadingAlert.actions[0].setValue("Aguarde...", forKey: "title");
            firstLoadingAlert.actions[0].isEnabled = false;
            firstLoginLoadingSping.startAnimating();
        }else{
            firstLoadingAlert.setValue("Sincronização finalizada.", forKey: "message");
            firstLoadingAlert.actions[0].setValue("OK", forKey: "title");
            firstLoadingAlert.actions[0].isEnabled = true;
            firstLoginLoadingSping.stopAnimating();
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3){
                self.firstLoadingAlert.dismiss(animated: true);
            }
        }
    }
    
    func setupLoadingSpin(){
        firstLoginLoadingSping.center = view.center;
        firstLoginLoadingSping.color = UIColor.white;
        firstLoginLoadingSping.hidesWhenStopped = true;
        firstLoginLoadingSping.layer.cornerRadius = 05;
        firstLoginLoadingSping.isOpaque = false;
        firstLoginLoadingSping.backgroundColor = UIColor.black;
        firstLoginLoadingSping.alpha = 0.3;
        view.addSubview(firstLoginLoadingSping);
    }
    
    @IBAction func newCustomerAction(_ sender: Any) {
        self.performSegue(withIdentifier: "goToNewCustomer", sender: self)
    }
    
    func showToast(message: String, seconds: Double) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert);
        alert.view.backgroundColor = UIColor.black;
        alert.view.alpha = 0.6;
        alert.view.layer.cornerRadius = 15;
        self.present(alert, animated: true);
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds){
            alert.dismiss(animated: true);
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return customersList?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "CustomersListCell", for: indexPath)
        let customer = self.customersList?[(indexPath as NSIndexPath).row]
        cell.textLabel!.text = customer?.nome;
        cell.detailTextLabel!.text = customer?.logradouro;
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCustomerId = self.customersList?[(indexPath as NSIndexPath).row].id ?? "";
        self.performSegue(withIdentifier: "customerSelectedSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "customerSelectedSegue") {
            let tabBarController = segue.destination as! TabBarCustomerViewController;
            tabBarController.customerId = self.selectedCustomerId;
            let tabCustomerViewController = self.storyboard!
                .instantiateViewController(withIdentifier: "TabCustomerViewController") as! TabCustomerViewController;
            tabCustomerViewController.customerId = self.selectedCustomerId;
        }
        if (segue.identifier == "customerLogoutSegue") {
            //            let tabBarController = segue.destination as! TabBarCustomerViewController;
            //            tabBarController.customerId = self.selectedCustomerId;
            //            let tabCustomerViewController = self.storyboard!.instantiateViewController(withIdentifier: "TabCustomerViewController") as! TabCustomerViewController;
            //            tabCustomerViewController.customerId = self.selectedCustomerId;
        }
        if (segue.identifier == "goToNewCustomer") {
            let nextVC = segue.destination as! CustomerVC;
            nextVC.selectedCustomerId = "";
            nextVC.editMode = false;
        }
    }
    
    
    deinit {
        notificationToken?.invalidate()
    }
    
    
}
