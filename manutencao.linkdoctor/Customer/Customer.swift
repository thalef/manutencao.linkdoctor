//
//  Customer.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 06/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class Customer: Object {
    
    @objc dynamic var id: String = "";
    
    //@Index
    @objc dynamic var precisaSincronizar: Bool = false;
    //@Index
    @objc dynamic var ativo: Bool = true;
    
    @objc dynamic var razaoSocial: String = "";
    @objc dynamic var nome: String = "";
    @objc dynamic var tipoPessoa: String = "";
    @objc dynamic var cpf: String = "";
    @objc dynamic var cnpj: String = "";
    @objc dynamic var telefonePrincipal: String = "";
    @objc dynamic var celularPrincipal: String = "";
    @objc dynamic var email: String = "";
    @objc dynamic var responsavel: String = "";
    @objc dynamic var responsavelContato: String = "";
    @objc dynamic var observacao: String = "";
    
    @objc dynamic var clienteEm: Date!;
    @objc dynamic var clienteEmTimeStamp: Double = 0.0;
    @objc dynamic var ultimaVisitaTecnicaEm: Date!;
    @objc dynamic var ultimaVisitaTecnicaEmTimeStamp: Double = 0.0;
    @objc dynamic var ultimaOrdemServicoEm: Date!;
    @objc dynamic var ultimaOrdemServicoEmTimeStamp: Double = 0.0;
    
    @objc dynamic var codigoQuemCriou: String = "";
    @objc dynamic var codigoQuemAtualizou: String = "";
    @objc dynamic var criadoEm: Date!;
    @objc dynamic var criadoPor: String = "";
    @objc dynamic var atualizadoPor: String = "";
    @objc dynamic var deletado: Bool = false;
    @objc dynamic var criado_local: Bool = false;
    @objc dynamic var servidorAtualizadoEm: Date!;
    @objc dynamic var atualizadoEm: Date!;
    @objc dynamic var versao: Int = 0;
    @objc dynamic var versao_local: Int = 0;
    @objc dynamic var app_version_code: Int =  0;
    @objc dynamic var app_version_name: String = "";
    @objc dynamic var registro_talvez_duplicado: Bool = false;
    @objc dynamic var criadoEmTimeStamp = 0.0;
    @objc dynamic var atualizadoEmTimeStamp = 0.0;
    @objc dynamic var servidorAtualizadoEmTimeStamp = 0.0;
    
    
    @objc dynamic var bairro: String = "";
    @objc dynamic var cep: String = "";
    @objc dynamic var cidade: String = "";
    @objc dynamic var complemento: String = "";
    @objc dynamic var logradouro: String = "";
    @objc dynamic var numero: String = "";
    @objc dynamic var uf: String = "";
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(id: String, nome: String, endereco: enderecoResponse?, objToConvert: CustomerResponse) {
        self.init();
        self.id = id;
        self.nome = nome;
        
        self.precisaSincronizar = objToConvert.precisaSincronizar;
        self.ativo = objToConvert.ativo;
        self.razaoSocial = objToConvert.razaoSocial;
        self.tipoPessoa = objToConvert.tipoPessoa;
        self.cpf = objToConvert.cpf ?? "";
        self.cnpj = objToConvert.cnpj ?? "";
        self.telefonePrincipal = objToConvert.telefonePrincipal  ?? "";
        self.celularPrincipal = objToConvert.celularPrincipal  ?? "";
        self.email = objToConvert.email;
        self.responsavel = objToConvert.responsavel  ?? "";
        self.responsavelContato = objToConvert.responsavelContato  ?? "";
        self.observacao = objToConvert.observacao  ?? "";
        
        self.clienteEm = objToConvert.clienteEm;
        self.clienteEmTimeStamp = objToConvert.clienteEmTimeStamp ?? 0.0;
        self.ultimaVisitaTecnicaEm = objToConvert.ultimaVisitaTecnicaEm;
        self.ultimaVisitaTecnicaEmTimeStamp = objToConvert.ultimaVisitaTecnicaEmTimeStamp ?? 0.0;
        self.ultimaOrdemServicoEm  = objToConvert.ultimaOrdemServicoEm;
        self.ultimaOrdemServicoEmTimeStamp = objToConvert.ultimaOrdemServicoEmTimeStamp ?? 0.0;
        
        self.codigoQuemCriou = objToConvert.codigoQuemCriou ?? "";
        self.codigoQuemAtualizou = objToConvert.codigoQuemAtualizou ?? "";
        self.criadoEm = objToConvert.criadoEm;
        self.criadoPor = objToConvert.criadoPor ?? "";
        self.atualizadoPor = objToConvert.atualizadoPor ?? "";
        self.deletado = objToConvert.deletado;
        self.servidorAtualizadoEm = objToConvert.servidorAtualizadoEm;
        self.atualizadoEm = objToConvert.atualizadoEm;
        self.versao = objToConvert.versao ?? 0;
        self.versao_local = objToConvert.versao_local ?? 0;
        self.app_version_code = 0;
        self.app_version_name = "ios";
        self.registro_talvez_duplicado = objToConvert.registro_talvez_duplicado  ?? false;
        self.criadoEmTimeStamp = objToConvert.criadoEmTimeStamp;
        self.atualizadoEmTimeStamp = objToConvert.atualizadoEmTimeStamp;
        self.servidorAtualizadoEmTimeStamp = objToConvert.servidorAtualizadoEmTimeStamp;
        self.criado_local = objToConvert.criado_local  ?? false;
        
        if(endereco != nil){
            self.bairro = endereco?.bairro ?? "";
            self.cep = endereco?.cep ?? "";
            self.cidade = endereco?.cidade ??  "";
            self.complemento = endereco?.complemento ??  "";
            self.logradouro = endereco?.logradouro ??  "";
            self.numero = endereco?.numero ??  "";
            self.uf = endereco?.uf ??  "";
        }else{
            self.bairro = "";
            self.cep = "";
            self.cidade = "";
            self.complemento = "";
            self.logradouro = "";
            self.numero = "";
            self.uf = "";
        }
    }
    
    func modificaAntesAtualizar(_ usuarioCod: String, _ usuarioDescricao: String, _ dataInformada:Date) {
        self.codigoQuemAtualizou = usuarioCod;
        self.atualizadoPor = usuarioDescricao;
        self.atualizadoEm = dataInformada;
        self.precisaSincronizar = true;
        self.atualizadoEmTimeStamp = Double(Date().toEpochInt64());
        //this.app_version_code = Utilidades.getAppVersionCode();
        //this.app_version_name = Utilidades.getAppVersionName();
    }
    
    func formatAdressToScreen() -> String {
        return (logradouro.count > 0) ? logradouro + ", " + numero + " - " + bairro + ", " + cep : "Sem endereço informado.";
    }
    
}

// MARK: - CRUD methods

extension Customer {
    
    
    
    static func convertObjectToRequest(_ customer: Customer) -> CustomerRequest {
        let objToReturn: CustomerRequest = CustomerRequest(
            id: customer.id,
            precisaSincronizar:  customer.precisaSincronizar,
            ativo: customer.ativo,
            deletado:  customer.deletado,
            atualizadoPor: customer.atualizadoPor,
            atualizadoEm: customer.atualizadoEm,
            atualizadoEmTimeStamp: customer.atualizadoEmTimeStamp,
            app_version_code: customer.app_version_code,
            app_version_name: customer.app_version_name,
            criado_local: customer.criado_local,
            tipoPessoa: customer.tipoPessoa,
            cnpj : customer.cnpj,
            cpf: customer.cpf,
            razaoSocial: customer.razaoSocial,
            nome: customer.nome,
            telefonePrincipal: customer.telefonePrincipal,
            celularPrincipal: customer.celularPrincipal,
            email: customer.email,
            cep: customer.cep,
            logradouro: customer.logradouro,
            numero: customer.numero,
            complemento: customer.complemento,
            bairro : customer.bairro
        );
        return objToReturn;
    }

    
    
    static func convertResponseToObject(_ customer: CustomerResponse) -> Customer {
        return Customer(id: customer.id, nome: customer.nome, endereco: customer.endereco, objToConvert: customer);
    }
    
    static func converListResponseToListObject(_ listToConvert: [CustomerResponse]) -> [Customer] {
        var list: [Customer] = [];
        for obj in listToConvert {
            list.append(convertResponseToObject(obj));
        }
        return list;
    }
    
    static func loadFromId(in realm: Realm = try! Realm(), id: String) -> Customer? {
        return realm.objects(Customer.self)
            .filter("id = '\(id)' and ativo = true and deletado = false")
            .first;
    }
    
    static func loadAllFromRealm(in realm: Realm = try! Realm()) -> Results<Customer> {
        return realm.objects(Customer.self)
            .filter("ativo = true and deletado = false")
            .sorted(byKeyPath: "nome")
    }
    
    static func loadAllFromRealmActive(in realm: Realm = try! Realm()) -> Results<Customer> {
        return realm.objects(Customer.self)
            .filter("\(Constants.CAMPO_ATIVO) = true AND \(Constants.CAMPO_DELETADO) = false")
            .sorted(byKeyPath: "nome")
    }
    
    static func loadNeedSync(in realm: Realm = try! Realm()) -> Results<Customer> {
        return realm.objects(Customer.self)
            .filter("\(Constants.CAMPO_PRECISA_SINCRONIZAR) = true")
    }
    
    @discardableResult
    static func add(objectToSave: Customer, in realm: Realm = try! Realm())
        -> Customer {
            let item = objectToSave;
            try! realm.write {
                realm.add(item)
            }
            return item
    }
    
    @discardableResult
    static func addOrUpdate(objectToSave: Customer, in realm: Realm = try! Realm())
        -> Customer {
            let item = objectToSave;
            try! realm.write {
                realm.add(item, update: .all);
            }
            return item
    }
    
    @discardableResult
    static func modifyBeforeUpdate(id: String, usuarioCod: String, usuarioDescricao: String, dataInformada: Date, in realm: Realm = try! Realm())
        -> Customer? {
            let item: Customer? = realm.objects(Customer.self).filter("id = '\(id)'").first;
            if(item != nil){
                try! realm.write {
                    //realm.add(item, update: true);
                    item!.codigoQuemAtualizou = usuarioCod;
                    item!.atualizadoPor = usuarioDescricao;
                    item!.atualizadoEm = dataInformada;
                    item!.precisaSincronizar = true;
                    item!.atualizadoEmTimeStamp = Double(Date().toEpochInt64());
                    
                }
            }
            return item;
    }
    
    
    @discardableResult
    static func addAll(listToSave: [Customer], in realm: Realm = try! Realm())
        -> [Customer] {
            realm.beginWrite();
            for user in listToSave
            {
                realm.add(user)
            }
            try! realm.commitWrite()
            return listToSave
    }
    
    @discardableResult
    static func addOrUpdateAll(listToSave: [Customer], in realm: Realm = try! Realm())
        -> [Customer] {
            //try! realm.beginWrite();
            realm.beginWrite();
            realm.add(listToSave, update: .all)
            try! realm.commitWrite()
            return listToSave
    }
    
    //    func toggleCompleted() {
    //        guard let realm = realm else { return }
    //        try! realm.write {
    //            isCompleted = !isCompleted
    //        }
    //    }
    //
    //    func delete() {
    //        guard let realm = realm else { return }
    //        try! realm.write {
    //            realm.delete(self)
    //        }
    //    }
}
