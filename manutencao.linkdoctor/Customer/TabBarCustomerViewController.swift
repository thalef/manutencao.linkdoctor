//
//  TabBarCustomerViewController.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 06/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import UIKit

class TabBarCustomerViewController: UITabBarController,UITabBarControllerDelegate {
    
    var customerId: String = "";
    var customerName: String?;
    var addButton: UIBarButtonItem!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        
        self.addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped));
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Voltar", style: .plain, target: nil, action: nil)
        showRightMenuButtom(false);
        navigationItem.rightBarButtonItem = self.addButton;
        
        if let tabCustomerController = self.viewControllers?[0] as? TabCustomerViewController{
            tabCustomerController.customerId = self.customerId;
        }
        if let tabProductStockController = self.viewControllers?[1] as? TabProductStockTableViewCtrl{
            tabProductStockController.customerId = self.customerId;
        }
        if let tabServiceOrderController = self.viewControllers?[2] as? TabServiceOrdersTableViewController{
            tabServiceOrderController.customerId = self.customerId;
        }
        if let tabAppointmentController = self.viewControllers?[3] as? TabAppointmentTableViewController{
            tabAppointmentController.customerId = self.customerId;
        }
        
    }
    
    
    @objc func addTapped(){
        if (self.selectedViewController as? TabAppointmentTableViewController) != nil{
            self.performSegue(withIdentifier: "newAppointmentDetail", sender: self)
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        switch tabBarController.selectedIndex{
        case 0: showRightMenuButtom(false);
        case 1: showRightMenuButtom(false);
        case 2: showRightMenuButtom(false);
        case 3: showRightMenuButtom(true);
        default: showRightMenuButtom(false);
        }
    }
    
    func showRightMenuButtom(_ show:Bool){
        if(show){
            self.addButton.isEnabled = true;
            self.addButton.tintColor = UIColor.white;
        }else{
            self.addButton.isEnabled = false
            self.addButton.tintColor = UIColor.clear
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "newAppointmentDetail") {
            let nextAppointmentVC = segue.destination as! AppointmentViewController;
            nextAppointmentVC.customerId = self.customerId;
            nextAppointmentVC.selectedAppointmentId = "";
            nextAppointmentVC.editMode = false;
        }
    }
    
}
