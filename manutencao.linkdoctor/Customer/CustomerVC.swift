//
//  CustomerVC
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 09/05/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import UIKit
import RealmSwift
import Network
import RxSwift
import RxCocoa

class CustomerVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    let monitor = NWPathMonitor();
    var queue: DispatchQueue!;
    var isInternetOn: Bool = false;
    var customerMode: Bool = true;
    let syncProcess = SyncProcessHelper();
    let disposeBag = DisposeBag();
    private var customerModel: Customer?;
    var situacaoSelecionada = "";
    var descricaoSituacaoSelecionada = "Selecione a situação";
    var equipamentoSelecionado = "";
    var tecnicoSelecionado = "";
    var customerId: String = "";
    var selectedCustomerId: String = "";
    var editMode: Bool = false;
    var toolbar:UIToolbar!;
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    @IBOutlet weak var deleteButton: UIBarButtonItem!
    //    let previsaoChegadaDP = UIDatePicker();
    let situacaoPicker = UIPickerView();
    let situacaoPickerData:[(id: String, description: String)] = [
        ("", "Selecione o Tipo da pessoa"),
        ("J", "Jurídica"),
        ("F", "Física")
    ];
    
    @IBOutlet weak var TipoPessoa: UITextField!
    @IBOutlet weak var CnpjCpf: UITextField!
    @IBOutlet weak var RazaoSocial: UITextField!
    @IBOutlet weak var Nome: UITextField!
    //    @IBOutlet weak var Clientedesde: UITextField!
    @IBOutlet weak var telefoneFixo: UITextField!
    @IBOutlet weak var celular: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var cep: UITextField!
    @IBOutlet weak var endereco: UITextField!
    @IBOutlet weak var enderecoNumero: UITextField!
    @IBOutlet weak var enderecoComplemento: UITextField!
    @IBOutlet weak var enderecoBairro: UITextField!
    @IBOutlet weak var enderecoCidade: UITextField!
    @IBOutlet weak var enderecoUf: UITextField!
    
    @IBOutlet weak var customerConstraint: NSLayoutConstraint!
    
    var activeField: UITextField?
    var activeTextView: UITextView?
    var lastOffset: CGPoint!
    var keyboardHeight: CGFloat!
    
    let genericAlert = UIAlertController(title: nil, message: "", preferredStyle: .alert);
    let saveAlert = UIAlertController(title: nil, message: "Salvando...", preferredStyle: .alert);
    let saveLoading = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge);
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = editMode ? "Editar cliente" : "Novo cliente";
        // Add touch gesture for contentView
        self.contentView.addGestureRecognizer(
            UITapGestureRecognizer(target: self, action: #selector(returnTextView(gesture:)))
        )
        //self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 1400);
        setupNotifications();
        setupCustomerMode();
        loadData();
        setupFields();
        setValueFields();
        setupKeyBoardToolBar();
        if(!editMode){
            deleteButton.isEnabled = false;
        }
        
    }
    
    // MARK: setups
    
    func setupNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(aNotification:)), name: UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil);
        
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                self.isInternetOn = true;
            } else {
                self.isInternetOn = false;
            }
        }
        let queue = DispatchQueue(label: "Monitor");
        monitor.start(queue: queue);
    }
    
    func setValueFields(){
        
        
        
        if(editMode){
            
            if(self.customerModel?.tipoPessoa == "J"){
                self.TipoPessoa.text = "Jurídica"
            }else if(self.customerModel?.tipoPessoa == "F"){
                self.TipoPessoa.text = "Física"
            }else{
                self.TipoPessoa.text = "Selecione o tipo da pessoa";
            }
            
            self.CnpjCpf.text = self.customerModel?.tipoPessoa == "F" ? self.customerModel?.cpf : self.customerModel?.cnpj ?? "J";
            self.RazaoSocial.text = self.customerModel?.razaoSocial;
            self.Nome.text = self.customerModel?.nome;
            //            self.Clientedesde.text = "";
            //            if(self.customerModel?.clienteEm != nil){
            //                self.Clientedesde.text = UtilEssencial.toScreenOnBrFormatWithOutHour(
            //                    date: self.customerModel?.clienteEm ?? Date()
            //                );
            //            }
            self.telefoneFixo.text = self.customerModel?.telefonePrincipal;
            self.celular.text = self.customerModel?.celularPrincipal;
            self.email.text = self.customerModel?.email;
            self.cep.text = self.customerModel?.cep;
            self.endereco.text = self.customerModel?.logradouro;
            self.enderecoNumero.text = self.customerModel?.numero;
            self.enderecoComplemento.text = self.customerModel?.complemento;
            self.enderecoBairro.text = self.customerModel?.bairro;
            self.enderecoCidade.text = self.customerModel?.cidade;
            self.enderecoUf.text = self.customerModel?.uf;
            
            
            situacaoSelecionada = self.customerModel?.tipoPessoa ?? situacaoPickerData[0].id;
            //            self.setSituacaoText(situacaoPickerData[0].description);
            for situacaoIndex in 0..<situacaoPickerData.count {
                if( situacaoSelecionada == situacaoPickerData[situacaoIndex].id ){
                    //                    //situacaoTextField.text = situacaoPickerData[situacaoIndex].description;
                    //                    self.setSituacaoText(situacaoPickerData[situacaoIndex].description);
                    descricaoSituacaoSelecionada = situacaoPickerData[situacaoIndex].description;
                    situacaoPicker.selectRow(situacaoIndex, inComponent: 0, animated: false);
                    break
                }
            }
            
            
        }else{
            
            //            self.setSituacaoText(situacaoPickerData[0].description);
            //            situacaoPicker.selectRow(0, inComponent: 0, animated: false);
            //            situacaoSelecionada = situacaoPickerData[0].id;
            //            descricaoSituacaoSelecionada = situacaoPickerData[0].description;
            //            tecnicoTextField.text = "Selecione o técnico";
            //            tecnicoSelecionado = "";
            //            technicianSelected = nil;
            //            equipamentoTextView.text = "Selecione o equipamento";
            //            equipamentoSelecionado = "";
            
        }
        
        
        
    }
    
    func setupFields(){
        
        
        TipoPessoa.delegate = self;
        CnpjCpf.delegate = self;
        RazaoSocial.delegate = self;
        Nome.delegate = self;
        //        Clientedesde.delegate = self;
        telefoneFixo.delegate = self;
        celular.delegate = self;
        email.delegate = self;
        cep.delegate = self;
        endereco.delegate = self;
        enderecoNumero.delegate = self;
        enderecoComplemento.delegate = self;
        enderecoBairro.delegate = self;
        enderecoCidade.delegate = self;
        enderecoUf.delegate = self;
        
        situacaoPicker.tag = 0;
        situacaoPicker.delegate = self;
        TipoPessoa.inputView = situacaoPicker;
        TipoPessoa.text = "Selecione o tipo da pessoa";
        TipoPessoa.dropDownStyle();
        //
        //        setupArriveDatePicker();
        
        genericAlert.view.backgroundColor = UIColor.black;
        genericAlert.view.alpha = 0.6;
        genericAlert.view.layer.cornerRadius = 15;
        genericAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        saveAlert.view.backgroundColor = UIColor.black;
        saveAlert.view.alpha = 0.6;
        saveAlert.view.layer.cornerRadius = 15;
        saveAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        setupLoadingSpin();
        
    }
    
    func setupKeyBoardToolBar(){
        toolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30));
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(
            title: "OK", style: .done, target: self,
            action: #selector(doneButtonAction)
            //action: Selector(("doneButtonAction"))
        );
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        self.TipoPessoa.inputAccessoryView = toolbar;
        self.CnpjCpf.inputAccessoryView = toolbar;
        self.RazaoSocial.inputAccessoryView = toolbar;
        self.Nome.inputAccessoryView = toolbar;
        //        self.Clientedesde.inputAccessoryView = toolbar;
        self.telefoneFixo.inputAccessoryView = toolbar;
        self.celular.inputAccessoryView = toolbar;
        self.email.inputAccessoryView = toolbar;
        self.cep.inputAccessoryView = toolbar;
        self.endereco.inputAccessoryView = toolbar;
        self.enderecoNumero.inputAccessoryView = toolbar;
        self.enderecoComplemento.inputAccessoryView = toolbar;
        self.enderecoBairro.inputAccessoryView = toolbar;
        self.enderecoCidade.inputAccessoryView = toolbar;
        self.enderecoUf.inputAccessoryView = toolbar;
        
    }
    
    
    func loadFromRealm(){
        if(selectedCustomerId.count > 0){
            if(editMode){
                self.customerModel = Customer.loadFromId(id: selectedCustomerId);
            }
        }
        
        
    }
    
    func loadData(){
        loadFromRealm();
    }
    
    func hideKeyboard(){
        _ = self.TipoPessoa.resignFirstResponder;
        _ = self.CnpjCpf.resignFirstResponder;
        _ = self.RazaoSocial.resignFirstResponder;
        _ = self.Nome.resignFirstResponder;
        //        self.Clientedesde.resignFirstResponder;
        _ = self.telefoneFixo.resignFirstResponder;
        _ = self.celular.resignFirstResponder;
        _ = self.email.resignFirstResponder;
        _ = self.cep.resignFirstResponder;
        _ = self.endereco.resignFirstResponder;
        _ = self.enderecoNumero.resignFirstResponder;
        _ = self.enderecoComplemento.resignFirstResponder;
        _ = self.enderecoBairro.resignFirstResponder;
        _ = self.enderecoCidade.resignFirstResponder;
        _ = self.enderecoUf.resignFirstResponder;
    }
    
    func enableInteraction(_ enable: Bool){
        self.view.isUserInteractionEnabled = enable;
        self.saveButton.isEnabled = enable;
        self.navigationController?.navigationBar.isUserInteractionEnabled = enable;
    }
    
    
    func saveMode(_ on: Bool){
        self.enableInteraction(!on);
        if(on){
            self.saveButton.isEnabled = false;
            saveLoading.startAnimating();
        }else{
            self.saveButton.isEnabled = true;
            saveLoading.stopAnimating();
        }
    }
    
    func setupCustomerMode(){
        self.customerMode = (Session.getUsuarioLogadoTipo() == Constants.USUARIO_TIPO_CLIENTE_FINAL);
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "selectProductStock") {
            let nextVC = segue.destination as! ProductStockToSelectTbVC;
            nextVC.customerId = self.customerId;
        }
        if (segue.identifier == "selectProductByQrCode") {
            let nextVC = segue.destination as! QRCodeScannerVC;
            nextVC.vcFrom = String(describing: type(of: self));
        }
    }
    
    
    func createAppointmentToSave(_ uuid: String) -> Customer {
        
        var tempObject: Customer!;
        tempObject = Customer();
        tempObject.id = uuid;
        tempObject.tipoPessoa = situacaoSelecionada;
        tempObject.cnpj = situacaoSelecionada == "J" ? self.CnpjCpf.text! : "";
        tempObject.cpf = situacaoSelecionada == "F" ? self.CnpjCpf.text! : "";
        tempObject.razaoSocial = self.RazaoSocial.text!;
        tempObject.nome = self.Nome.text!;
        tempObject.telefonePrincipal = self.telefoneFixo.text!;
        tempObject.celularPrincipal = self.telefoneFixo.text!;
        tempObject.email = self.email.text!;
        tempObject.cep = self.cep.text!;
        tempObject.logradouro = self.endereco.text!;
        tempObject.numero = self.enderecoNumero.text!;
        tempObject.complemento = self.enderecoComplemento.text!;
        tempObject.bairro = self.enderecoBairro.text!;
        
        tempObject.codigoQuemAtualizou = Session.getUsuarioLogadoId();
        tempObject.atualizadoPor = Session.getUsuarioLogadoDescricao();
        tempObject.atualizadoEm = Date();
        tempObject.precisaSincronizar = true;
        tempObject.atualizadoEmTimeStamp = Double(Date().toEpochInt64());
        
//        tempObject.nome = cidade;
//        tempObject.nome = uf;
        
        
        
        return tempObject;
    }
    
    
    func updateOnRealm() {
        
        if (internetOn()) {
            
            /* M = medico , E = enfermagem, T = tecnico,  @ = ultra,master,essencial */
            if (Session.getUsuarioLogadoTipo() == Constants.USUARIO_TIPO_CLIENTE_MEDICO
                || Session.getUsuarioLogadoTipo() == Constants.USUARIO_TIPO_MASTER ) {
                
                updateOnRealmIfMedic();
                
            } else if (Session.getUsuarioLogadoTipo() == Constants.USUARIO_TIPO_CLIENTE_FINAL) {
                
                //                if (customerModel?.codSituacao == "3" || appointmentModel?.codSituacao == "4") {
                //                    msgCantEditObject(false);
                //                } else {
                updateOnRealmIfCustomer();
                //                }
            } else {
                self.showAlert(vc: self, "Tipo do usuário não definido, faça login novamente.", "OK");
            }
        }
    }
    
    func insertOnRealm() {
        
        if (internetOn()) {
            
            self.saveMode(true);
            let tempDadosDaTela: Customer = createAppointmentToSave(UUID().uuidString);
            
            tempDadosDaTela.criado_local = true;
        
            Customer.add(objectToSave: tempDadosDaTela);
            
            
            self.syncServer();
            //            self.showToastAndReturnScreen(message: "Chamado salvo", seconds: 2, closeView: true);
            
        }
        
    }
    
    
    func updateOnRealmIfCustomer() {
        
        if (internetOn()) {
            
            self.saveMode(true);
            let tempDadosDaTela: Customer = createAppointmentToSave("");
            Customer.addOrUpdate(objectToSave: tempDadosDaTela);
            Customer.modifyBeforeUpdate(
                id: customerModel!.id,
                usuarioCod: Session.getUsuarioLogadoId(), usuarioDescricao: Session.getUsuarioLogadoDescricao(), dataInformada: Date()
            );
            
            self.syncServer();
            
        }
        
    }
    
    func updateOnRealmIfMedic() {
        
        if (internetOn()) {
            
            self.saveMode(true);
            let tempDadosDaTela: Customer = createAppointmentToSave(customerModel!.id );
            Customer.addOrUpdate(objectToSave: tempDadosDaTela);
            self.syncServer();
            
        }
        
    }
    
    @objc func syncServerDeleteCustomer(){
        syncProcess.getDefaultSyncRx(token: Session.getToken(), serviceKey: Session.getServiceKey(),
                                     appCustomerId:  Session.getUsuarioLogadoCodCliente(), tipoUsuario: Session.getUsuarioLogadoTipo(),
                                     beginTimeStamp: Session.getKeySincronizacaoTimestampUltimaString()
            )
            .observeOn(MainScheduler.instance)
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(
                onNext: { el in
            },
                onError: { err in
                    
                        self.showToastAndReturnScreen(message: "Erro ao tentar excluir o Cliente", seconds: 2, closeView: false);
                    
            },
                onCompleted: { () in
                    self.setLastSync();
                        self.showToastAndReturnScreenDeleted(message: "Cliente excluído", seconds: 2, closeView: true);
            },
                onDisposed: { () in
            }
            ).disposed(by: disposeBag);
        
    }
    @objc func syncServer(){
        syncProcess.getDefaultSyncRx(token: Session.getToken(), serviceKey: Session.getServiceKey(),
                                     appCustomerId:  Session.getUsuarioLogadoCodCliente(), tipoUsuario: Session.getUsuarioLogadoTipo(),
                                     beginTimeStamp: Session.getKeySincronizacaoTimestampUltimaString()
            )
            .observeOn(MainScheduler.instance)
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(
                onNext: { el in
            },
                onError: { err in
                    if(self.editMode){
                        self.showToastAndReturnScreen(message: "Erro ao tentar atualizar o Cliente", seconds: 2, closeView: false);
                    }else{
                        self.showToastAndReturnScreen(message: "Erro ao tentar salvar o Cliente", seconds: 2, closeView: false);
                    }
            },
                onCompleted: { () in
                    self.setLastSync();
                    if(self.editMode){
                        self.showToastAndReturnScreen(
                            message: "Cliente atualizado", seconds: 2, closeView: false
                        );
                    }else{
                        self.showToastAndReturnScreen(message: "Cliente salvo", seconds: 2, closeView: true);
                    }
            },
                onDisposed: { () in
            }
            ).disposed(by: disposeBag);
        
    }
    
    func setLastSync(){
        UserDefaults.standard.set((0), forKey: Session.KEY_SINCRONIZACAO_TIMESTAMP_SERVER);
        let tempSyncTsServer: Double = Session.getSincronizacaoTsUltimaServer();
        let dateToSave: Date = Date();
        if (tempSyncTsServer == 0 || tempSyncTsServer < Session.getKeySincronizacaoTimestampUltima()) {
            UserDefaults.standard.set((dateToSave.getDeviceDateDisplayBrWithHour()), forKey: Session.KEY_SINCRONIZACAO_DATA_ULTIMA);
            UserDefaults.standard.set((dateToSave.toEpochInt64()), forKey: Session.KEY_SINCRONIZACAO_TIMESTAMP);
        } else {
            UserDefaults.standard.set((dateToSave.getDeviceDateDisplayBrWithHour()), forKey: Session.KEY_SINCRONIZACAO_DATA_ULTIMA);
            UserDefaults.standard.set((dateToSave.toEpochInt64()), forKey: Session.KEY_SINCRONIZACAO_TIMESTAMP);
        }
    }
    
    
    func createErroMsg(_ errors: String, _ newError: String) -> String {
        if (errors.count > 0){
            return errors + "\n" + newError;
        }else{
            return errors + " " + newError;
        }
    }
    
    
    func internetOn() -> Bool{
        if(!isInternetOn){
            showAlert(vc: self, "Sem internet não é possível realizar a operação.", nil);
        }
        return isInternetOn;
    }
    func isFieldsValidToSave() -> Bool{
        var hasErros = false;
        var errors = "";
        
        if(situacaoSelecionada == "" || situacaoSelecionada == "0"){
            hasErros = true;
            errors = createErroMsg(errors, "Selecione o tipo da pessoa");
        }
        
        if(self.CnpjCpf.text == ""){
            errors = createErroMsg(errors, "Informe o CNPJ ou CPF.");
            hasErros = true;
        }
        
        if(self.Nome.text == ""){
            errors = createErroMsg(errors, "Informe o nome.");
            hasErros = true;
        }
        
        if(self.RazaoSocial.text == ""){
            errors = createErroMsg(errors, "Informe a Razão social.");
            hasErros = true;
        }
        
        
//        if(editMode){
//            if(selectedCustomerId == ""){
//                errors = createErroMsg(errors, "Chamado sem código.");
//                hasErros = true;
//            }
//        }
//
        if(hasErros){
            self.showAlert(vc: self, errors, "OK");
        }
        
        return !hasErros;
    }
    
    @IBAction func saveAction(_ sender: Any) {
        if(isFieldsValidToSave()){
            if(editMode){
                updateOnRealm();
            }else{
                insertOnRealm();
            }
        }
    }
    
    @IBAction func deleteAction(_ sender: Any) {
        if(editMode){
            let alert = UIAlertController(title: "Deseja excluir este cliente?", message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Sim", style: .default, handler: { action in
                if (self.internetOn()) {
                    self.saveMode(true);
                    let tempDadosDaTela: Customer = self.createAppointmentToSave(self.customerModel!.id );
                    tempDadosDaTela.ativo = false;
                    tempDadosDaTela.deletado = true;
                    Customer.addOrUpdate(objectToSave: tempDadosDaTela);
                    self.syncServerDeleteCustomer();
                }
            }
            ))
            alert.addAction(UIAlertAction(title: "Não", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    // MARK: OBJC
    @objc func doneButtonAction() {
        self.view.endEditing(true)
    }
    
    @objc func returnTextView(gesture: UIGestureRecognizer) {
        guard (activeField != nil || activeTextView != nil) else {
            return
        }
        activeField?.resignFirstResponder()
        activeField = nil;
        activeTextView?.resignFirstResponder()
        activeTextView = nil;
    }
    
    // MARK: UIPicker
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 0 {
            return situacaoPickerData.count;
        }else{
            return 0;
        }
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0 {
            return situacaoPickerData[row].description;
        }else{
            return "";
        }
    }
    
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if (pickerView.tag == 0) {
            self.TipoPessoa.text = (situacaoPickerData[row].description);
            situacaoSelecionada = situacaoPickerData[row].id;
            descricaoSituacaoSelecionada = situacaoPickerData[row].description;
        }else if (pickerView.tag == 2) {
            //            tecnicoTextField.text = tecnicoPickerData[row].nome;
            //            tecnicoSelecionado = tecnicoPickerData[row].id;
            //            technicianSelected = tecnicoPickerData[row];
        }
    }
    
    func showAlert( vc: UIViewController, _ message: String,_ okBtnTitle: String?){
        vc.present(genericAlert, animated: true, completion: nil);
        genericAlert.setValue(message, forKey: "message");
        if(okBtnTitle != nil){
            genericAlert.actions[0].setValue((okBtnTitle ?? "OK"), forKey: "title");
        }
    }
    
    func showToastAndReturnScreen(message: String, seconds: Double, closeView: Bool) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert);
        alert.view.backgroundColor = UIColor.black;
        alert.view.alpha = 0.6;
        alert.view.layer.cornerRadius = 15;
        self.present(alert, animated: true);
        self.saveMode(false);
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds, execute: {
            [weak self] in
            guard let self = self else { return }
            alert.dismiss(animated: true);
            self.saveLoading.stopAnimating();
            if(closeView){
                self.closeView();
            }
        });
    }
    func showToastAndReturnScreenDeleted(message: String, seconds: Double, closeView: Bool) {
          let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert);
          alert.view.backgroundColor = UIColor.black;
          alert.view.alpha = 0.6;
          alert.view.layer.cornerRadius = 15;
          self.present(alert, animated: true);
          self.saveMode(false);
          DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds, execute: {
              [weak self] in
              guard let self = self else { return }
              alert.dismiss(animated: true);
              self.saveLoading.stopAnimating();
              if(closeView){
                self.navigationController?.popToRootViewController(animated: true);
                self.dismiss(animated: true, completion: nil);
              }
          });
      }
    
    
    func msgCantEditObject(_ oldObject:Bool){
        self.showAlert(vc: self, "Não é possível editar um chamado finalizado ou cancelado", "OK");
    }
    
    func setupLoadingSpin(){
        saveLoading.center = view.center;
        saveLoading.color = UIColor.white;
        saveLoading.hidesWhenStopped = true;
        saveLoading.layer.cornerRadius = 05;
        saveLoading.isOpaque = false;
        saveLoading.backgroundColor = UIColor.black;
        saveLoading.alpha = 0.3;
        view.addSubview(saveLoading);
    }
    
    func closeView(){
        self.navigationController?.popViewController(animated: true);
        self.dismiss(animated: true, completion: nil);
    }
    
    func showToast(message: String, seconds: Double) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert);
        alert.view.backgroundColor = UIColor.black;
        alert.view.alpha = 0.6;
        alert.view.layer.cornerRadius = 15;
        self.present(alert, animated: true);
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds){
            alert.dismiss(animated: true);
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil);
    }
    
    
}



// MARK: UITextFieldDelegate
extension CustomerVC: UITextFieldDelegate, UITextViewDelegate {
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeField = textField;
        lastOffset = self.scrollView.contentOffset;
        return true;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        activeField?.resignFirstResponder();
        activeField = nil;
        return true;
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        activeTextView = textView;
        lastOffset = self.scrollView.contentOffset;
        return true;
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        activeTextView?.resignFirstResponder();
        activeTextView = nil;
        return true;
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return (textField != self.TipoPessoa);
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let existingLines = textView.text.components(separatedBy: CharacterSet.newlines)
        let newLines = text.components(separatedBy: CharacterSet.newlines)
        let linesAfterChange = existingLines.count + newLines.count - 1
        if(text == "\n") {
            return linesAfterChange <= textView.textContainer.maximumNumberOfLines
        }
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return numberOfChars <= 100;
    }
}


// MARK: Keyboard Handling
extension CustomerVC {
    
    @objc func keyboardWillShow(aNotification: NSNotification) {
        let info = aNotification.userInfo!;
        let kbSize: CGSize = (
            (info["UIKeyboardFrameEndUserInfoKey"] as? CGRect)?.size
            )!;
        let contentInsets: UIEdgeInsets = UIEdgeInsets(
            top: 0.0, left: 0.0, bottom: kbSize.height, right: 0.0
        );
        var frame: CGRect!;
        if(activeTextView != nil){
            frame = activeTextView!.frame;
        }
        if(activeField != nil){
            frame = activeField!.frame;
        }
        scrollView.contentInset = contentInsets;
        scrollView.scrollIndicatorInsets = contentInsets;
        var aRect: CGRect = self.view.frame;
        aRect.size.height -= kbSize.height;
        if (frame != nil){
            if !aRect.contains(frame.origin) {
                self.scrollView.scrollRectToVisible(frame, animated: true);
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.3) {
            self.customerConstraint.constant -= (self.keyboardHeight ?? 0);
            if(self.lastOffset != nil){
                self.scrollView.contentOffset = self.lastOffset;
            }
        }
        keyboardHeight = nil;
    }
}
