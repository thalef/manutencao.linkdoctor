//
//  ProductStock.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 06/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class ProductStock: Object {
    
    @objc dynamic var id: String = "";
    @objc dynamic var criado_local: Bool = false;
    @objc dynamic var ativo: Bool = false;
    @objc dynamic var codLocalAtual: String = "";
    @objc dynamic var codMarca: String = "";
    @objc dynamic var codModelo: String = "";
    @objc dynamic var precisaSincronizar: Bool = false;
    @objc dynamic var codProduto: String = "";
    @objc dynamic var descricaoEquipamento: String = "";
    @objc dynamic var descricaoLocalAtual: String = "";
    @objc dynamic var descricaoMarca: String = "";
    @objc dynamic var descricaoModelo: String = "";
    @objc dynamic var eComodato: Bool = false;
    @objc dynamic var flagLocalAtual: String = "";
    @objc dynamic var handlerDestination: String?;
    @objc dynamic var handlerDestinationCustomerFolder: String?;
    @objc dynamic var handlerDestinationRoot: String?;
    @objc dynamic var handlerFilename: String?;
    @objc dynamic var handlerMimetype: String?;
    @objc dynamic var handlerPath: String?;
    @objc dynamic var idRegistroAnterior: String = "";
    @objc dynamic var qrCodeGerado: Bool = false;
    @objc dynamic var serial: String = "";
    @objc dynamic var tipoLocalAtualDescricao: String = "";
    @objc dynamic var atualizadoEm: Date!;
    @objc dynamic var atualizadoEmTimeStamp: Double = 0.0;
    @objc dynamic var criadoEm: Date!;
    @objc dynamic var criadoEmTimeStamp: Double = 0.0;
    @objc dynamic var servidorAtualizadoEm: Date!;
    @objc dynamic var servidorAtualizadoEmTimeStamp: Double = 0.0;
    @objc dynamic var codigoQuemCriou: String = "";
    @objc dynamic var deletado: Bool = false;
    @objc dynamic var estoqueAvulso: Bool = false;
    @objc dynamic var qrCodeVinculado: Bool = false;
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(   id: String,
                        criado_local: Bool,
                        ativo: Bool,
                        codLocalAtual: String,
                        codMarca: String,
                        codModelo: String,
                        precisaSincronizar: Bool?,
                        codProduto: String,
                        descricaoEquipamento: String,
                        descricaoLocalAtual: String,
                        descricaoMarca: String,
                        descricaoModelo: String,
                        eComodato: Bool,
                        flagLocalAtual: String,
                        handlerDestination: String?,
                        handlerDestinationCustomerFolder: String?,
                        handlerDestinationRoot: String?,
                        handlerFilename: String?,
                        handlerMimetype: String?,
                        handlerPath: String?,
                        idRegistroAnterior: String,
                        qrCodeGerado: Bool,
                        serial: String,
                        tipoLocalAtualDescricao: String,
                        atualizadoEm: Date!,
                        atualizadoEmTimeStamp: Double,
                        criadoEm: Date!,
                        criadoEmTimeStamp: Double,
                        servidorAtualizadoEm: Date!,
                        servidorAtualizadoEmTimeStamp: Double,
                        codigoQuemCriou: String,
                        deletado: Bool,
                        estoqueAvulso: Bool,
                        qrCodeVinculado: Bool
        ) {
        
        self.init();
        self.id = id;
        self.criado_local = criado_local;
        self.ativo = ativo;
        self.codLocalAtual = codLocalAtual;
        self.codMarca = codMarca;
        self.codModelo = codModelo;
        self.precisaSincronizar = false;
        self.codProduto = codProduto;
        self.descricaoEquipamento = descricaoEquipamento;
        self.descricaoLocalAtual = descricaoLocalAtual;
        self.descricaoMarca = descricaoMarca;
        self.descricaoModelo = descricaoModelo;
        self.eComodato = eComodato;
        self.flagLocalAtual = flagLocalAtual;
        self.handlerDestination = handlerDestination ?? "";
        self.handlerDestinationCustomerFolder = handlerDestinationCustomerFolder ?? "";
        self.handlerDestinationRoot = handlerDestinationRoot ?? "";
        self.handlerFilename = handlerFilename ?? "";
        self.handlerMimetype = handlerMimetype ?? "";
        self.handlerPath = handlerPath ?? "";
        self.idRegistroAnterior = idRegistroAnterior;
        self.qrCodeGerado = qrCodeGerado;
        self.serial = serial;
        self.tipoLocalAtualDescricao = tipoLocalAtualDescricao;
        self.atualizadoEm = atualizadoEm;
        self.atualizadoEmTimeStamp  = atualizadoEmTimeStamp;
        self.criadoEm = criadoEm;
        self.criadoEmTimeStamp = criadoEmTimeStamp;
        self.servidorAtualizadoEm = servidorAtualizadoEm;
        self.servidorAtualizadoEmTimeStamp = servidorAtualizadoEmTimeStamp;
        self.codigoQuemCriou = codigoQuemCriou;
        self.deletado = deletado;
        self.estoqueAvulso = estoqueAvulso;
        self.qrCodeVinculado = qrCodeVinculado;
    }
    
    
    func getDescricaoTela() -> String {
        return self.descricaoEquipamento + " ( " + getDescricaoMarcaModeloSerial() + " )";
    }
    func getDescricaoEquipamento() -> String {
        return self.descricaoEquipamento;
    }
    
    func getDescricaoMarcaModelo() -> String {
        return self.descricaoMarca + " - " + self.descricaoModelo;
    }
    
    func getDescricaoMarcaModeloSerial() -> String {
        return self.descricaoMarca + " - " + self.descricaoModelo + ", Serial: \(self.serial.count > 0 ? self.serial : "Não informado")";
    }
    
    func getDescricaoSerial() -> String {
        return "Serial: " + self.serial;
    }
    
    func getDescricaoComodato() -> String {
        return self.eComodato ? "É comodato" : "Não é comodato";
    }
    
    
    
}
// MARK: - CRUD methods

extension ProductStock {
    
    
    static func convertResponseToObject(_ productStock: ProductStockResponse) -> ProductStock {
        return ProductStock(
            id: productStock.id,
            criado_local: productStock.criado_local,
            ativo: productStock.ativo,
            codLocalAtual: productStock.codLocalAtual,
            codMarca: productStock.codMarca,
            codModelo: productStock.codModelo,
            precisaSincronizar: false,
            codProduto: productStock.codProduto,
            descricaoEquipamento: productStock.descricaoEquipamento,
            descricaoLocalAtual: productStock.descricaoLocalAtual,
            descricaoMarca: productStock.descricaoMarca,
            descricaoModelo: productStock.descricaoModelo,
            eComodato: productStock.eComodato,
            flagLocalAtual: productStock.flagLocalAtual,
            handlerDestination: productStock.handlerDestination ?? "",
            handlerDestinationCustomerFolder: productStock.handlerDestinationCustomerFolder ?? "",
            handlerDestinationRoot: productStock.handlerDestinationRoot ?? "",
            handlerFilename: productStock.handlerFilename ?? "",
            handlerMimetype: productStock.handlerMimetype ?? "",
            handlerPath: productStock.handlerPath ?? "",
            idRegistroAnterior: productStock.idRegistroAnterior,
            qrCodeGerado: productStock.qrCodeGerado,
            serial: productStock.serial,
            tipoLocalAtualDescricao: productStock.tipoLocalAtualDescricao,
            atualizadoEm: productStock.atualizadoEm,
            atualizadoEmTimeStamp: productStock.atualizadoEmTimeStamp,
            criadoEm: productStock.criadoEm,
            criadoEmTimeStamp: productStock.criadoEmTimeStamp,
            servidorAtualizadoEm: productStock.servidorAtualizadoEm,
            servidorAtualizadoEmTimeStamp: productStock.servidorAtualizadoEmTimeStamp,
            codigoQuemCriou: productStock.codigoQuemCriou,
            deletado: productStock.deletado,
            estoqueAvulso: productStock.estoqueAvulso,
            qrCodeVinculado: productStock.qrCodeVinculado
        );
    }
    
    static func converListResponseToListObject(_ listToConvert: [ProductStockResponse]) -> [ProductStock] {
        var list: [ProductStock] = [];
        for obj in listToConvert {
            list.append(convertResponseToObject(obj));
        }
        return list;
    }
    
    static func loadAllFromRealm(in realm: Realm = try! Realm()) -> Results<ProductStock> {
        return realm.objects(ProductStock.self).sorted(byKeyPath: "descricaoEquipamento")
    }
    
    static func loadAllFromRealmActive(in realm: Realm = try! Realm()) -> Results<ProductStock> {
        return realm.objects(ProductStock.self)
            .filter("\(Constants.CAMPO_ATIVO) = true AND \(Constants.CAMPO_DELETADO) = false")
            .sorted(byKeyPath: "descricaoEquipamento")
    }
    
    static func loadFromId(in realm: Realm = try! Realm(), id: String) -> ProductStock? {
        return realm.objects(ProductStock.self).filter("\(Constants.CAMPO_ID) = '\(id)'").first;
    }
    
    static func loadAllFromCustomerRealm(in realm: Realm = try! Realm(), customerId: String) -> Results<ProductStock> {
        return realm.objects(ProductStock.self)
            .filter("\(Constants.CAMPO_COD_LOCAL_ATUAL) = '\(customerId)' AND \(Constants.CAMPO_ATIVO) = true AND \(Constants.CAMPO_DELETADO) = false")
            .sorted(byKeyPath: Constants.CAMPO_CRIADO_EM_TIMESTAMP, ascending: false);
    }
    
    @discardableResult
    static func add(objectToSave: ProductStock, in realm: Realm = try! Realm())
        -> ProductStock {
            let item = objectToSave;
            try! realm.write {
                realm.add(item)
            }
            return item
    }
    
    @discardableResult
    static func addOrUpdate(objectToSave: ProductStock, in realm: Realm = try! Realm())
        -> ProductStock {
            let item = objectToSave;
            try! realm.write {
                realm.add(item, update: .all);
            }
            return item
    }
    
    @discardableResult
    static func addAll(listToSave: [ProductStock], in realm: Realm = try! Realm())
        -> [ProductStock] {
            realm.beginWrite();
            for user in listToSave
            {
                realm.add(user)
            }
            try! realm.commitWrite()
            return listToSave
    }
    @discardableResult
    static func addOrUpdateAll(listToSave: [ProductStock], in realm: Realm = try! Realm())
        -> [ProductStock] {
            //try! realm.beginWrite();
            realm.beginWrite();
            realm.add(listToSave, update: .all)
            try! realm.commitWrite()
            return listToSave
    }
    
    //    func toggleCompleted() {
    //        guard let realm = realm else { return }
    //        try! realm.write {
    //            isCompleted = !isCompleted
    //        }
    //    }
    //
    //    func delete() {
    //        guard let realm = realm else { return }
    //        try! realm.write {
    //            realm.delete(self)
    //        }
    //    }
}
