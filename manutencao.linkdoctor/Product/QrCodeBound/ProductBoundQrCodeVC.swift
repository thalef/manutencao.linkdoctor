//
//  ProductBoundQrCodeVC.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 06/06/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import Network
import RxSwift
import RxCocoa

class ProductBoundQrCodeVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    let monitor = NWPathMonitor();
    var queue: DispatchQueue!;
    var isInternetOn: Bool = false;
    //var customerMode: Bool = true;
    private var customerSelected: Customer?
    private var productSelected: ProductType?
    private var technicianSelected: Tecnico?
    var equipamentoSelecionado = "";
    var tecnicoSelecionado = "";
    var customerId: String = "";
    var selectedQRCodeId: String = "";
    var editMode: Bool = false;
    var toolbar:UIToolbar!;
    @IBOutlet weak var tecnicoLabel: UILabel!
    @IBOutlet weak var clienteTextField: UITextField!
    @IBOutlet weak var tecnicoTextField: UITextField!
    @IBOutlet weak var equipamentoTextView: UITextView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var serialTextField: UITextField!
    @IBOutlet weak var eComodatoSwitch: UISwitch!
    
    @IBOutlet weak var qrCodeBoundConstraintContentHeight: NSLayoutConstraint!
    let syncProcess = SyncProcessHelper();
    let disposeBag = DisposeBag();
    private let loginService = LoginService();
    
    var equipamentoData:[ProductType] = [];
    private var equipamentoList: Results<ProductType>?
    
    let tecnicoPicker = UIPickerView();
    var tecnicoPickerData:[Tecnico] = [];
    private var tecnicoList: Results<Tecnico>?
    
    var activeField: UITextField?
    var activeTextView: UITextView?
    var lastOffset: CGPoint!
    var keyboardHeight: CGFloat!
    
    let genericAlert = UIAlertController(title: nil, message: "", preferredStyle: .alert);
    let saveAlert = UIAlertController(title: nil, message: "Salvando...", preferredStyle: .alert);
    let saveLoading = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge);
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = editMode ? "Vincular QRCode" : "Vincular QRCode";
        self.contentView.addGestureRecognizer(
            UITapGestureRecognizer(target: self, action: #selector(returnTextView(gesture:)))
        )
        setupNotifications();
        loadData();
        setupFields();
        setValueFields();
        setupKeyBoardToolBar();
        
    }
    
    // MARK: setups
    
    func setupNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(aNotification:)), name: UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil);
        
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                self.isInternetOn = true;
            } else {
                self.isInternetOn = false;
            }
        }
        
        let queue = DispatchQueue(label: "Monitor")
        monitor.start(queue: queue)
    }
    
    func internetOn() -> Bool{
        if(!isInternetOn){
            showAlert(vc: self, "Sem internet não é possível realizar a operação.", nil);
        }
        return isInternetOn;
    }
    
    func setValueFields(){
        clienteTextField.text = customerSelected?.nome;
//        tecnicoTextField.text = "Selecione o técnico";
//        tecnicoSelecionado = "";
        //technicianSelected = nil;
        equipamentoTextView.text = "Selecione o tipo do equipamento";
        equipamentoSelecionado = "";
        eComodatoSwitch.isOn = false;
        eComodatoSwitch.setOn(false, animated:false);
    }
    
    
    func setupFields(){
        
        tecnicoLabel.isHidden = true;
        tecnicoTextField.isHidden = true;
        
        serialTextField.delegate = self;
        clienteTextField.delegate = self;
        //tecnicoTextField.delegate = self;
        equipamentoTextView.delegate = self;
        clienteTextField.text = "Cliente não localizado";
        clienteTextField.isUserInteractionEnabled = false;
//        tecnicoPicker.tag = 2;
//        tecnicoPicker.delegate = self;
//        tecnicoTextField.delegate = self;
//        tecnicoTextField.inputView = tecnicoPicker;
//        tecnicoTextField.dropDownStyle();
        equipamentoTextView.delegate = self;
        equipamentoTextView.isScrollEnabled = false
        equipamentoTextView.isEditable = true
        equipamentoTextView.isUserInteractionEnabled = true
        equipamentoTextView.isSelectable = true
        equipamentoTextView.dropDownStyle();
        
        genericAlert.view.backgroundColor = UIColor.black;
        genericAlert.view.alpha = 0.6;
        genericAlert.view.layer.cornerRadius = 15;
        genericAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        saveAlert.view.backgroundColor = UIColor.black;
        saveAlert.view.alpha = 0.6;
        saveAlert.view.layer.cornerRadius = 15;
        saveAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        setupLoadingSpin();
    }
    
    func setupKeyBoardToolBar(){
        toolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30));
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(
            title: "OK", style: .done, target: self,
            action: #selector(doneButtonAction)
        );
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        self.serialTextField.inputAccessoryView = toolbar;
        self.clienteTextField.inputAccessoryView = toolbar;
//        self.tecnicoTextField.inputAccessoryView = toolbar;
    }
    
    func loadFromRealm(){
        
        customerSelected = Customer.loadFromId(id: customerId);
        
//        tecnicoList = Tecnico.loadAllFromRealmActive();
//        if( tecnicoList != nil && (tecnicoList?.count ?? 0) > 0){
//            tecnicoPickerData = Array(tecnicoList!);
//        }
//
        equipamentoList = ProductType.loadAllFromRealmActive();
        if( equipamentoList != nil && (equipamentoList?.count ?? 0) > 0){
            equipamentoData = Array(equipamentoList!);
        }
        
    }
    
    func loadData(){
        loadFromRealm();
    }
    
    func hideKeyboard(){
        self.serialTextField.resignFirstResponder();
        self.clienteTextField.resignFirstResponder();
//        self.tecnicoTextField.resignFirstResponder();
    }
    
    func enableInteraction(_ enable: Bool){
        self.view.isUserInteractionEnabled = enable;
        self.saveButton.isEnabled = enable;
        self.navigationController?.navigationBar.isUserInteractionEnabled = enable;
    }
    
    
    func saveMode(_ on: Bool){
        self.enableInteraction(!on);
        if(on){
            self.saveLoading.startAnimating();
        }else{
            self.saveLoading.stopAnimating();
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "selectProductTypeSegue") {
            let nextVC = segue.destination as! ProductTypeToSelectTbVC;
            nextVC.vcFrom = String(describing: type(of: self));
        }
        if (segue.identifier == "selectProductByQrCode") {
            let nextVC = segue.destination as! QRCodeScannerVC;
            nextVC.vcFrom = String(describing: type(of: self));
        }
    }
    
    
    func createRequestToSave() -> ProductBoundQrCodeRequest {
        
        let tempStock: ProductBoundStockRequest = ProductBoundStockRequest(
            id: selectedQRCodeId,
            codLocalAtual: customerSelected?.id ??  customerId,
            flagLocalAtual: "C",
            tipoLocalAtualDescricao: "(Cliente)",
            descricaoLocalAtual: customerSelected?.nome ?? "",
            idRegistroAnterior: "",
            codProduto: productSelected?.id ?? "",
            descricaoEquipamento: productSelected?.descricao ?? "",
            codMarca: productSelected?.codMarca ?? "",
            descricaoMarca: productSelected?.descricaoMarca ?? "",
            codModelo: productSelected?.codModelo ?? "",
            descricaoModelo: productSelected?.descricaoModelo ?? "",
            serial: serialTextField.text ?? "",
            eComodato: eComodatoSwitch.isOn,
            qrCodeGerado: true,
            deletado: false,
            estoqueAvulso: true,
            qrCodeVinculado: true
        )
        
        let tempMoviment: ProductBoundMovimentRequest = ProductBoundMovimentRequest(
            codLocalAtual: tempStock.codLocalAtual,
            flagLocalAtual: tempStock.flagLocalAtual,
            tipoLocalAtualDescricao: tempStock.tipoLocalAtualDescricao,
            descricaoLocalAtual: tempStock.descricaoLocalAtual,
            idRegistroAnterior: tempStock.idRegistroAnterior,
            codProduto: tempStock.codProduto,
            descricaoEquipamento: tempStock.descricaoEquipamento,
            codMarca: tempStock.codMarca,
            descricaoMarca: tempStock.descricaoMarca,
            codModelo: tempStock.codModelo,
            descricaoModelo: tempStock.descricaoModelo,
            eComodato: tempStock.eComodato,
            serial: tempStock.serial,
            deletado: tempStock.deletado,
            estoqueAvulso: tempStock.estoqueAvulso,
            qrCodeVinculado: tempStock.qrCodeVinculado,
            codEstoque: tempStock.id,
            tipoAcao: "E",
            idMovimentacaoAnterior: "",
            codLocalAnterior: "",
            flagLocalAnterior: "",
            descricaoLocalAnterior: ""
        );
        
        var result: ProductBoundQrCodeRequest = ProductBoundQrCodeRequest();
        result.id = selectedQRCodeId;
        result.stockToUpdate = tempStock;
        result.movimentToInsert = tempMoviment;
        
        return result;
    }
    
    
    func updateOnRealm() {
        
        if (internetOn()) {
            /* M = medico , E = enfermagem, T = tecnico,  @ = ultra,master,essencial */
            if (Session.getUsuarioLogadoTipo() == Constants.USUARIO_TIPO_CLIENTE_MEDICO
                || Session.getUsuarioLogadoTipo() == Constants.USUARIO_TIPO_MASTER ) {
                updateOnRealmIfMedic();
            } else if (Session.getUsuarioLogadoTipo() == Constants.USUARIO_TIPO_CLIENTE_FINAL) {
                msgCantEditObject(false);
            } else {
                self.showAlert(vc: self, "Tipo do usuário não definido, faça login novamente.", "OK");
            }
        }
    }
    
    func updateOnRealmIfMedic() {
        if (internetOn()) {
            self.saveMode(true);
            let tempDadosDaTela: ProductBoundQrCodeRequest = createRequestToSave();
            self.boundQrCodeOnServer(productBoundQrCodeRequest: tempDadosDaTela)
        }
    }
    
    
    func boundQrCodeOnServer(productBoundQrCodeRequest: ProductBoundQrCodeRequest){
        
        _ = self.loginService.putBoundQrCodeRx(
            request: self.loginService.putBoundQrCodeRequest(qrCodeId: selectedQRCodeId, body: productBoundQrCodeRequest)
            )
            .observeOn(MainScheduler.instance)
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(
                onSuccess: { el in
                    let newProductStock = ProductStock.convertResponseToObject(el);
                    ProductStock.addOrUpdate(objectToSave: newProductStock);
                    self.saveMode(false);
                    self.showToastAndReturnScreen(message: "QRCode vinculado ao equipamento.", seconds: 2, closeView: true);
            },
                onError: { err in
                    if(err == HttpStatusError.objectAlreadyExist){
                        self.showAlert(vc: self, "Erro ao tentar vincular.\nEsse QRCode já foi vinculado a outro equipamento.\nTente outro QRCode.", nil);
                    }else{
                        self.showAlert(vc: self, "Erro ao tentar vincular o QRCode", nil);
                    }
                    self.saveMode(false);
            }
        )
    }
    
    
    func createErroMsg(_ errors: String, _ newError: String) -> String {
        if (errors.count > 0){
            return errors + "\n" + newError;
        }else{
            return errors + " " + newError;
        }
    }
    
    func isFieldsValidToSave() -> Bool{
        var hasErros = false;
        var errors = "";
        
        if(equipamentoSelecionado == ""){
            errors = createErroMsg(errors, "Selecione um equipamento.");
            hasErros = true;
        }
        
        
        if(customerId == ""){
            errors = createErroMsg(errors, "Selecione o cliente.");
            hasErros = true;
        }
        
        if(hasErros){
            self.showAlert(vc: self, errors, "OK");
        }
        
        return !hasErros;
    }
    
    @IBAction func saveAction(_ sender: Any) {
        if(isFieldsValidToSave()){
            updateOnRealm();
        }
    }
    
    @IBAction func getSelectedProductTypeToBound(_ unwindSegue: UIStoryboardSegue) {
        
        let productStockToSelectTbVC = unwindSegue.source as? ProductTypeToSelectTbVC;
        equipamentoSelecionado = productStockToSelectTbVC?.selectedProductId ?? "";
        
        for eqIndex in 0..<equipamentoData.count {
            if( equipamentoSelecionado == equipamentoData[eqIndex].id ){
                equipamentoTextView.text = equipamentoData[eqIndex].getDescricaoTela();
                productSelected = equipamentoData[eqIndex];
                break
            }
        }
    }
    
    // MARK: OBJC
    @objc func doneButtonAction() {
        self.view.endEditing(true)
    }
    
    @objc func returnTextView(gesture: UIGestureRecognizer) {
        guard (activeField != nil || activeTextView != nil) else {
            return
        }
        activeField?.resignFirstResponder()
        activeField = nil;
        activeTextView?.resignFirstResponder()
        activeTextView = nil;
    }
    
    // MARK: UIPicker
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 2 {
            return tecnicoPickerData.count;
        }else{
            return 0;
        }
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 2 {
            return tecnicoPickerData[row].nome;
        }else{
            return "";
        }
    }
    
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        if (pickerView.tag == 2) {
//            tecnicoTextField.text = tecnicoPickerData[row].nome;
//            tecnicoSelecionado = tecnicoPickerData[row].id;
//            technicianSelected = tecnicoPickerData[row];
//        }
    }
    
    func showAlert( vc: UIViewController, _ message: String,_ okBtnTitle: String?){
        vc.present(genericAlert, animated: true, completion: nil);
        genericAlert.setValue(message, forKey: "message");
        if(okBtnTitle != nil){
            genericAlert.actions[0].setValue((okBtnTitle ?? "OK"), forKey: "title");
        }
    }
    
    func showToastAndReturnScreen(message: String, seconds: Double, closeView: Bool) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert);
        alert.view.backgroundColor = UIColor.black;
        alert.view.alpha = 0.6;
        alert.view.layer.cornerRadius = 15;
        self.present(alert, animated: true);
        self.saveMode(false);
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds, execute: {
            [weak self] in
            guard let self = self else { return }
            alert.dismiss(animated: true);
            self.saveLoading.stopAnimating();
            if(closeView){
                self.closeView();
            }
        });
    }
    
    
    func msgCantEditObject(_ oldObject:Bool){
        self.showAlert(vc: self, "Só usuários da LinkDoctor tem permissão para vincular QRCode", "OK");
    }
    
    func setupLoadingSpin(){
        saveLoading.center = view.center;
        saveLoading.color = UIColor.white;
        saveLoading.hidesWhenStopped = true;
        saveLoading.layer.cornerRadius = 05;
        saveLoading.isOpaque = false;
        saveLoading.backgroundColor = UIColor.black;
        saveLoading.alpha = 0.3;
        view.addSubview(saveLoading);
    }
    
    func closeView(){
        self.navigationController?.popViewController(animated: true);
        self.dismiss(animated: true, completion: nil);
    }
    
    func showToast(message: String, seconds: Double) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert);
        alert.view.backgroundColor = UIColor.black;
        alert.view.alpha = 0.6;
        alert.view.layer.cornerRadius = 15;
        self.present(alert, animated: true);
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds){
            alert.dismiss(animated: true);
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil);
    }
    
    
}



// MARK: UITextFieldDelegate
extension ProductBoundQrCodeVC: UITextFieldDelegate, UITextViewDelegate {
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeField = textField;
        lastOffset = self.scrollView.contentOffset;
        return true;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        activeField?.resignFirstResponder();
        activeField = nil;
        return true;
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        activeTextView = textView;
        lastOffset = self.scrollView.contentOffset;
        if(textView == self.equipamentoTextView){
            activeField?.resignFirstResponder();
            self.performSegue(withIdentifier: "selectProductTypeSegue", sender: self)
            return false;
        }else{
            return true;
        }
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        activeTextView?.resignFirstResponder();
        activeTextView = nil;
        return true;
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return (textField != self.tecnicoTextField);
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if( textView == self.equipamentoTextView){
            return false;
        }else{
            let existingLines = textView.text.components(separatedBy: CharacterSet.newlines)
            let newLines = text.components(separatedBy: CharacterSet.newlines)
            let linesAfterChange = existingLines.count + newLines.count - 1
            if(text == "\n") {
                return linesAfterChange <= textView.textContainer.maximumNumberOfLines
            }
            let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
            let numberOfChars = newText.count
            return numberOfChars <= 100;
        }
    }
}


// MARK: Keyboard Handling
extension ProductBoundQrCodeVC {
    
    @objc func keyboardWillShow(aNotification: NSNotification) {
        let info = aNotification.userInfo!;
        let kbSize: CGSize = (
            (info["UIKeyboardFrameEndUserInfoKey"] as? CGRect)?.size
            )!;
        let contentInsets: UIEdgeInsets = UIEdgeInsets(
            top: 0.0, left: 0.0, bottom: kbSize.height, right: 0.0
        );
        var frame: CGRect!;
        if(activeTextView != nil){
            frame = activeTextView!.frame;
        }
        if(activeField != nil){
            frame = activeField!.frame;
        }
        scrollView.contentInset = contentInsets;
        scrollView.scrollIndicatorInsets = contentInsets;
        var aRect: CGRect = self.view.frame;
        aRect.size.height -= kbSize.height;
        if (frame != nil){
            if !aRect.contains(frame.origin) {
                self.scrollView.scrollRectToVisible(frame, animated: true);
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.3) {
            self.qrCodeBoundConstraintContentHeight.constant -= (self.keyboardHeight ?? 0);
            if(self.lastOffset != nil){
                self.scrollView.contentOffset = self.lastOffset;
            }
        }
        keyboardHeight = nil;
    }
}
