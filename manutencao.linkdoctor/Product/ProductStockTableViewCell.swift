//
//  ProductStockTableViewCell.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 28/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import UIKit

class ProductStockTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var firstTextLabel: UILabel!
    @IBOutlet weak var secondTextLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLabel.text = nil;
        subTitleLabel.text = nil;
        firstTextLabel.text = nil;
        secondTextLabel.text = nil;
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
