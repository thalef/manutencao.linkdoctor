//
//  ProductStockResponse.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 26/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation

struct ProductStockResponse: Codable {
    
    let id: String;
    let criado_local : Bool;
    let ativo: Bool;
    let codLocalAtual: String;
    let codMarca: String;
    let codModelo: String;
    let precisaSincronizar: Bool?;
    let codProduto: String;
    let descricaoEquipamento: String;
    let descricaoLocalAtual: String;
    let descricaoMarca: String;
    let descricaoModelo: String;
    let eComodato: Bool;
    let flagLocalAtual: String;
    let handlerDestination: String?;
    let handlerDestinationCustomerFolder: String?;
    let handlerDestinationRoot: String?;
    let handlerFilename: String?;
    let handlerMimetype: String?;
    let handlerPath: String?;
    let idRegistroAnterior: String;
    let qrCodeGerado: Bool;
    let serial: String;
    let tipoLocalAtualDescricao: String;
    let atualizadoEm: Date;
    let atualizadoEmTimeStamp: Double;
    let criadoEm: Date;
    let criadoEmTimeStamp: Double;
    let servidorAtualizadoEm: Date;
    let servidorAtualizadoEmTimeStamp: Double;
    let codigoQuemCriou: String;
    let deletado: Bool;
    let estoqueAvulso: Bool;
    let qrCodeVinculado: Bool;
    
    enum CodingKeys: String, CodingKey {
        case id;
        case criado_local;
        case ativo;
        case codLocalAtual;
        case codMarca;
        case codModelo;
        case precisaSincronizar;
        case codProduto;
        case descricaoEquipamento;
        case descricaoLocalAtual;
        case descricaoMarca;
        case descricaoModelo;
        case eComodato;
        case flagLocalAtual;
        case handlerDestination;
        case handlerDestinationCustomerFolder;
        case handlerDestinationRoot;
        case handlerFilename;
        case handlerMimetype;
        case handlerPath;
        case idRegistroAnterior;
        case qrCodeGerado;
        case serial;
        case tipoLocalAtualDescricao;
        case atualizadoEm;
        case atualizadoEmTimeStamp;
        case criadoEm;
        case criadoEmTimeStamp;
        case servidorAtualizadoEm;
        case servidorAtualizadoEmTimeStamp;
        case codigoQuemCriou;
        case deletado;
        case estoqueAvulso;
        case qrCodeVinculado;
    }
    
}
