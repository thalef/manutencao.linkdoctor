//
//  ProductStockEditRequest.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 23/01/20.
//  Copyright © 2020 ThiagoCarvalho. All rights reserved.
//

import Foundation

struct ProductStockEditRequest: Codable {
    
    var id: String;
    var serial: String;
    var eComodato: Bool
    
    init(){
        self.id = "";
        self.serial = "";
        self.eComodato = true;
    }
    
    
    enum CodingKeys: String, CodingKey {
        case id;
        case serial;
        case eComodato;
    }
    
}
