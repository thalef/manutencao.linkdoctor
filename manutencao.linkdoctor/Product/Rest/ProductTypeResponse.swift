//
//  ProductTypeResponse.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 08/05/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation

struct ProductTypeResponse: Codable {
    
    
    let id: String;
    let descricaoTipoProduto: String?;
    let codigo: String?;
    let descricao: String;
    let numRegistroAnvisa: String?;
    let codMarca: String;
    let codModelo: String;
    let descricaoMarca: String;
    let descricaoModelo: String;
    let observacao: String?;
    let acessorioOutroProduto: Bool;
    let ativo: Bool;
    let deletado: Bool;
    let servidorAtualizadoEmTimeStamp: Double;
    
    enum CodingKeys: String, CodingKey {
        case id;
        case descricaoTipoProduto;
        case codigo;
        case descricao;
        case numRegistroAnvisa;
        case codMarca;
        case codModelo;
        case descricaoMarca;
        case descricaoModelo;
        case observacao;
        case acessorioOutroProduto;
        case ativo;
        case deletado;
        case servidorAtualizadoEmTimeStamp;
    }
    
}
