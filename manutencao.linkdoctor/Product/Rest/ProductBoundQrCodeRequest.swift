//
//  ProductBoundQrCodeRequest.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 06/06/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation

struct ProductBoundQrCodeRequest: Codable {
    
//    public ProductBoundQrCodeRequest(String id, ProductBoundStockRequest stockToUpdate, ProductBoundMovimentRequest movimentToInsert) {
//    this.id = id;
//    this.deviceDescription = AppConfig.DEVICE_DESCRIPTION;
//    this.appVersionCode = AppConfig.APP_VERSION_CODE;
//    this.appVersionName = AppConfig.APP_VERSION_NAME;
//    this.stockToUpdate = stockToUpdate;
//    this.movimentToInsert = movimentToInsert;
//    }
 
    var id: String;
    let deviceDescription: String;
    let appVersionCode: Int;
    let appVersionName: String;
    var stockToUpdate: ProductBoundStockRequest!;
    var movimentToInsert: ProductBoundMovimentRequest!;
    
    init(){
        self.id = "";
        self.deviceDescription = "iOS";
        self.appVersionCode = 1;
        self.appVersionName = "iOS";
    }
    
    
    enum CodingKeys: String, CodingKey {
        case id;
        case deviceDescription;
        case appVersionCode;
        case appVersionName;
        case stockToUpdate;
        case movimentToInsert;
    }
    
}
