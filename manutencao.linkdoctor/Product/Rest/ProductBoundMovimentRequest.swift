//
//  ProductBoundMovimentRequest.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 06/06/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//
import Foundation

struct ProductBoundMovimentRequest: Codable {
    
    let codLocalAtual: String;
    let flagLocalAtual: String;
    let tipoLocalAtualDescricao: String;
    let descricaoLocalAtual: String;
    let idRegistroAnterior: String;
    let codProduto: String;
    let descricaoEquipamento: String;
    let codMarca: String;
    let descricaoMarca: String;
    let codModelo: String;
    let descricaoModelo: String;
    let eComodato: Bool;
    let serial: String;
    let deletado: Bool;
    let estoqueAvulso: Bool;
    let qrCodeVinculado: Bool;
    let codEstoque: String;
    let tipoAcao: String;
    let idMovimentacaoAnterior: String;
    let codLocalAnterior: String;
    let flagLocalAnterior: String;
    let descricaoLocalAnterior: String;
        
    enum CodingKeys: String, CodingKey {
        case codLocalAtual
        case flagLocalAtual;
        case tipoLocalAtualDescricao;
        case descricaoLocalAtual;
        case idRegistroAnterior;
        case codProduto;
        case descricaoEquipamento;
        case codMarca;
        case descricaoMarca;
        case codModelo;
        case descricaoModelo;
        case eComodato;
        case serial;
        case deletado;
        case estoqueAvulso;
        case qrCodeVinculado;
        case codEstoque;
        case tipoAcao;
        case idMovimentacaoAnterior;
        case codLocalAnterior;
        case flagLocalAnterior;
        case descricaoLocalAnterior;
    }
    
}
