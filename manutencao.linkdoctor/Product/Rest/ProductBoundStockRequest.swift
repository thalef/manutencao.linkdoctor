//
//  ProductBoundStockRequest.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 07/06/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation


struct ProductBoundStockRequest: Codable {
    
    let id: String;
    let codLocalAtual: String;
    let flagLocalAtual: String;
    let tipoLocalAtualDescricao: String;
    let descricaoLocalAtual: String;
    let idRegistroAnterior: String;
    let codProduto: String;
    let descricaoEquipamento: String;
    let codMarca: String;
    let descricaoMarca: String;
    let codModelo: String;
    let descricaoModelo: String;
    let serial: String;
    let eComodato: Bool;
    let qrCodeGerado: Bool;
    let deletado: Bool;
    let estoqueAvulso: Bool;
    let qrCodeVinculado: Bool;
    
    
    enum CodingKeys: String, CodingKey {
        case id
        case codLocalAtual;
        case flagLocalAtual;
        case tipoLocalAtualDescricao;
        case descricaoLocalAtual;
        case idRegistroAnterior;
        case codProduto;
        case descricaoEquipamento;
        case codMarca;
        case descricaoMarca;
        case codModelo;
        case descricaoModelo;
        case serial;
        case eComodato;
        case qrCodeGerado;
        case deletado;
        case estoqueAvulso;
        case qrCodeVinculado;
    }
    
}
