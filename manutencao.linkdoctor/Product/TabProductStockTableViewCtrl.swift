//
//  TabProductStocksTableViewController.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 28/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//
import Foundation
import UIKit
import RealmSwift
import RxSwift
import RxCocoa

class TabProductStockTableViewCtrl: UITableViewController, UISearchResultsUpdating {
    
    
    var customerId: String = "";
    var selectedId: String = "";
    private var productStockList: Results<ProductStock>?
    var notificationToken: NotificationToken? = nil
    let syncProcess = SyncProcessHelper();
    let disposeBag = DisposeBag();
    var searchController: UISearchController!
    
    @IBOutlet weak var emptyView: UIView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPullToRefresh();
        loadData();
        setupFields();
    }
    
    func setupPullToRefresh(){
        self.refreshControl?.addTarget(self, action: #selector(syncServer), for: .valueChanged)
    }
    
    @objc func syncServer(refreshControl: UIRefreshControl){
        syncProcess.getDefaultSyncRx(token: Session.getToken(), serviceKey: Session.getServiceKey(),
                                     appCustomerId:  Session.getUsuarioLogadoCodCliente(), tipoUsuario: Session.getUsuarioLogadoTipo(),
                                     beginTimeStamp: Session.getKeySincronizacaoTimestampUltimaString()
            )
            .observeOn(MainScheduler.instance)
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(
                onNext: { el in
            },
                onError: { err in
                    refreshControl.endRefreshing();
            },
                onCompleted: { () in
                    self.setLastSync();
                    refreshControl.endRefreshing();
            },
                onDisposed: { () in
                    refreshControl.endRefreshing();
            }
            ).disposed(by: disposeBag);
        
    }
    
    func setLastSync(){
        UserDefaults.standard.set((0), forKey: Session.KEY_SINCRONIZACAO_TIMESTAMP_SERVER);
        let tempSyncTsServer: Double = Session.getSincronizacaoTsUltimaServer();
        let dateToSave: Date = Date();
        if (tempSyncTsServer == 0 || tempSyncTsServer < Session.getKeySincronizacaoTimestampUltima()) {
            UserDefaults.standard.set((dateToSave.getDeviceDateDisplayBrWithHour()), forKey: Session.KEY_SINCRONIZACAO_DATA_ULTIMA);
            UserDefaults.standard.set((dateToSave.toEpochInt64()), forKey: Session.KEY_SINCRONIZACAO_TIMESTAMP);
        } else {
            UserDefaults.standard.set((dateToSave.getDeviceDateDisplayBrWithHour()), forKey: Session.KEY_SINCRONIZACAO_DATA_ULTIMA);
            UserDefaults.standard.set((dateToSave.toEpochInt64()), forKey: Session.KEY_SINCRONIZACAO_TIMESTAMP);
        }
    }
    
    func loadFromRealm(){
        productStockList = ProductStock.loadAllFromCustomerRealm(customerId: customerId);
    }
    
    func loadData(){
        loadFromRealm();
        notificationToken = productStockList?.observe { [weak self] (changes: RealmCollectionChange) in
            guard let tableView = self?.tableView else { return }
            switch changes {
            case .initial:
                tableView.reloadData()
            case .update(_, let deletions, let insertions, let modifications):
                tableView.beginUpdates()
                tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0)}),
                                     with: .automatic)
                tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.endUpdates()
            case .error(let error):
                fatalError("\(error)")
            }
        }
        
    }
    
    func setupFields(){
        setupSearchBar();
    }
    
    func setupSearchBar(){
        searchController = UISearchController(searchResultsController: nil);
        searchController.searchResultsUpdater = self;
        //        searchController.dimsBackgroundDuringPresentation = false;
        searchController.obscuresBackgroundDuringPresentation = false;
        searchController.searchBar.sizeToFit();
        searchController.searchBar.setValue("Cancelar", forKey: "cancelButtonText");
        searchController.searchBar.placeholder = "Buscar pelo nome";
        tableView.tableHeaderView = searchController.searchBar;
        definesPresentationContext = true;
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        if let searchText = searchController.searchBar.text {
            if(searchText.isEmpty){
                loadFromRealm();
            }else{
                if(productStockList?.count ?? 0 > 0 ){
                    productStockList = productStockList?
                        .filter("\(Constants.CAMPO_DESCRICAO_EQUIPAMENTO) contains[cd] %@", searchText);
                }
            }
            self.tableView.reloadData();
        }else{
            loadFromRealm();
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let size = productStockList?.count {
            if(size>0){
                self.emptyView.isHidden = true;
                return size;
            }else{
                self.emptyView.isHidden = false;
                return 0;
            }
        }
        self.emptyView.isHidden = false;
        return productStockList?.count ?? 0
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "ProductStocksListCell", for: indexPath) as! ProductStockTableViewCell;
        let productStock = self.productStockList?[(indexPath as NSIndexPath).row];
        cell.titleLabel.text = productStock?.getDescricaoEquipamento();
        cell.subTitleLabel.text = productStock?.getDescricaoMarcaModelo();
        cell.firstTextLabel.text = productStock?.getDescricaoSerial();
        cell.secondTextLabel.text = productStock?.getDescricaoComodato();
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        self.customerId = self.productStockList?[(indexPath as NSIndexPath).row].appCompanyId ?? "";
        self.selectedId = self.productStockList?[(indexPath as NSIndexPath).row].id ?? "";
        self.performSegue(withIdentifier: "goToProductByIdSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "goToProductByIdSegue") {
            let nextVC = segue.destination as! ProductStockVC;
            nextVC.customerId = self.customerId;
            if(self.selectedId.count > 0){
                nextVC.selectedId = self.selectedId;
                nextVC.editMode = true;
            }else{
                nextVC.selectedId = "";
                nextVC.editMode = false;
            }
        }
        
    }
    
    

    deinit {
        notificationToken?.invalidate()
    }
    
}
