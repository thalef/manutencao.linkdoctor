//
//  ProductTypeToSelectTbVC.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 07/06/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//
import Foundation
import UIKit
import RealmSwift
import RxSwift
import RxCocoa

class ProductTypeToSelectTbVC: UITableViewController, UISearchResultsUpdating {
    
    
    var customerId: String = "";
    private var productStockList: Results<ProductType>?
    var notificationToken: NotificationToken? = nil
    var vcFrom: String = "";
    
    var selectedProductId: String = "";
    var searchController: UISearchController!
    
    @IBOutlet weak var emptyView: UIView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData();
        setupFields();
    }
    
    func loadFromRealm(){
        productStockList = ProductType.loadAllFromRealmActive();
    }
    
    func loadData(){
        loadFromRealm();
        notificationToken = productStockList?.observe { [weak self] (changes: RealmCollectionChange) in
            guard let tableView = self?.tableView else { return }
            switch changes {
            case .initial:
                tableView.reloadData()
            case .update(_, let deletions, let insertions, let modifications):
                tableView.beginUpdates()
                tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0)}),
                                     with: .automatic)
                tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.endUpdates()
            case .error(let error):
                fatalError("\(error)")
            }
        }
        
    }
    
    func setupFields(){
        setupSearchBar();
        self.title = "Selecione o tipo";
    }
    
    func setupSearchBar(){
        searchController = UISearchController(searchResultsController: nil);
        searchController.searchResultsUpdater = self;
        //        searchController.dimsBackgroundDuringPresentation = false;
        searchController.obscuresBackgroundDuringPresentation = false;
        searchController.searchBar.sizeToFit();
        searchController.searchBar.setValue("Cancelar", forKey: "cancelButtonText");
        searchController.searchBar.placeholder = "Buscar pelo nome";
        tableView.tableHeaderView = searchController.searchBar;
        definesPresentationContext = true;
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        if let searchText = searchController.searchBar.text {
            if(searchText.isEmpty){
                loadFromRealm();
            }else{
                if(productStockList?.count ?? 0 > 0 ){
                    productStockList = productStockList?
                        .filter("\(Constants.CAMPO_DESCRICAO) contains[cd] %@", searchText);
                }
            }
            self.tableView.reloadData();
        }else{
            loadFromRealm();
        }
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let size = productStockList?.count {
            if(size>0){
                self.emptyView.isHidden = true;
                return size;
            }else{
                self.emptyView.isHidden = false;
                return 0;
            }
        }
        self.emptyView.isHidden = false;
        return productStockList?.count ?? 0
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "ProductTypeToSelectTbViewCell", for: indexPath) as! ProductTypeToSelectTbViewCell;
        let productStock = self.productStockList?[(indexPath as NSIndexPath).row];
        cell.titleLabel.text = productStock?.descricao;
        cell.subTitleLabel.text = productStock?.getDescricaoMarcaModelo();
        cell.firstTextLabel.text = "";
        cell.secondTextLabel.text = "";
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedProductId = self.productStockList?[(indexPath as NSIndexPath).row].id ?? "";
        self.performSegue(withIdentifier: "getSelectedProductTypeToBoundSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }
    
    deinit {
        notificationToken?.invalidate()
    }
    
}
