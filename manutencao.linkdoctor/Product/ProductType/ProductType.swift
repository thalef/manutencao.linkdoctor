//
//  ProductType.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 06/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation

import RealmSwift

@objcMembers
class ProductType: Object {
    
    @objc dynamic var id: String = "";
    @objc dynamic var descricaoTipoProduto: String = "";
    @objc dynamic var codigo: String = "";
    @objc dynamic var descricao: String = "";
    @objc dynamic var numRegistroAnvisa: String = "";
    @objc dynamic var codMarca: String = "";
    @objc dynamic var codModelo: String = "";
    @objc dynamic var descricaoMarca: String = "";
    @objc dynamic var descricaoModelo: String = "";
    @objc dynamic var observacao: String = "";
    @objc dynamic var acessorioOutroProduto: Bool = false;
    @objc dynamic var ativo: Bool = true;
    @objc dynamic var deletado: Bool = false;
    @objc dynamic var servidorAtualizadoEmTimeStamp: Double = 0.0;
    
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(   id: String,
                        descricaoTipoProduto: String,
                        codigo: String,
                        descricao: String,
                        numRegistroAnvisa: String,
                        codMarca: String,
                        codModelo: String,
                        descricaoMarca: String,
                        descricaoModelo: String,
                        observacao: String,
                        acessorioOutroProduto: Bool,
                        ativo: Bool,
                        deletado: Bool,
                        servidorAtualizadoEmTimeStamp: Double
        ) {
        
        self.init();
        self.id = id;
        self.ativo = ativo;
        self.descricaoTipoProduto = descricaoTipoProduto;
        self.codigo = codigo;
        self.descricao = descricao;
        self.numRegistroAnvisa = numRegistroAnvisa;
        self.codMarca = codMarca;
        self.codModelo = codModelo;
        self.descricaoMarca =  descricaoMarca;
        self.descricaoModelo =  descricaoModelo;
        self.observacao = observacao;
        self.acessorioOutroProduto = acessorioOutroProduto;
        self.deletado = deletado;
        self.servidorAtualizadoEmTimeStamp = servidorAtualizadoEmTimeStamp;
    }
    
    func getDescricaoMarcaModelo() -> String{
        return descricaoMarca + " - " + descricaoModelo;
    }
    
    func getDescricaoTela()-> String{
        let temp:String = "(" + descricaoMarca + " - " + descricaoModelo + ")";
        return descricao + (temp.count < 5 ? "" : ("\n" + temp));
    }
    
}



extension ProductType {
    
    static func convertResponseToObject(_ object: ProductTypeResponse) -> ProductType {
        return ProductType(
            id: object.id,
            descricaoTipoProduto: object.descricaoTipoProduto ?? "",
            codigo: object.codigo ?? "",
            descricao: object.descricao,
            numRegistroAnvisa: object.numRegistroAnvisa ?? "",
            codMarca: object.codMarca,
            codModelo: object.codModelo,
            descricaoMarca: object.descricaoMarca,
            descricaoModelo: object.descricaoModelo,
            observacao: object.observacao ?? "",
            acessorioOutroProduto: object.acessorioOutroProduto,
            ativo: object.ativo,
            deletado: object.deletado,
            servidorAtualizadoEmTimeStamp: object.servidorAtualizadoEmTimeStamp
        );
    }
    
    static func converListResponseToListObject(_ listToConvert: [ProductTypeResponse]) -> [ProductType] {
        var list: [ProductType] = [];
        for obj in listToConvert {
            list.append(convertResponseToObject(obj));
        }
        return list;
    }
    
    static func loadAllFromRealm(in realm: Realm = try! Realm()) -> Results<ProductType> {
        return realm.objects(ProductType.self).sorted(byKeyPath: "descricao")
    }
    
    static func loadAllFromRealmActive(in realm: Realm = try! Realm()) -> Results<ProductType> {
        return realm.objects(ProductType.self)
            .filter("\(Constants.CAMPO_ATIVO) = true AND \(Constants.CAMPO_DELETADO) = false")
            .sorted(byKeyPath: "descricao")
    }
    
    static func loadFromId(in realm: Realm = try! Realm(), id: String) -> ProductType? {
        return realm.objects(ProductType.self).filter("\(Constants.CAMPO_ID) = '\(id)'").first;
    }
    
    static func loadFromIdActive(in realm: Realm = try! Realm(), id: String) -> ProductType? {
        return realm.objects(ProductType.self).filter("\(Constants.CAMPO_ID) = '\(id)' AND \(Constants.CAMPO_ATIVO) = true AND \(Constants.CAMPO_DELETADO) = false").first;
    }
    
    static func loadAllFromCustomerRealm(in realm: Realm = try! Realm(), customerId: String) -> Results<ProductType> {
        return realm.objects(ProductType.self)
            .filter("\(Constants.CAMPO_COD_LOCAL_ATUAL) = '\(customerId)' AND \(Constants.CAMPO_ATIVO) = true AND \(Constants.CAMPO_DELETADO) = false")
            .sorted(byKeyPath: Constants.CAMPO_CRIADO_EM_TIMESTAMP, ascending: false);
    }
    
    @discardableResult
    static func add(objectToSave: ProductType, in realm: Realm = try! Realm())
        -> ProductType {
            let item = objectToSave;
            try! realm.write {
                realm.add(item)
            }
            return item
    }
    
    @discardableResult
    static func addOrUpdate(objectToSave: ProductType, in realm: Realm = try! Realm())
        -> ProductType {
            let item = objectToSave;
            try! realm.write {
                realm.add(item, update: .all);
            }
            return item
    }
    
    @discardableResult
    static func addAll(listToSave: [ProductType], in realm: Realm = try! Realm())
        -> [ProductType] {
            realm.beginWrite();
            for user in listToSave
            {
                realm.add(user)
            }
            try! realm.commitWrite()
            return listToSave
    }
    @discardableResult
    static func addOrUpdateAll(listToSave: [ProductType], in realm: Realm = try! Realm())
        -> [ProductType] {
            //try! realm.beginWrite();
            realm.beginWrite();
            realm.add(listToSave, update: .all)
            try! realm.commitWrite()
            return listToSave
    }
    
    //    func toggleCompleted() {
    //        guard let realm = realm else { return }
    //        try! realm.write {
    //            isCompleted = !isCompleted
    //        }
    //    }
    //
    //    func delete() {
    //        guard let realm = realm else { return }
    //        try! realm.write {
    //            realm.delete(self)
    //        }
    //    }
}
