//
//  RealmSetup.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 13/06/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

// All changes to an object (addition, modification and deletion) must be done within a write transaction.
final class RealmSetup {
    
    static let currentSchemaVersion: UInt64 = 1
    
    static func configureMigration() {
        let config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            schemaVersion: 1,
            
            // Set the block which will be called automatically when opening a Realm with
            // a schema version lower than the one set above
            migrationBlock: { migration, oldSchemaVersion in
                // We haven’t migrated anything yet, so oldSchemaVersion == 0
                if (oldSchemaVersion < 1) {
                    // Nothing to do!
                    // Realm will automatically detect new properties and removed properties
                    // And will update the schema on disk automatically
                }
        })
        
        // Tell Realm to use this new configuration object for the default Realm
        Realm.Configuration.defaultConfiguration = config
        
        // Now that we've told Realm how to handle the schema change, opening the file
        // will automatically perform the migration
        _ = try! Realm();
        
//        let config = Realm.Configuration(schemaVersion: currentSchemaVersion, migrationBlock: { (migration, oldSchemaVersion) in
//            if oldSchemaVersion < 1 {
//                migrateFrom0To1(with: migration)
//            }
//
////            if oldSchemaVersion < 2 {
////                migrateFrom1To2(with: migration)
////            }
////
////            if oldSchemaVersion < 3 {
////                migrateFrom2To3(with: migration)
////            }
//        })
//        Realm.Configuration.defaultConfiguration = config
    }
    
    // MARK: - Migrations
    static func migrateFrom0To1(with migration: Migration) {
        // Add an email property
        migration.enumerateObjects(ofType: Appointment.className()) {
            (_, newObj) in
            newObj?["dataPrevisaoChegadaTimeStamp"] = 0.0;
            newObj?["dataPrevisaoChegada"] = nil;
            newObj?["horaPrevisaoChegada"] = "";
        }
    }
    
//    static func migrateFrom1To2(with migration: Migration) {
//        // Rename name to fullname
//        migration.renameProperty(onType: Person.className(), from: "name", to: "fullName")
//    }
//
//    static func migrateFrom2To3(with migration: Migration) {
//        // Replace fullname with firstName and lastName
//        migration.enumerateObjects(ofType: Person.className()) { (oldPerson, newPerson) in
//            guard let fullname = oldPerson?["fullName"] as? String else {
//                fatalError("fullName is not a string")
//            }
//
//            let nameComponents = fullname.components(separatedBy: " ")
//            if nameComponents.count == 2 {
//                newPerson?["firstName"] = nameComponents.first
//                newPerson?["lastName"] = nameComponents.last
//            } else {
//                newPerson?["firstName"] = fullname
//            }
//        }
//    }
    
    
}
