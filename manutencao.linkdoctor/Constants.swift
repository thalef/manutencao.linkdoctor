//
//  Constants.swift
//  manutencao.linkdoctor
//
//  Created by Thiago Carvalho on 08/04/19.
//  Copyright © 2019 ThiagoCarvalho. All rights reserved.
//

import Foundation

class Constants{
    
    static let developerUrlRestHost = "http://192.168.0.6:3020";
    static let productionUrlRestHost = "https://manutencao-app-api.essencial-ti-siti.com.br/manutencao-app-api";
    static let apiKeyUrlRestHost = "3b91cab8-926f-49b6-ba00-920bcf934c2a";
    static let apiTokenResetPassUrlRestHost = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzaWQiOiIyOTc3NDU5Yy1kZThiLTRmNjctODlhYS0zNmE2Yzk3YmRiZjUiLCJ1c3VhcmlvIjoidGhhbGVmQGdtYWlsLmNvbSIsImV4cCI6MTQ3NzA1MTI1OSwiaWF0IjoxNDc2NDQ2NDU5fQ.LuiVb4Wi-8oMM5Ajir2XlDEjg9EKkhY-KdrX0TT5DlU";
    static let productionHospitalName = "LinkDoctor";
    static let developerHospitalName = "Essencial-TI";
    static let testHospitalName = "testeMarcacao";
    static let developerHospitalKey = "0dbec8a0-ddec-4667-8e5c-60a7c6987acf";
    static let productionHospitalKey = "0dbec8a0-ddec-4667-8e5c-60a7c6987acf";
    static let linkDoctorHospitalKey = "0dbec8a0-ddec-4667-8e5c-60a7c6987acf";
    static let linkDoctorHospitalName = "Link Doctor";

    static let BARRA = "/";
//    static let URL_REST_HOST = developerUrlRestHost;
    static let URL_REST_HOST = productionUrlRestHost;
    
    static let URL_REST = URL_REST_HOST;
    static let URL_REST_BASE = URL_REST + BARRA;
    static let APP_HOSPITAL_KEY = developerHospitalKey;
    static let APP_SERVICE_KEY = apiKeyUrlRestHost;
    static let APP_TOKEN_RESET_PASS_KEY = apiTokenResetPassUrlRestHost;
    static let EQUAL = "=";
    static let RX = "rx";
    static let PATH_PRONTUARIOS = "prontuarios";
    static let PATH_SINCRONIZAR = "sincronizar";
    static let PATH_SYNC = "sync";
    static let PATH_DATA = "data";
    static let PATH_MEDICAL_RECORDS = "medical-records";
    static let PATH_SEARCH = "search";
    static let PATH_PACIENTES = "pacientes";
    static let PATH_CUSTOMERS = "/clientes";
    static let PATH_STOCK = "/estoque";
    static let PATH_PARAM_ID = "?id=";
    static let URL_USER = URL_REST + "/usuarios/web";
    static let URL_CUSTOMER = URL_REST + PATH_CUSTOMERS;
    static let PATH_CUSTOMER_SYNC = URL_CUSTOMER + BARRA + PATH_SINCRONIZAR;
    static let PATH_CUSTOMER_SYNC_RX = URL_CUSTOMER + BARRA + PATH_SYNC + BARRA + RX;
    static let PATH_CUSTOMER_SYNC_RX_BY_ID = URL_CUSTOMER + BARRA + PATH_SYNC + BARRA + RX + BARRA;
    static let PATH_CUSTOMER_DATA = URL_CUSTOMER + BARRA + PATH_DATA;
    static let PATH_CUSTOMER_WITH_RECORDS = URL_CUSTOMER + BARRA + PATH_MEDICAL_RECORDS;
    static let PATH_CUSTOMER_UUID = URL_CUSTOMER + BARRA + "getuuid";
    static let PATH_PRODUCT_STOCK_BOUND_QR_CODE = URL_REST + PATH_STOCK + BARRA + "boundqrcode" + BARRA;
//    static let PATH_PRODUCT_STOCK = URL_REST + PATH_STOCK + BARRA;
    static let PATH_CUSTOMER_WITH_RECORDS_BY_ID = URL_CUSTOMER + BARRA + PATH_MEDICAL_RECORDS + BARRA;
    //static let URL_CHECK_LOGIN = URL_LOGIN + "?" + CAMPO_APP_VERSION_CODE + EQUAL + APP_VERSION_CODE + "&" + CAMPO_APP_VERSION_NAME + EQUAL + APP_VERSION_NAME;
    static let URL_LOGIN = URL_REST + BARRA + "autentica";
    static let URL_CHECK_LOGIN = URL_LOGIN + "?" + CAMPO_APP_VERSION_CODE + EQUAL + "1" + "&" + CAMPO_APP_VERSION_NAME + EQUAL + "ios";
    static let URL_PASSWORD_RESET_REQUEST = URL_REST + "/autentica/resetPasswordRequest";
    static let URL_HOSPITAIS = URL_REST + "/hospitais";
    static let URL_HOSPITALS_SYNC = URL_HOSPITAIS + BARRA + PATH_SYNC;
    static let URL_USUARIOS = URL_REST + "/usuarios";
    static let URL_PRODUCT_STOCK = URL_REST + PATH_STOCK;
    static let URL_USUARIOS_SYNC = URL_USUARIOS + BARRA + PATH_SYNC;
    static let URL_PRODUCT_STOCK_DELETE = URL_REST + PATH_STOCK + BARRA + "delete" + BARRA;

    static let URL_TECNICOS = URL_REST + "/tecnicos";
    static let URL_TECNICOS_SYNC = URL_TECNICOS + BARRA + "data";
    static let URL_TABELAS_TIPO = URL_REST + "/tabelasTipo";
    static let URL_TABELAS_TIPO_SYNC = URL_TABELAS_TIPO + BARRA + PATH_SYNC;
    
    static let PARAM_CUSTOMER_ID = "customerId";
    static let PARAM_PRODUCT_QR_CODE_ID = "productId";

    static let FIREBASE_EVENT_CONDUTA_CRIADA = "conduta_criada";
    static let FIREBASE_EVENT_CONDUTA_ATUALIZADA = "conduta_atualizada";
    static let FIREBASE_EVENT_LOGIN_FEZ_LOGIN = "login_success";
    static let FIREBASE_EVENT_LOGIN_ACESSOU = "login_access";
    static let FIREBASE_EVENT_LOGIN_FEZ_LOGOFF = "logoff";
    static let FIREBASE_EVENT_LIMPAR_DADOS = "limpar_dados";
    static let FIREBASE_EVENT_SINCRONIZACAO_INICIOU = "sync_begin";
    static let FIREBASE_EVENT_SINCRONIZACAO_FALHOU = "sync_fail";
    static let FIREBASE_EVENT_SINCRONIZACAO_SUCESSO = "sync_success";
    static let FIREBASE_EVENT_HTTP_EXCEPTION = "http_exception";
    static let FIREBASE_EVENT_SOCKET_TIMEOUT_EXCEPTION = "socket_timeout_exception";
    
    static let PARAM_PACIENTE_ID = "pacienteId";
    static let PARAM_HOSPITAL_ID = "hospitalId";
    
    static let PARAM_MODO_EDICAO = "modoEdicao";
    static let PARAM_CONDUTA_ID = "condutaId";
    static let PARAM_INICIA_MODO_SYNC = "iniciaModoSincronizacao";
    static let PARAM_SYNC_RESULT_DIALOG = "SyncResultDialog";
    static let REST_PARAM_CLOSE = "close";
    static let REST_PARAM_KEEP_ALIVE = "keep-alive";
    static let REST_PARAM_CONNECTION = "Connection";
    static let REST_PARAM_AUTHORIZATION = "Authorization";
    static let REST_PARAM_SERVICE_KEY = "service_key";
    static let REST_PARAM_FORMATADO_PARA_ARRAY = "formatadoParaArray";
    static let REST_PARAM_BEGIN_TIMESTAMP = "beginTimeStamp";
    static let REST_PARAM_APP_VERSION_CODE = "appVersionCode";
    static let REST_PARAM_APP_VERSION_NAME = "appVersionName";
    static let USUARIO_TIPO_CLIENTE_FINAL = "C";
    static let USUARIO_TIPO_CLIENTE_MEDICO = "M";
    static let USUARIO_TIPO_EMPRESA_TECNICO = "T";
static let USUARIO_TIPO_EMPRESA_ADM = "E";
    static let USUARIO_TIPO_MASTER = "@";
    static let CAMPO_ID_SERVER = "id_server";
    static let CAMPO_DESCRICAO = "descricao";
    static let CAMPO_TIPO_USUARIO = "tipoUsuario";
    static let CAMPO_ID = "id";
    static let CAMPO_COD_CLIENTE = "codCliente";
    static let CAMPO_ATIVO = "ativo";
    static let CAMPO_COD_LOCAL_ATUAL = "codLocalAtual";
    static let CAMPO_COD_LOCAL = "codLocal";
    static let CAMPO_DELETADO = "deletado";
    static let CAMPO_PRECISA_SINCRONIZAR = "precisaSincronizar";
    static let CAMPO_CRIADO_LOCAL = "criado_local";
    static let CAMPO_DESCRICAO_EQUIPAMENTO = "descricaoEquipamento";
    static let CAMPO_CRIADO_EM_TIMESTAMP = "criadoEmTimeStamp";
    static let CAMPO_BEGIN_DATE_TIMESTAMP = "beginDateTimeStamp";
    static let CAMPO_COD_PRONTUARIO = "codProntuario";
    static let CAMPO_NOME = "nome";
    static let CAMPO_APP_VERSION_CODE = "app_version_code";
    static let CAMPO_APP_VERSION_NAME = "app_version_name";
    static let CAMPO_APP_COMPANY_ID = "appCompanyId";
    static let CAMPO_POSICAO = "posicao";
    static let FIELD_DEVICE_DESCRIPTION = "deviceDescription";
    static let TEXTO_S = "S";
    static let EMPTY_STRING = "";
    static let TEXTO_ATIVADO_POR = "Ativado por ";
    static let TEXTO_INATIVADO_POR = "Inativado por ";
    static let TEXTO_ATIVO = "Ativo";
    static let TEXTO_INATIVO = "Inativo";
    static let TEXTO_ATIVAR = "Ativar";
    static let TEXTO_INATIVAR = "Inativar";
    static let CLASSE_GENERIC_TABLES_RESPONSE = "GenericTablesResponse";
    static let CLASS_CUSTOMER_WITH_RECORDS_RESPONSE = "CustomerWithRecordsResponse";
    


}
